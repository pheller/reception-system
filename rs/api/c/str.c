/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    STR.C       String Verbs
**--------------------------------------------------------------------
**
**  Do the following API instructions:
**
**  STRING    string()
**  SUBSTR    substr()
**  INSTR     instr()
**
**  02/20/86        R.G.R       ORIGINAL
**  03/20/86        A.J.M.      UPDATE
**  04/02/86        A.J.M.      UPDATE
**  06/23/87        P.M.J.      SUBSTR;FIX FOR PROBLEM LOG #147:
**                  SUBSTRING would access beyond
**                  end of variable buffer.
**  07/??/87        SLW     Rewrite
**  01/14/88        LRZ     "string()" need to recalculate result lenght
**                          after data had been set up
**-------------------------------------------------------------------*/

#include <STR.LNT>      /* lint args (from msc /Zg) */
#include <SMDEF.IN>     /* Service manager definitions */
#include <DEBUG.IN>     /* Debugger compiler switch */
#include <OPERDEF.IN>       /* Operand definitions */
#include <OPERTSTR.IN>      /* Operand structure */
#include <RTADEF.IN>        /* Run time array definitions */
#include <INSERR.IN>        /* Instruction error return codes */
#include <APIUTIL.IN>       /* API utility externs */

/*---------------------------------------------------------------------
**          Externals
**-------------------------------------------------------------------*/

/*---------------------------------------------------------------------
**          Definitions
**-------------------------------------------------------------------*/

#define ENABLE    1        /* Enable external variable search */
#define DISABLE   0        /* Disable external variable search */
#define HIBYTE    1        /* Used to get to MSB of ins index */
#define LWBYTE    0        /* Used to get to LSB of ins index */
#define LITERAL   0        /* Literal string indicator */
#define NODATA    0        /* Null character detected */
#define DATA      1        /* Non null character detected */
#define LSB   0        /* Least sig. byte       */

/*---------------------------------------------------------------------
**  string()
**
**  The API STRING verb.  Concatenate the source operands and place
**  the result in the result operand.  Protect against dest being a source.
**
**  Instruction format:
**
**  INSTRUCTION #of opers. OPERAND#1  OPERAND#2  [OPERAND#3....OPERAND#8]
**  ---------------------------------------------------------------------
**   string    #of opers   result    source#1     source#2....source#7
**
**    DATE        PROGRAMMER      REASON
**--------------------------------------------------------------------
**  02/17/86        R.G.R.       ORIGINAL
**   3/20/86        A.J.M.       UPDATE
**   4/02/86        A.J.M.       UPDATE
**   6/12/86        R.G.R.       SOURCES SUPPORT
**-------------------------------------------------------------------*/
string ()
{
    OPER *source;           /* Ptr to 1'st operand table entry */
    OPER *result;           /* Ptr to 2'nd operand table entry */
    unsigned char FAR *tbuf, *tp;   /* Temporary for holding buf ptr */
    REG int i;              /* used for loop */
    unsigned char mode;         /* Instruction mode mask byte.*/
    unsigned char stype;        /* Source type */
    unsigned char nbr_of_pera;      /* Number of operands within this inst. */
    REG int result_buflen;

    /* Resolve mode mask */
    mode = getmode();

    /* Get total number of operands */
    r_getbyte();
    nbr_of_oper = *ip;

    /* Call to resolve operands */
    if (api_rtn_code = procoper(nbr_of_oper, mode, PAR1OP))
    return;

    /* Initialize pointers to operand table entries */
    result = &optbl[0];

#if DEBUG
    /*d Return error if result operands is a register or literal. d*/
    if (result->op_type <= ASC_TYPE)
    {
    api_rtn_code = OPER_ERR;
    return;
    }
#endif

    /* Calculate result buffer size and get a buffer */
    for (result_buflen = 0,
     source = &optbl[1],
     i = nbr_of_oper-1; i-- > 0; source++)
    result_buflen += ((stype=source->op_type) == INT_TYPE) ? IALEN :
              (stype          == DEC_TYPE) ? DALEN :
                             source->buflen;

    /* get buffer for result */
    tp = tbuf = get_buf(result_buflen);

    /* Fill in new buffer, OPER ==> RTA */
    for (source = &optbl[1],
     i=nbr_of_oper; --i>0; source++)
    {
    get_ascii_from_rta(tp, (RTA *)source, source->op_type);
    tp += source->buflen;
    }

    /* now it is safe to put result in RTA */
    make_buf_in_rta((RTA *)result->entry, 0);
    result->entry->buffer = tbuf;
    result->entry->buflen = tp - tbuf; /* LRZ 1/14/88  */
}

/*---------------------------------------------------------------------
**  substr()
**
**   The API SUBSTR verb.  Copy a substring of a source string into a
**   distination field.  The substring can start anywhere in the
**   source string, and can be of any length.  If the source is null,
**   do not set the destination [I consider this a bug, but it is part
**   of the spec -- SLW].  If the length is less than 0, reprot a
**   fatal error.  Only characters in the source that fall within the
**   specified length and displacement are returned, with null or a
**   partial subset returned if the request goes beyond the end of the
**   string.  Handles dest==source overlaps with memcpy().
**
**  Instruction format:
**
**   INSTRUCTION   OPERAND#1   OPERAND#2     OPERAND#3     OPERAND#4
**   ---------------------------------------------------------------
**   substring      source     destination   displacement  length
**
**    DATE            PROGRAMMER      REASON
**--------------------------------------------------------------------
**  02/20/86         R.G.R.         ORIGINAL
**  03/20/86         A.J.M.         UPDATE
**  04/02/86         A.J.M.         UPDATE
**  06/23/87         P.M.J.         SUBSTR;FIX FOR PROBLEM LOG #147
**                        SUBSTRING would access beyond
**                        end of variable buffer.
**  07/29/87         M.V.      SUBSTR;FIX FOR PROBLEM LOG #147:
**  07/30/87         SLW       rewrite to use memcpy() and fix
**                        numerous special cases
**-------------------------------------------------------------------*/

substr()
{
    OPER *dest;               /* Ptr to 2'nd operand table entry */
    int sbuflen;              /* Holds source len */
    REG int len;              /* Holds length operand */
    REG int offs;             /* Holds offset operand */
    RTE FAR *opentry;              /* Pointer to loc. of oeprand entry */

    /* Initialize pointers to operand table entries */
#define source (&optbl[0])
    dest = &optbl[1];
#define offset (&optbl[2])
#define length (&optbl[3])

    /* Check to see if source is null if so return               */
    if (!(sbuflen = source->buflen))
     return;

#if DEBUG
    /*d Return error if 1'st & 2'nd operand is a literal or register. d*/
    if (source->op_type <= ASC_TYPE || dest->op_type <= ASC_TYPE)
     api_rtn_code = OPER_ERR;

    if (api_rtn_code != APIOK)
     return;

#endif

    /* Get valid length operand */
    if ((len = get_int_from_rta((RTA *)length, length->op_type)) < 0)
    {
     api_rtn_code = OPER_ERR;
     return;
    }

    /* Get offset, convert index origin from 1 to 0 */
    if ((offs = get_int_from_rta((RTA *)offset, offset->optype)-1) < 0 ||
     offs >= sbuflen)
     offs = len = 0;               /* return NULL */

    /* Update dest run time entry, handle 0 length */
    put_buf_in_rta((RTA *)dest->entry, source->buffer + offs,
             imin(len, sbuflen - offs));

#undef source
#undef offset
#undef length
}

/*---------------------------------------------------------------------
**  instr()
**
**   The API INSTR verb.  Calculate the offset of the first occurrence
**   of <pattern> in source>/  Store this offset (index origin 1) in
**   position, or store 0 if rn> not found.
**
**  Instruction format:
**
**   INSTRUCTION  OPERAND#1   OPERAND#2     OPERAND#3
**   -----------------------------------------------
**   substring     source     pattern       position
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  02/20/86        R.G.R.         ORIGINAL
**  03/20/86        A.J.M.         UPDATE
**   4/02/86        A.J.M.         UPDATE
**  10/06/86        R.G.R.         ALLOWED SOURCE AS LITERAL.
**-------------------------------------------------------------------*/

instr()
{
    OPER *source;            /* Ptr to 1'st operand table entry */
    OPER *pattern;           /* Ptr to 2'nd operand table entry */ 
    OPER *position;          /* Ptr to 3'rd operand table entry */ 
    int disp;                /* Holds displacement */
    REG int i;
    unsigned char stype;     /* source type */
    RTE FAR *opentry;             /* pointer to operand run time entry */
    unsigned char *srcp;     /* pointer to source string */
    unsigned char *patp;     /* pointer to pattern string */
    REG int patlen;          /* length of pattern to match.     */
    unsigned char *tmpp;     /* pointer to source string */
    unsigned char tmpbuf[DALEN];    /* have enough room for a DEC string */

    /* Initialize pointers to operand table entries */
    source = &optbl[0];
    pattern = &optbl[1];
    position = &optbl[2];
    stype = source->op_type;

#if DEBUG
    /*d Return error if 3'rd operand is literal. R.G.R. 10:06:86 d*/
    if (position->op_type == LIT_TYPE)
     api_rtn_code = OPER_ERR;

    /* Return error if 2'd operand is a register. d*/
    if (pattern->op_type <= REG_TYPE)
     api_rtn_code = OPER_ERR;

    if (api_rtn_code != APIOK)
     return;

#endif

    /* get ascii'fied form of source */
    if (stype <= REG_TYPE)
     get_ascii_from_rt(tmpp=tmpbuf, (RTA *)source, stype);
    else
     tmpp = source->buffer;

    /* try to find pattern in source */
    for (srcp=tmpp,
      patp=pattern->buffer,
      i=source->buflen-(patlen=pattern->buflen); i>=0; i--)
     if (!strncmp(srcp++, patp, patlen))
     {
         disp = srcp-tmpp;         /* index origin 1 */
         goto found;
     }
    disp = 0;      /* not found */
found:
    put_int_in_rta(disp, (RTA *)position->entry, position->op_type);
}
