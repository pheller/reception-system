/*API     = 1.1.2    LAST_UPDATE = 1.1.2    */
/********************************************************************
*
*              **** SCREEN COMMANDS ****
*
*********************************************************************
*
* FILE NAME             SCREEN.C
*
* DESCRIPTION:
*        This file contains the modules which will process the  API
* file handling instructions:
*
*               define_field ()
*               set_attribute()
*               set_key ()
*               set_func ()
*               trig_func ()
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/22/86        R.G.R.          ORIGINAL
*   09/16/86        R.G.R           SET-KEY BUG FIX.
*   07/12/88        SLW        Object id allocated but not freed
*
*********************************************************************/

#include <SCREEN.LNT>          /* lint args (from msc /Zg) */
#include <SMDEF.IN>       /* Service manager definitions. */
#include <SMKEYS.IN>           /* Service manager keys. */
#include <DEBUG.IN>       /* Debugger compiler switch. */
#include <INSERR.IN>           /* Instruction error return codes. */
#include <PKMACRO.IN>          /* Packet data handling macrs. */
#include <RTADEF.IN>           /* Run time array definitions. */
#include <OPERDEF.IN>          /* Operand definitions. */
#include <SLIST.IN>       /* Link list structure. */
#include <OBJUNIT.IN>          /* Object unit structure. */
#include <smppt.in>       /* Service manager structures. */
#include <ISCB.IN>        /* Call,link:RTN stack control block.*/
#include <SSCB.IN>        /* Window stack control block. */
#include <GEV_DEFS.IN>         /* Global definitions. */

/*********************************************************************
*
*              **** EXTERNALS ****
*
*********************************************************************/
extern struct oper optbl[];      /* Resolve operands table. */
extern unsigned int api_rtn_code;      /* Api return code. */

/*extern REASSIGNED_KEY_LIST  *reassigned_key;*/
extern KEY_TO_FUNCTION_MAP FAR *Key_Functions;
extern FUNCTION_STATUS_MAP function_status[];
extern unsigned char FAR *copy_buf();
extern unsigned char apirtn;      /* Return to reception system flag*/
extern unsigned char Function_set;  /* KM function change flag. */

extern SSCB sscb_stack[MAXWINDOWS+1]; /* Window stack. */
extern SSCB *api_suspend;            /* Current suspend stack ptr. */
extern ISCB *instack;
extern ISCB *level0;
extern unsigned int Sys_api_event;     /* Current api process. */
extern FAR WINDOW *W;

#if (DEBUG)
extern unsigned char Debug_flag;       /* Debugger enable flag. */
#endif
/*********************************************************************
*
*              **** DEFINITIONS ****
*
*********************************************************************/
#define NO_PROG_OBJ    0
#define PROG_OBJ       1
#define PROG_OBJ_PARM  2

#define ENABLE      1
#define DISABLE      0

#define RTN         1
#define NORTN       0
#define WARN_MSG    1
#define TEXT_MODE   1

#define PARM_HEADER_SIZE     4
#define TOTAL_HEADER_SIZE    2

#define MSB         1
#define LSB         0

/*********************************************************************
*
*              **** VARIABLE DECLARATIONS ****
*
*********************************************************************/
/* Union of integer and character array for fast hi byte access. */
union intary
{
unsigned char cary[2];          /* MSB and LSB of ivar. */
unsigned int ivar;         /* Integer variable. */
};

extern unsigned char trigger_event;
extern unsigned char trigger_function_event;

/********************************************************************
*
*            **** PROCESS RESET KEY FUNCTION ****
*
*********************************************************************
*
* FUNCTION:             release_key()
*
* DESCRIPTION:
*     This module will delete the reassigned key list entry that
* is attached to the logical key.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
release_key (log_key)

unsigned int log_key;           /* Logical key that is to be reset.*/
{
}


/********************************************************************
*
*            **** SEARCH FOR RE-ASSIGNED KEY ****
*
*********************************************************************
*
* FUNCTION:             key_search (logical key)
*
* DESCRIPTION:
*          This module is responsible for searching the re-assigne
* key link list for a logical key. if the key is found then the list
* entry pointer will be passed back to the caller otherwise a NULL
* value will be returned.
*
* INPUT:
*    .logical key
*
* OUTPUT:
*    .list entry pointer or null
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
key_search (log_key)
unsigned int log_key;
{
}


/********************************************************************
*
*            **** PROCESS KEY INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             proc_key ()
*
* DESCRIPTION:
*          This module is responsible for re-assigning a KM.
* key.
*   Instruction  operand#1     operand#2   operand#3  operand#4 oper#5
*     set_key   logical-key   process-id  display-id prgm-id state
*
* The logical key is used to index into the key-to-funciton table
* where the logical and display event are stored. Both of the events
* within the table will be saved in the old value fields within the
* re-assigned key list structure entry. The logical event will then
* be set to re-assigned key so that KM will know that it must search
* the re-assigned key list for the structure which will contain the
* process information for that key. This module will construct a
* re-assigned key list entry for the logical-key.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*   09/16/86        R.G.R.          LINK BUG FIX.
*
*********************************************************************/
proc_key (prog_flag)
unsigned int prog_flag;
{
}
/********************************************************************
*
*            **** PROCESS SET KEY INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             set_key ()
*
* DESCRIPTION:
*          This module is responsible for re-assigning a KM
* key and calls on process key to perform this functin. Since there
* is no program attached to this instruction the process key module
* will get called with a flag indicating no program object attached.
*
*     Instruction  operand#1     operand#2   operand#3
*      set_key    logical-key   process-id  display-id
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
set_key ()
{
}
/********************************************************************
*
*    **** PROCESS SET KEY WITH PROGRAM FUNCTION INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             set_key_prog ()
*
* DESCRIPTION:
*          This module is responsible for re-assigning a KM.
* key and calls on process key to perform this function. Since there
* is a program attached to this instruction the process key module
* will get called with a flag indicating program object attached.
*
*   Instruction  operand#1     operand#2   operand#3  operand#4 oper#5
*     set_key   logical-key   process-id  display-id prgm-id state
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
set_key_prog ()
{
}
/********************************************************************
*
*           **** PROCESS FUNCTION INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             proc_func ()
*
* DESCRIPTION:
*          This module is responsible for procesing the set function
* and set function with prog attach API instructions:
*
*   Instruction  operand#1     operand#2   operand#3  operand#4 o
*     set_func  fnction-id    status       program-id  state
*
* The function-id is used to index into the function status map table
* where the status and attached function are stored. The function status
* will be updated and a function will be attached.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
proc_func (prog_flag)

unsigned int prog_flag;

{

struct oper *func_id;         /* Function that is to be altered. */
struct oper *status;          /* Status of a newly define function. */
struct oper *prog_id;         /* Attached program object. */
struct oper *p1_id;      /* Program data bound for parm. regs. */

unsigned int func_nbr;
union intary status_nbr;

union intary total_p1_size;

unsigned char *tbuf;
unsigned count *data_for_p1;
unsigned int count;

REDEFINED_FUNCTION FAR *attach_buf;

/* Initialize pointers. */
func_id = &optbl[0];
status = &optbl[1];

#if DEBUG
/* Validate operands. */
if ((func_id->op_type == DEC_TYPE) || (status->op_type == DEC_TYPE))
{
    api_rtn_code = OPER_ERR;
    return;
}
#endif

/* If necessary convert function-id and status to binary integer. */
if (func_id->op_type != INT_TYPE)
    func_nbr = asciibin(func_id->buffer, func_id->buflen);
else
    func_nbr = *((unsigned int FAR *) func_id->buffer);

if (status->op_type != INT_TYPE)
    status_nbr.ivar = asciibin(status->buffer, status->buflen);
else
    status_nbr.ivar = *((unsigned int FAR *) status->buffer);

/* Update status field. */

funcion_status[func_nbr].s = ((function_status[func_nbr].s & 0x0F) | (status ?

if(func_nbr != ACTION){
if(((function_status[func_nbr].s & 0xF0) == DEFAULT_SETTING) ||((
                   function_status[func_nbr].s & oxf0) == DISABLED)){

  if(function_status[func_nbr].attached_function != 0){
    Obj_ref_kill(&function_status[func_nbr].attached_function->program_ptr.pr?
    if(function_status[func_nbr].attached_function->program_ptr.p1 != 0){
     free(function_status[func_nbr].attached_function->program_ptr.p1);
    }
    /* free prior object ID, if any - 7-12-88 SLW */
    if (function_status[func_nbr].attached_function->program_ptr.
        prog_obj.status != OBJREFOBJUNDEFINED)
     free((char *)function_status[func_nbr].attached_function->program_ptr.
          prog_obj.varptr.objidptr);

    free((char *)function_status[func_nbr].attached_function);
    funcion_status[func_nbr].attached_function = 0;
  }
}
}
/* If a program is to be attached than attach program. */
if ((prog_flag == PROG_OBJ) || (prog_flag == PROG_OBJ_PARJ))
{
    /* Initialize pointers. */
    prog_id = &optbl[2];

#if DEBUG
    /* Validate operands .*/
    if (prog_id->op_type <= REG_TYPE)
    {
       api_rtn_code = OPER_ERR;
       return;
    }
#endif
    /* If ncessary get a buffer for attached function. */
    if ((attach_buf = function_status[func_nbr].attached_function) == 0)
    {
       GETBUF ((unsigned char *)attach_buf, (sizeof(REDEFINED_FUNCTION)));
       function_status[func_nbr].attached_function = attach_buf;

    }else{
       /* Freeup old program and parameters if there. */

       Obj_ref_kill (&attach_buf->program_ptr.prog_obj);

       if (attach_buf->program_ptr.p1 != 0)
       free ((unsigned char *)attach_buf->program_ptr.p1);

       /* free prior object ID, if any - 7-12-88 SLW */
       if (attach_buf->program_ptr.prog_obj.status !=
        OBJREFOBJUNDEFINED)
        free((char *)attach_buf->program_ptr.prog_obj.
             varptr.objidptr);
    }

    if (prog_id->buflen == 0)
    {
     /* Set object ref status to not defined. */
     attach_buf->program_ptr.prog_obj.status = OBJREFOBJUNDEFINED;

    }else{
     /* Copy program object-ID into new buffer. */
     if (((char *)(attach_buf->program_ptr.prog_obj.varptr.objidptr) = copy_b?
     {
         return;
     }

     /* Set object ref status to not accessed. */
     attach_buf->program_ptr.prog_obj.status = OBJREFNOTACCESSED;
    }

    /* Show that no parameter binding is to occur. */
    attach_buf->program_ptr.p1 = 0;

    /* Update event fields within attached buffer. */
/*    attach_buf->new_values.function_event = func_nbr;
    attach_buf->new_values.display_event = NO_EVENT;*/

    /* Update pointer to window structure for PEVE access. */
    if ((Sys_api_event != SELECTOR)
              &&
      (Sys_api_event != PAGE_INITIALIZER)
               &&
      (Sys_api_event != PAGE_POST_PROCESSOR)
               &&
      (Sys_api_event != PAGE_HELP_PROGRAM) )
    {

     attach_buf->w_pev = W;
    }else{
     attach_buf->w_pev = O;
    }
}

/* If parameter binding is to occur then set up for binding. */
if (prog_flag == PROG_OBJ_PARJ)
{
    /* Initialize pointers. */
    p1_id = &optbl[3];

#if DEBUG
    /* Validate operands. */
    if (p1_id->op_type <= REG_TYPE)
    {
       api_rtn_code = OPER_ERR;
       return;
    }
#endif

    /* Update parameter list pointer. */
    if (P1_id->buflen != 0)
    {
     /* Initialize total parameter buffer size field within parameter list. */
     total_p1_size.ivar = p1_id->buflen + PARM_HEADER_SiZE;

     /* Build parameter list. */
     GETBUF (tbuf,total_p1_size.ivar)

     /* Update header fields within parameter list. */
     *tbuf = total_p1_size.cary[MSB];
     *(tbuf+1) = total_p1_size.cary[LSB];

     total_p1_size.ivar -= TOTAL_HEADER_SIZE;
     *(tbuf+2) = total_p1_size.cary[MSB];
     *(tbuf+3) = total_p1_size.cary[LSB];

     /* Attach program parameter to nuew buffer. */
     attach_buf->program_ptr.p1 = tbuf;

     /* Move program parameter data to new buffer. */
     data_for_p1 = p1_id->buffer;
     tbuf += TOTAL_HEADER_SIZE * 2;
     for (count = p1_id->buflen; count != 0; --count)
         *(tbuf++) = *(data_for_p1++);

    }else{

     /* Show that no parameter list is attached. */
     attach_buf->program_ptr.p1 = 0;
    }
}


/* Inform KM of the logical function change. */
Function_set = ENABLE;

}
/********************************************************************
*
*            **** PROCESS SET FUNCTION INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             set_func ()
*
* DESCRIPTION:
*          This module is responsible for setting a KM key
* function and calls on process function to perform this. Since there
* is no program attached to this instruction the process function module
* will get called with a flag indicating no program object attached.
*
*     Instruction    operand#1     operand#2
*      set_function  function-id   status
*
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
set_f ()
{

proc_func (NO_PROG_OBJ);

}
/********************************************************************
*
*       **** PROCESS SET FUNCTION PROGRAM INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:         set_func_prog ()
*
* DESCRIPTION:
*          This module is responsible for setting a KM key
* function and calls on process function to perform this. Since there
* is a program attached to this instruction the process function module
* will get called with a flag indicating program object attached.
*
*     Instruction   operand#1     operand#2  operand#3
*     set_function  function-id   status      prog-id
*
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
set_f_prog ()
{

proc_func (PROG_OBJ);

}
/********************************************************************
*
*    **** PROCESS SET FUNCTION PROGRAM WITH PARM INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             set_func_prog_parm ()
*
* DESCRIPTION:
*          This module is responsible for setting a KM key
* function and calls on process function to perform this. Since there
* is a program attached to this instruction the process function module
* will get called with a flag indicating program object attached. Also
* since data is bound to parameter register 1 a parameter list will
* be created.
*
*     Instruction    operand#1     operand#2  operand#3  perand#4
*      set_function  function-id   status      prog-id    parm-p1
*
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*
*********************************************************************/
set_f_p_parm ()
{

proc_func (PROG_OBJ_PARM);

}
/********************************************************************
*
*       **** PROCESS TRIGGER FUNCTION INSTRUCTION ****
*
*********************************************************************
*
* FUNCTION:             trig_func ()
*
* DESCRIPTION:
*          This module is responsible for calling a KM process
* function module. The function id is within the operand portion of
* the instruction.
*
*          Instruction      operand#1
*          trigger-function      function-id
*
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* -------------------------------------------------------------------
*   05/16/86        R.G.R.          ORIGINAL
*   08/25/86        R.G.R.          REMOVED WINDOW STACK FLUSH
*
*********************************************************************/
trig_func ()
{

struct oper *function_id;     /* Pointer to function id operand. */
unsigned int func_event;      /* Function event. */
ISCB *sv_level0;
ISCB *sv_instack;

/* Initialize operand pointer. */
function_id = &optbl[0];

#if DEBUG
/* Validate operand. */
if (function_id->op_type == DEC_TYPE)
{
    api_rtn_code = OPER_ERR;
    return;
}
#endif

/* If necessary convert function id to binary integer. */
if (function_id->op_type != INT_TYPE)

    func_event = asciibin(function_id->buffer, function_id->buflen);
else
    func_event = *((unsigned int FAR *)function_id->buffer);


trigger_event = ON;
trigger_function_event = func_event;

/* Return to reception system. */
apirtn = RTN;

}
