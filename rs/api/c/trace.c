/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/******************************************************************** 
*
*            **** PROCESS TBOL DEBUG TRACE COMMAND ****
*
*********************************************************************
*
* FILE NAME:           TRACE.C
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
**********************************************************************/
#include <SMDEF.IN>
#include <conio.h>
#include <stdio.h>
#include <apityp.in>
#include <INSERR.IN>
#include <DBGMACRO.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <SYMBOL.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
/***********************************************************************
*
*                  **** DEFINITIONS ****
*
***********************************************************************/
#define F1KEY     59
#define F2KEY     60

#define OPSPECIAL 0xF0
#define CALL      0xF1
#define GODEP     0xF2
#define JMP       0xF3
#define MKFORM    0xF4
#define EDIT      0xF5
#define CJMP      0xF6
#define FOUND     0
#define NOTFOUND  1
#define LSB       0
#define MSB       1


#define MAXPKTCNT  255
#define OUTMAX    0x40
#define PRINT     1
#define SCREEN    0
#define BREAK   0
#define REPLACE 1
#define TWO_OPER 2

#define RTN     1
#define NORTN   0

#define NOINPUT 0xFFFF
#define API_START 1

#define ENABLE  1
#define DISABLE 0

#define END_REC    0xFFFF
/***********************************************************************
*
*                    **** EXTERNALS ****
*
***********************************************************************/
typedef struct brkpnt
{
unsigned int brkloc;
unsigned char opcode;
}BRK;

extern unsigned char brknum;        /* Current number of breakpoints. */
extern struct brkpnt brktbl[10];    /* Table of break points. */

extern unsigned char FAR *ip;       /* Current instruction pointer. */
extern unsigned char bcount;        /* Number of packet bytes. */
extern unsigned char apirtn;        /* Return to system flag. */
extern unsigned char FAR *strtloc;  /* Unassemble start location. */
extern unsigned char FAR *strtaddr; /* Go cmd start address. */
extern unsigned char indata[];
extern unsigned char outdata[];

extern struct symbol symtbl[];          /* Holds symbol table. */
extern unsigned int symread;            /* Nbr of bytes in symbol table.*/
extern struct symbol *symrec;           /* Sym table record pointer. */
extern struct symbol *symend;           /* End of symbol table. */

extern struct oper optbl[];

extern unsigned char opnbrtbl[];        /* Number of operands table. */
extern unsigned char psmode;            /* Print/screen mode flag. */
extern FILE *outf;

extern unsigned char cnvtbl[];
extern TBOLPROG tbol_str;
extern Objstream stream_ptr;
/***********************************************************************
*
*                 **** VARIABLE DECLARATIONS ****
*
***********************************************************************/
union intary
{
unsigned char cary[2];
unsigne dint ivar;
};

/* unsigned char trcetbl[8 * 82];  */
unsigned char *trcptr;
unsigned char trlital;                /* Literal trace output flag. */
/********************************************************************
*
*                **** PROCESS TRACE  DEBUG COMMAND ****
*
*********************************************************************
*
* ROUTINE NAME:            trace ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
**********************************************************************/
trace (ins_nbr)
unsigned int ins_nbr;          /* Nbr of tbol instrs. to trace for. */
{

unsigned char FAR *sip;        /* Used to save current ip. */
unsigned char opcode;          /* Instruction opcode. */
unsigned int  loc;             /* Unassemble loc = 0 */
unsigned char flag;            /* Unassemble flag (use current loc.) */
unsigned int number;           /* Nbr of unassemble instructions. A*/
unsigned char nbroper;         /* Holds number of operands. */
unsigned char cnt;             /* Holds break table loop count. */
unsigned long brkloc;          /* Holds location of break. */
unsigned char *brk_loc;
unsigned char FAR *savip;      /* Holds saved ip. */
unsigned char FAR *previp;     /* Holds prev ip. */
unsigned char repflg;          /* Break replace flag. */
struct brkpnt *brkptr;         /* Pointer to break table. */
unsigned int delay;
unsigned char mode;
unsigned int  chr;

/* Initialize parameters pass to unasseble module. */
flag = 0;
loc = 0;
number = 1;

/* Set up location to start unassembly. */
strtloc = ip;

/* Default to 1 instr. if nbr. of instr. is zero. */
if (ins_nbr == NOINPUT)
    ins_nbr = 1;

/* Loop until all instructions are processed. */
while (ins_nbr != 0)
{
    /* Call to unassemble this instruction. */
    unassemb (loc, number, flag);

    /* Return to caller if the previous instruction requested
       a system return.
    */
    if (apirtn == RTN)
    {
#if DEBUG
       /* Inform Tbol programmer to exit (quit) the debugger. */
       printf ("RETURN CONTROL TO SYSTEM - Hit 'Q' and then ENTER key.\n");
       if (psmode == PRINT)
       {
         fprintf (outf,"RETURN CONTROL TO SYSTEM - HIT 'Q' and then ENTER key.\n");
         flushall();
       }
#endif
        return;


    /* Get instruction opcode. */
    opcode = *ip & ~COMPLEX;

    /* Save ip. */
    previp = ip;

    /* Replace opcode if instruction is a break. */
    repflg = 0;

    if (*ip == BREAK)
    {
       /* Compute break location. */
       brk_loc = &indata[0];
       brkloc = ip - brk_loc;

       brkptr = &brktbl[0];
       for (cnt = 0; cnt != brknum; ++cnt)
       {
          if (brkptr->brkloc == brkloc)
          {
             savip = ip
             *ip = brkptr->opcode;
             opcode = *ip & ~COMPLEX;
             repflg = REPLACE;
             break;
          }
          ++brkptr;
       }
    }
    nbroper = opnbrtbl[opcode];

    /* Reset packet count. */
    bcount = MAXPKTCNT;

    /* Initialize stream_ptr. */
    stream_ptr = tbol_str.prog_obj.varptr.streamptr;

    /* Call API interpreter to process this instruction.*/
    tboldrv (&tbol_str, API_START);

    /* Reinitialize start location. */
    strtloc = ip;

    /* Enable literal trace output. */
    trlital = ENABLE;

    /* Replace opcode if break was inserted. */
    if (repflg == REPLACE)
       *savip = BREAK;

    if ((opcode == O_EDIT)
               ||
       (opcode == O_STRING))
    {
        bcount = MAXPKTCNT;
        GETMODE(previp,bcount);
        GETBYTE(previp,bcount);
        nbroper = *previp;
    }
    if (opcode == O_CLEAR_FIELD)
    {
         GETMODE(previp,bcount);
         GETBYTE(previp,bcount);
         nbroper = 1;
    }
    if (opcode == O_SAV_FIELD)
    {
         GETMODE(previp,bcount);
         GETBYTE(previp,bcount);
         nbroper = 2;
    }
    if (opcode == O_SET_ATT)
       nbroper = 1;

    /* Default nbr of operands to 2 for CJMP. */
    if ((opcode >= O_CJEQ) && (opcode < O_JMP))
    {
        nbroper = TWO_OPER;
        trlital = DISABLE;
    }

    /* Disable literal ouput trace for arith. operations. */
    if ((opcode >= O_ADD) && (opcode <= O_DIV_REM))
        trlital = DISABLE;

    /* Format trace buffer. */
    /* If not special opcode then simply call to format. */
    if ((nbroper < OPSPECIAL) && (nbroper != 0))
    {
       /* Call to format trace. */
       trformat (nbroper);

       /* Place null terminator in trace buffer. */
       *(traceptr++) = 0;

       /* Output to display and or printer. */
       printf ("%s\n", outdata);
       if (psmode == PRINT)
       {
          fprintf (outf,"%s\n",outdata);
          flushall();
       }
    }
    if (apirtn == RTN)
    {
       /* Set up start address to previous location. */
       ip = strtaddr = strtloc = previp;

#if DEBUG
       /* Inform Tbol programmer to exit (quit) the debugger. */
       printf ("RETURN CONTROL TO SYSTEM - Hit 'Q' and then ENTER key.\n");
       if (psmode == PRINT)
       {
         fprintf (outf,"RETURN CONTROL TO SYSTEM - Hit 'Q' and then ENTER key.\n");
         flushall();
       }
#endif

       return;
    }

    if ((ins_nbr > 1) && (kbhit() != 0))
    {
       if (getch() == 0)
       {
          if ((chr = getch()) == F!KEY)
              goto trend;
       }
       for (;;)
       {
          if (kbhit() != 0)
          {
             if (getch() == 0)
             {
                 if ((chr = getch()) == F1KEY)
                     goto trend;
             }
             break;
          }
       }
    }
    --ins_nbr;
}

trend:

/* Set go command start address. */
strtaddr = ip;

/* Call to unassemble next instruction. */
unassemb (loc, number, flag);

}


/********************************************************************
*
*                **** FORMAT TRACE  ****
*
*********************************************************************
*
* ROUTINE NAME:            trformat (NUMBER OF OPERANDS)
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*   12/29/86        C. Hsieh         fix bug for scanning .SYM file
*
**********************************************************************/
trformat (nbroper)
unsigned char nbroper;            /* Number of operands to format. */
{

register struct oper *operand;
unsigned char *saverec;
unsigned int prevloc;
unsigned char scan;
unsigned char cnt;
unsigned char FAR *dataptr;
usigned char FAR *tbuf;
unsigned char FAR *tbuf1;
unsigned char count;
unsigned char index;
union intary num;
unsigned int roffset;
unsigned int rt_index;
unsigned int num1;

/* Initialize pointer to top of operand table. */
operand = &optbl[0];

/* Initialize pointer to trace table. */
trcptr = &outdata[0];

for (; nbroper != 0; --nbroper)
{
    /* If literal output is disabled then abort to end of loop. */
    if ((operand->op_type == LIT_TYPE) && (trlital == DISABLE))
    {
       ++operand;
       if (--nbroper == 0)
          break;
    }

    /* If operand is a partition variable then store its name. */
    if ((operand->op_type == PAR_TYPE)
                    ||
        (operand->op_type == GLB_TYPE))
    {
        /* Conbver to ascii string. */
        GETBUF (tbuf,IALEN);

         num.ivar = operand->rtoff.irtoff;

        /* Strip off 1'st bit if par variable. */
        if (num.cary[MSB] != 0)
        {
            if (num.cary[MSB] == 1)
            {
               /* Partition external variable. */
               num.ivar -= PAR2;

            }else{

               /* Global variable. */
               num.ivar -= PAR4;
            }
        }else{
               /* Partition ext var. */
               num.ivar -= (PAR1 - 1);
        }

        /* Convert to ascii. */
        index = count = binascii(num.ivar,tbuf);

        /* Store converted value into trace buffer. */
        if (operand->op_type == GLB_TYPE)
        {
           /* Operand is a global variable mode a # into output buf. */
           *(trcptr++) = '#';

        }else{
           /* Operand is a partition variable. */
           *(trcptr++) = '&';
        }

        dataptr = tbuf;
        /* Move converted data to output data. */
        for (; count != 0; --count)
        {
            *(traceptr++) = *(dataptr++);
        }
        while (index < 28)
        {
            *(trcptr++) = ' ';
            ++index;
        }
        /* Free up buffer. */
        free (tbuf);

    }else{
        /* Opeand is either a literal string or field data. */
        if (operand->op_type == LIT_TYPE)
        {
            /* Operand is a literal string. */
            *(trcptr++) = 'L';
            *(trcptr++) = 'I';
            *(trcptr++) = 'T';
            *(trcptr++) = 'E';
            *(trcptr++) = 'R';
            *(trcptr++) = 'A';
            *(trcptr++) = 'L';
            for (index = 7; index < 29; ++index)
                 *(trcptr++) = ' ';
        }else{
            /* Operand is field data. Scan symbol file. */
            scan = NOTFOUND;
            symrec = &symtbl[0];
            saverec = 0;
            prevloc = 0;

            /* Recalculate rta if necessary. */
            if ((operand->rtoff.irtoff >= RTA2)
                            &&
               (operand->rtoff.irtff <= RTA3))
            {
                roffset = ((operand->rtoff.irtoff - RTA2) + (RTA1+1));

            }else{
                roffset = operand->rtoff.irtoff;
            }

            for (;symrec->fldname[0] != 0;++symrec)
            {
               if (symrec >= symend)
               {
                   break;
               }
               /** if (((symrec->typ == RTA_FIELD)
                              ||
                   (symrec->typ == PROCEDURE)
                              ||
                   (symrec->typ == LABEL)
                              &&
                   (symrec->loc == roffset))      C. H. 12/29/86 **/
          if ((symrec->typ == RTA_FIELD)
                &&
         (symrec->loc == roffset))
               {
                 for (cnt = 0; symrec->fldname[cnt] != 0; ++cnt)
                 {
                    if (cnt >= 29)
                    {
                       break;
                    }
                    *(trcptr++) = symrec->fldname[cnt];
                 }
                 /* Found field! */
                 scan = FOUND;

                 while (cnt < 29)
                 {
                     *(trcptr++) = ' ';
                     ++cnt;
                 }
                 /* Get out of scan loop. */
                 break;

               }else{
                 if ((symrec->loc <= roffset)
                               &&
                     (symrec->loc > prevloc))
                 {
                     prevloc = symrec->loc;
                     (struct symbol *)saverec = symrec;
                 }
               }
            }
            if (scan == NOTFOUND)
            {
                if (saverec == 0)
                {
                    *(trcptr++) = 'N';
                    *(trcptr++) = 'U';
                    *(trcptr++) = 'L';
                    *(trcptr++) = 'L';
                    *(trcptr++) = ' ';
                    *(trcptr++) = ' ';
                    *(trcptr++) = ' ';
                    *(trcptr++) = ' ';
                }else{
                    /* Move least value name. */
                    symrec = (struct symbol *)saverec;
                    for (cnt = 0; symrec->fldname[cnt] != 0; ++cnt)
                    {
                       if (cnt >= 29)
                       {
                          break;
                       }
                       *(trcptr++) = symrec->fldname[cnt];
                    }
                    if (symrec->typ == RTA_ARRAY)
                    {
                       *(trcptr++) = '(';
                       ++cnt;
                       rt_index  = roffset - symrec->loc;
                       rt_index += 1;
                       GETBUF(tbuf,IALEN);
                       num1 = binascii(rt_index,tbuf);
                       tbuf1 = tbuf;
                       for (;num1 != 0; num1 -= 1)
                       {
                           *(trcptr++) = *(tbuf1++);
                           ++cnt;
                       }
                       free (tbuf);
                       *(trcptr++) = ')';
                       ++cnt;
                    }
                    while (cnt < 29)
                    {
                        *(trcptr++) = ' ';
                        ++cnt;
                    }
                }
            }
        }
    }
    /* Place delimiter into buffer. */
    *(trcptr++) = ' ';

    /* Call to convert and store results of operation. */
    results (operand);

    /* Point to next operand. */
    ++operand;

    /* Insert newline. */
    *(trcptr++) = '\n';
}

}
/********************************************************************
*
*                **** FORMAT RESULTS ****
*
*********************************************************************
*
* ROUTINE NAME:            results (Pointer to operand)
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
**********************************************************************/
results (operand)

struct oper *operand;

{
unsigned char FAR *tbuf;
unsigned char FAR *tbuf1;
unsigned char FAR *dataptr;
RTE         FAR *opentry;
unsigned char index;
unsigned int length;
unsigned int num;
union intary num1;
unsigned char count;

/* Store buffer length into trcptr buffer. */
*(trcptr++) = 'L';
*(trcptr++) = 'E';
*(trcptr++) = 'N;
*(trcptr++) = '=';
*(trcptr++) = ' ';

/* If operand is a literal, then store buffer in trace buffer. */
if (operand->op_type == LIT_TYPE)
{
     /* Conver tlength to ascii. */
     GETBUF (tbuf, IALEN);
     num = operand->buflen;
     count = index = binascii (num, tbuf);
     for (dataptr = tbuf; count != 0; --count)
         *(trcptr++) = *(dataptr++);

     while (index <= IALEN)
     {
         *(trcptr++) = ' ';
         ++index;
     }
     free (tbuf);

     /* Store  string within trawce buffer. */
     store (operand->buffer, operand->buflen);

     /* Return to caller. */
     return;
}
/* Point to run time entry. */
opentry = operand->entry;

/* Conver tlength to ascii. */
GETBUF (tbuf, IALEN);
num = opentry->buflen;
count = index = binascii(num, tbuf);
dataptr = tbuf;
for (;count != 0; --count)
{

     *(trcptr++) = *(dataptr++);
}
free (tbuf);

while (index <= IALEN)
{
    *(trcptr++) = ' ';
    ++index;
}

/* If operand is a integer register then store result. */
if (operand->rtoff.irtoff <= INT2)
{
   /* Move in DATA. */
   *(trcptr++) = ' ';
   *(trcptr++) = ' ';
   *(trcptr++) = 'D';
   *(trcptr++) = 'A';
   *(trcptr++) = 'T';
   *(trcptr++) = 'A';
   *(trcptr++) = '=';
   *(trcptr++) = ' ';

   /* Convert length to ascii. */
   GETBUF (tbuf, IALEN);
   count = index = binascii ((*((unsigned int FAR *)opentry->buffer)), tbuf);
   dataptr = tbuf;
   for (;count != 0; --count)
   {
        *(trcptr++) = *(dataptr++);
   }
   free(tbuf);

   while (index <= IALEN)
   {
       *(trcptr++) = ' ';
       ++index;
   }

}else{

   /* If operand is a decimal register then convert to ascii
      and store.
   */
   if (operand->rtoff.irtoff <= DEC2)
   {
       GETBUF (tbuf1,DALEN);
       length = bcd_ask (opentry->buffer,tbuf1);
       store (tbuf1, length);
       free (tbuf1);
    }else{

        /* Operand is a field. */
        store (opentry->buffer, opentry->buflen);
    }
}

}
/********************************************************************
*
*                **** STORE RESULTS ****
*
*********************************************************************
*
* ROUTINE NAME:        store (buffer ptr, buffer length)
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
**********************************************************************/
store (buf, blength)

unsigned char  FAR *buf;
unsigned int blength;

{

unsigned int cnt;             /* Used for maximum loop. */

/* Move in DATA. */
*(trcptr++) = ' ';
*(trcptr++) = ' ';
*(trcptr++) = 'D';
*(trcptr++) = 'A';
*(trcptr++) = 'T';
*(trcptr++) = 'A';
*(trcptr++) = '=';
*(trcptr++) = ' ';
/* Initialize max loop count. */
cnt = OUTMAX;

/* Loop and store results. */
while (blength != 0)
{
    /* Store ascii string within trace buffer. */
    if ((*buf >= 32) && (*buf <= 254))
    {
        *(trcptr++) = *(buf++);

    }else{

       /* Output a smile. */
        *(trcptr++) = 2;
        ++buf;
    }

    --blength;

    /* break from loop if max char display is reached. */
    if (--cnt == 0)
    {
       break;
    }
}

}
