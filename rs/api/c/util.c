/* API     = 1.1.2    LAST+UPDATE = 1.0.0    */
#include <SMDEF.IN>
#include <stdio.h>
#include <stdlib.h>
#include <INSERR.IN>
#include <apityp.in>
#include <DBGMACRO.IN>
#include <RTASTR.IN>
#include <SYMBOL.IN>
#include <OPERDEF.IN>

#define FILESIZE (8 * 1024)
#define OPLIMIT  0x4F
#define DISABLE  0
#define HIGHNUM  255
#define FOUND    0
#define NOTFOUND 1
#define PAR1     98
#define PAR2     255
#define PAR3     320
#define PAR4     511
#define SYS1     768
#define IALEN    6
#define OPSPECIAL 0xF0
#define CALL      0xF1
#define GODEP     0xF2
#define JMP       0xF3
#define MKFORM    0xF4
#define EDIT      0xF5
#define CJMP      0xF6
#define MOVEBLOCK 0xFB
#define SET_ATTR  0xF9
#define LSB       0
#define MSB       1

struct disasm
{
     unsigned char nemonic[11];
};

union intary
{
     unsigned char cary[2];
     unsigned int ivar;
};

     extern unsigned char opnbrtbl[];
     extern struct disasm *opname[];
     extern unsigned long FAR *partbl;
     extern unsigned char cur_par;
     extern unsigned char *glbmid;
     extern struct rta rtatbl[256];
     extern struct rta sysvtbl[100];
     extern unsigned char filename[12];
     extern FILE *fileptr;
     extern unsigned char indata [FILESIZE];
     extern unsigned char outdata [FILESIZE * 3];
     extern unsigned char outfile [12];
     extern unsigned int numread;
     extern unsigned char FAR *ip;
     extern unsigned char bcount;
     extern unsigned char *outptr;
     extern unsigned char symfile[12];
     extern FILE *symptr;
     extern unsigned int symread;
     extern struct symbol symtbl [100];
     extern struct symbol *symrec;
     extern struct symbol *symend;
     extern unsigned int wrsize;
     extern struct oper optbl[9];


callproc ()
{
     unsigned char *dataptr;
     unsigned char *tbuf;
     unsigned char mode;
     unsigned char nbroper;
     unsigned char cnt;
     union intary label;

     GETMODE (ip,bcount);
     GETBYTE (ip,bcount);
     nbroper = *ip;
     if ((dprocoper (nbroper, mode, DISABLE, DISABLE)) != OK)
          return;
     disoper(nbroper);
     *(outptr++) = ' ';
     label.cary[MSB] = *ip;
     GETBYTE (ip,bcount);
     label.cary[LSB] = *ip;
     GETBYTE (ip,bcount);
     label.ivar += (ip - indata);
     GETBYF (tbuf,IALEN);
     itoa (label.ivar,tbuf,10);
     dataptr = tbuf;
     for  (cnt = 7;((*dataptr != 0)||(cnt==0)); --cnt)
     {
/*        if (cnt == 0)
          {
               break;
          }*/
          *(outptr++) = *(dataptr++);
     }
     free (tbuf);
}

linkproc ()
{
     unsigned char *dataptr;
     unsigned char *tbuf;
     unsigned cchar mode;
     unsigned char nbroper;
     unsigned char cnt;
     union intary label;

     GETMODE (ip,bcount);
     GETBYTE (ip,bcount);
     nbroper = *ip;
     if ((dprocoper (nbroper, mode, DISABLE, DISABLE)) != OK)
     {
          return;
     }
     disoper(nbroper);
}

proc_set_attr ()
{
     unsigned char mode;
     unsigned char  form_flag = 0 ;
     union intary   form_len;
     unsigned int   i;

     GETMODE (ip,bcount);
     if ((dprocoper (1, mode, DISABLE, DISABLE)) != OK)
     {
          return;
     }
     disoper(1);
     GETBYTE(ip,bcount);
     GETBYTE(ip,bcount);
     form_flag = *ip;
     GETBYTE(ip,bcount);
     GETBYTE(ip,bcount);
     if ( form_flag > 0 )
     {
          GETBYTE (ip,bcount);
          form_len.cary[MSB] = *ip;
          GETBYTE (ip,bcount);
          form_len.cary[LSB] = *ip;
          for ( i = 0 ; i < form_len.ivar ; i++)
               GETBYTE(ip,bcount);
     }
}

gdep ()
{
     unsigned char *dataptr;
     unsigned char *tbuf;
     unsigned char mode;
     unsigned char nbroper;
     unsigned char cnt;
     unsigned char labelcnt;
     union intary label;

     GETMODE (ip,bcount);
     if ((dprocoper (OPER1, mode, DISABLE, DISABLE)) != OK)
     {
          return;
     }
     disoper(OPER1);
     *(outptr++) = ' ';
     for (labelcnt = *ip; labelcnt != 0; --labelcnt)
     {
          GETBYTE (ip,bcount);
          label.cary[MSB] = *ip;
          GETBYTE (ip,bcount);
          label.cary[LSB] = *ip;
          label.ivar += ((ip - indata) + 1);
          GETBUF (tbuf,IALEN);
          itoa (label.ivar,tbuf,10);
          dataptr = tbuf;
          for  (cnt = 7;((*dataptr != 0)||(cnt == 0)); --cnt)
          {
/*             if (cnt == 0)
               {
                    break;
               }*/
               *(outptr++) = *(dataptr++);
          }
          free (tbuf);
          *(outptr++) = ' ';
     }
     GETBYTE (ip,bcount);
}

procjmp ()
{
     unsigned char *dataptr;
     unsigned char *tbuf;
     unsigned char mode;
     unsigned char nbroper;
     unsigned char cnt;
     union intary label;

     GETBYTE (ip,bcount);
     label.cary[MSB] = *ip;
     GETBYTE (ip,bcount);
     label.cary[LSB] = *ip;
     GETBYTE (ip,bcount);
     label.ivar += (ip - indata);
     GETBUF (tbuf,IALEN);
     itoa (label.ivar,tbuf,10);
     dataptr = tbuf;
     for  (cnt = 7;((*dataptr != 0)||(cnt==0)); --cnt)
     {
/*        if (cnt == 0)
          {
               break;
          }*/
          *(outptr++) = *(dataptr++);
     }
     free (tbuf);
}

cjmp ()
{
     unsigned char *dataptr;
     unisgned char *tbuf;
     unsigned char mode;
     unsigned char nbroper;
     unsigned char cnt;
     union intary label;

     GETMODE (ip,bcount);
     if ((dprocoper (OPER2, mode, DISABLE, DISABLE)) != OK)
     {
          return;
     }
     disoper(OPER2);
     *(outptr++) = ' ';
     label.cary[MSB] = *ip;
     GETBYTE (ip,bcount);
     label.cary[LSB] = *ip;
     GETBYTE (ip,bcount);
     label.ivar += (ip - indata);
     GETBUF (tbuf, IALEN);
     itoa (label.ivar,tbuf,10);
     dataptr = tbuf;
     for  (cnt = 7;((*dataptr != 0)||(cnt==0)); --cnt)
     {
/*        if (cnt == 0)
          {
               break;
          }*/
          *(outptr++) = *(dataptr++);
     }
     free (tbuf);
}

proc_move_block ()
{
     unsigned char *dataptr;
     unsigned char *tbuf;
     unsigned char mode;
     unsigned char nbroper;
     unsigned char cnt;
     union intary label;

     GETMODE (ip,bcount);
     GETMODE (ip,bcount);
     if ((dprocoper (OPER2, mode, DISABLE, DISABLE)) != OK)
     {
          return;
     }
     disoper(OPER2);
}

proc_clear ()
{
     unsigned char *dataptr;
     unsigned char *tbuf;
     unsigned char mode;
     unsigned char nbroper;
     unsigned char cnt;
     union intary label;

     GETMODE (ip,bcount);
     GETMODE (ip,bcount);
     if ((dprocoper (OPER1, mode, DISABLE, DISABLE)) != OK)
     {
          return;
     }
     disoper(OPER1);
}

mapmake ()
{
     OPER     *oper1;
     OPER     *dest;
     OPER     *max;
     OPER     *imb;
     RTE      *opentry;
     unsigned char *bcsave;
     unsigned char *ipsave;
     struct mapstr FAR *mapbuf;
     unsigned char nbroper;
     int  optotal;
     int  addoper;
     int  addmode;
     int  bit_ctr ;
     int  mode_ctr = 8 ;
     unsigned char mode;
     unsigned char tmode;
     unsigned char modflag;
     unsigned char state;
     int        bckreset;
     union intary maxlen;
     union intary imbdlen;
     unsigned cha r*cur_offset;

     if ((*ip & COMPLEX) != 0)
     {
          GETBYTE(ip,bcount);
          mode = *ip;
          state = COMPLEX;
     }
     else
     {
          mode = state = 0;
     }
     GETBYTE (ip,bcount);
     nbroper = *ip;
     optotal = (nbroper * 3) + 1;
     if (state == COMPLEX)
     {
          addoper = (optotal - 8);
          if (addoper < 0)
          {
               addmode = 0;
               state = 0;
          }
          else
          {
               addmode = (addoper / 8);
               if (((addoper % 8)) != 0)
                    ++addmode;
          }
          bcsave = ip;
          SETPTR (ip, bcount, addmode);
     }
     if (dprocoper (4, mode, DISABLE, DISABLE) != OK)
          return;
     disoper(4);
     optotal -= OPER4;
     bit_ctr = OPER4 ;
     mode <<= OPER4 ;
     modflag = CLEAR;
     oper1 = &optbl[0];
     dest = &optbl[1];
     max = &optbl[2];
     imb = &optbl[3];
     opentry = oper1->entry;

maploop:
     maxlen.ivar = asciibin (max->buffer, max->buflen);
     imbdlen.ivar = asciibin (imb->buffer, imb->buflen);
     if (optotal == 0)
     {
          return;
     }
     if (state == COMPLEX)
     {
          while ( bit_ctr )
          {
               if (( mode_ctr == 8 )&& (addmode != 0))
               {
                    mode_ctr = 0 ;
                    cur_offset = ip;
                    ip = bcsave;
                    GETBYTE (ip,bcount);
                    ip = cur_offset;
                    tmode = *ip;
                    ++bcsave;
                    --addmode;
               }
               mode |= ( tmode & 0x80 ) >> ( 8 - bit_ctr ) ;
               tmode <<= 1 ;
               mode_ctr++ ;
               bit_ctr--  ;
          }
     }
     bckreset = -1;
     RESETPTR (ip,bcount,bckreset);
     if (dprocoper (3, mode, DISABLE, DISABLE) != OK)
     {
          return;
     }
     disoper(3);
     opertotal -= OPER3;
     bit_ctr += OPER3 ;
     mode <<= OPER3 ;
     dest = &optbl[0];
     max = &optbl[1];
     imb = &optbl[2];
     goto maploop;
}

#if DEBUG_ALONE
Obj_ref_get ()
{}

Obj_get_size ()
{}

prn_om_id ()
{}
#endif
 OPER
