/*
 *  09/16/87 LA  remove memory_dump() and replace with memsnap()
 *  09/14/87 PMJ add command line recall / editing (DOS-like,F1,F3 keys)
 *  09/02/87 PMJ add switches to activate/deactivate diagnostic traces:
 *               OMT - object manager trace
 *               SCT - object scan trace
 *               OBJ - object trace
 *
 *    Rev 1.5   27 May 1987 14:58:54   rs6
 * File has been modified for version 5.0.4
 *
 *    Rev 1.2   09 Mar 1987 15:16:24   rs6
 * Angle brackets for all includes. /LH/
 *
 *    Rev 1.1   03 Mar 1987  9:59:04   rs6
 * PVCS header macros installed.
*/
/* 12 December '86  algis  add command to dump memory space
   type "memory" at prompt in debug mode                    */

#include <process.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <malloc.h>
#include <debug.in>
#include <smdef.in>
#include <objunit.in>
#include <SMPPT.IN>
#include <SMKEYS.IN>

#define TEST 1
#define EQUAL   61
#define GREATER 62
#define LESSER  60
#define NUMCMDS 31
#define INVALID 0
#define LOAD    1
#define SET_BREAK 2
#define REM_BREAK 3
#define REM_ALL   4
#define LIST_BREAK 5
#define UNASSEMB   6
#define DISP_H     7
#define DISP_A     8
#define MODEM_H    9
#define MODEM_A    10
#define FILL_H     11
#define FILL_A     12
#define MODCODE    13
#define TRACE      14
#define GO         15
#define QUIT       16
#define CMD_HELP       17
#define OUTMODE    18
#define RESET      19
#define SETMBRK    20
#define RMVMBRK    21
#define LISTMB     22
#define LISTVAR    23
#define DUMP_PPT   24
#define RETURN_TO_DOS 25
#define QUIT_DISABLE  26
#define DUMP_CORE   27
#define DUMP_BUFFERS 28
#define DISP_SAVES 29
#define OM_TRACE 30
#define SC_TRACE 31
#define OB_TRACE 32
#define CH_BS      8
#define CH_LF      '\n'
#define CH_RTN     13
#define CH_LA      75
#define CH_F1      59
#define CH_F2      60
#define CH_F3      61            /* 09/14/87 PMJ */
#define CH_INS     82            /* 09/14/87 PMJ */ 
#define CH_DEL     83            /* 09/14/87 PMJ */ 

extern unsigned char  Debug_flag;
extern unsigned char  debug;           /* 09/02/87 PMJ */
extern unsigned int   obj_trace;       /* 09/02/87 PMJ */

#if !DEBUG_ALONE
extern struct EVENT event;
#endif

struct cmdt
{
     char *cm;
     int  v;
};

static struct cmdt cmdtab[] =
{
               {"L",LOAD},
               {"SB",SET_BREAK},
               {"RB",REM_BREAK},
               {"RAB",REM_ALL},
               {"LB",LIST_BREAK},
               {"U",UNASSEMB},
               {"DH",DISP_H},
               {"DA",DISP_A},
               {"MH",MODMEM_H},
               {"MA",MODMEM_A},
               {"FH",FILL_H},
               {"FA",FILL_A},
               {"MP",MODCODE},
               {"T",TRACE},
               {"G",GO},
               {"Q",QUIT},
               {"H",CMD_HELP},
               {"OUTMODE",OUTMODE},
                                   /* 09/14/87 PMJ remove reset entry */
            {"SMB",SETMBRK},
            {"RMB",RMVMBRK},
            {"LMB",LISTMB},
            {"LV",LISTVAR},
            {"PPT",DUMP_PPT},
            {"A",RETURN_TO_DOS},
         {"QD",QUIT_DISABLE},
            {"MEMORY",DUMP_CORE},
            {"QS",DUMP_BUFFERS},
            {"DS",DISP_SAVES},
            {"OMT",OM_TRACE},         /* 09/08/87 PMJ */
            {"SCT",SC_TRACE},         /* 09/08/87 PMJ */ 
            {"OBT",OB_TRACE},         /* 09/08/87 PMJ */ 
     };

static int loc1,loc2,num,val,nbytes,cmd;
static int flag,mode,offset;
static char fname[32],fldname[32];
static char lbuf[80];
static char fillbuf[10];
static char modbuf[256];

static char *p;
unsigned int dbg_print_flag;            /*Enable/disable printer*/

extern FILE *outf;
extern unsigned char psmode;
extern FILE *print_stream;

int cmdprc()
     {
l0:
     if (psmode)
        {
            printf("P_");
        }else{
            printf("S_");
           }
        if(getline() == 0)goto l0;
        cmd = getcmd();
        switch(cmd) {
        case INVALID : printf("Invalid command\n");
                    break;

        case LOAD:       if (getstr(fname) == 0)
                       {
                       printf("File name missing\n");
                       }
                    else
                       {
                       procload(fname);
                       }
                    break;
        case SET_BREAK:
                    if (getint(&loc1) == 0)
                       {
                       printf("Location missing\n");
                       }
                    else
                       {
                       setbreak(loc1);
                       }
                    break;

        case REM_BREAK:
                    if (getint(&loc1) == 0)
                       {
                       printf("Location mssing\n");
                       }
                    else
                       {
                       rembreak(loc1);
                       }
                    break;

        case REM_ALL:
                    remall();
                    break;

        case LIST_BREAK:
                    listbreak();
                    break;

        case UNASSEMB:
                    if (getint(&loc1) == 0)
                       {
                       flag = 0;
                       val = 0;
                       loc1= 0;
                       }
                    else
                       {
                       flag = 1;
                       getint(&val);
                       }
                    unassemb(loc1,val,flag);
                    break;

        case DISP_H:
                  mode = 1;
                  goto l1;
        case DISP_A:
                  mode = 0;
l1:
                    if (getstr(fldname) == 0)
                       {
                       printf("Field name missing\n");
                       break;
                       }
                    else
                       {
                       if (getint(&offset0 == 0)
                            {
                            flag = 0;
                            }
                       else
                            {
                            if (getint(&nbytes) == 0)
                                 {
                                 flag = 1;
                                 }
                            else
                                 {
                                 flag = 2;
                                 }
                            }
                       }
                    disp(fldname,offset,nbytes,mode,flag);
               break;

        case MODMEM_A:
                    mode = 0;
               goto l2;
        case MODMEM_H:
                    mode = 1;
l2:
                    if (getstr(fldname) == 0)
                       {
                       printf("Field name missing\n");
                       }
               else
                       {
                       if (getint(&offset) == 0) offset = -1;
                       modmem(fldname,offset,mode);
                       }
                    break;


        case FILL_H:
               mode = 1;
                    goto l3;
        case FILL_A:
                    mode = 0;
l3:
                    if (getstr(fldname) == 0)
                       {
                       printf("Field name missing\n");
                       }
                    else
                       {
                       if ((getstr(fillbuf) == 0)
                                             &&
                                        (mode == 0))
                                   {
                                       fillbuf[0] = ' ';
                                       fillbuf[1] = 0;
                                   }
                       procfill(fldname,fillbuf,mode);
                             }
                    break;

        case MODCODE:
                  if (getint(&loc1) == 0)
                       {
                       printf("Location missing\n");
                       }
                  else
                       {
                       while (getstr(fillbuf))
                            strcat(modbuf,fillbuf);
                       modcode(loc1,modbuf);
                       }
                     break;

        case TRACE:
               if (getint(&num))
                   trace(num);
                     else
                         trace (~num);
               break;

        case GO:
                    flag = 2;
                    if (getint(&loc1))
                       {
                       if (getint(&loc2))
                            {
                            flag = 0;
                            }
                       else
                            {
                       flag = 1;
                            loc2 = loc1;
                            }
                       }
                    procgo(loc1,loc2,flag);
                    break;

        case QUIT:
                    quit();
                    return;

        case CMD_HELP:
               prochelp();
                    break;

        case OUTMODE:
               outmode();
               break;
                             /* 09/14/87 PMJ delete RESET, not functional*/
           case    SETMBRK:
                             if (getstr(fldname) == 0)
                             {
                       printf("Variable name missing\n");
                                   break;
                             }
                             if (getstr(fillbuf)== 0)
                                 flag = 0;
                             else
                             {
                                 if (*fillbuf == EQUAL)
                                    flag = 1;
                                 if (*fillbuf == GREATER)
                                    flag = 2;
                                 if (*fillbuf == LESSER)
                                    flag = 3;
                                 if (flag == 0)
                                 {
                                    printf ("INVALID CONDITIONAL.\n");
                                    break;
                                 }
                                 if (getstr(fillbuf) == 0)
                                 {
                                    printf("CONDITIONAL SIGN MISSING.\n");
                                    break;
                                 }
                             }
                             set_mem_brk (fldname,fillbuf,flag);
                             break;
           case    RMVBRK:
                             if (getstr(fldname) == 0)
                             {
                       printf("Variable name missing\n");
                                   break;
                             }
                             rem_mem_brk (fldname);
                             break;
           case    LISTMB:
                             list_mem_brk ();
                             break;
           case    LISTVAR:
                             dsymbol ();
                             break;
           case    DUMP_PPT:
#if !DEBUG_ALONE
                             event.event = PRINT_PPT;
                             do_display_event(event);
#endif
                             break;
           case    RETURN_TO_DOS:
                             exit ();
                             break;
        case QUIT_DISABLE:
                                Debug_flag = 0 ;
                                quit();
                                return;

           case    DUMP_CORE:
                             memsnap();
                             break;
           case DUMP_BUFFERS:
             dump_save_buffers();
                break;

        case DISP_SAVES:

                   if((num = getstr(fname)) == 0){
                  printf("Buffer name missing\n");
             }
             else
             {
             display_saves(fname,num);
                }
             break;

                            /* 09/02/87 PMJ activate/deactivate om trace */
          case OM_TRACE:
                         if (getint(&num))     /* this is what smgr does */
                                debug = num;
                            else
                                debug = 0;
                         break;
                   /* 09/02/87 PMJ activate/deactivate object scan trace */
          case SC_TRACE:
                         if (getint(&num))     /* this is what smgr does */
                                Dmpsetmode( num );
                            else
                                Dmpsetmode( 0 );
                         break;

                        /* 09/02/87 PMJ activate/deactivate object trace */
          case OB_TRACE:
                         if (getint(&num))     /* this is what smgr does */
                                obj_trace = num;
                            else
                                obj_trace = 0;
                         break;


             }
             goto l0;
        }

int getline()
     {
     char *s;
     char ch;
        char *i,*j;
        char *insert;

     s = lbuf;
     p = lbuf;
        insert = 0;             /* 09/14/87 PMJ set insert mode off */

start:
        while(!kbhit()) PREEMPT();
        ch = getch();
     if (ch == 0)
          {
          ch = getch();
          switch (ch) {
                          /* 09/14/87 PMJ use F1 for character recall */
          case CH_F1:    putch(*s);
                                s+;
                    break;
          case CH_F2:    strcpy(lbuf,"OUTMODE");
                    return;
          case CH_F3:           /* 09/14/87 command line recall */
                                ch = *s;
                                while ( ch != '\0' )
                                {     putch(ch);
                                      s++;
                                      ch = *s;
                                }
                                break;
          case CH_LA:           if (s > p)
                            {
                            s--;
                            putch(CH_BS);
                            putch(' ');
                            putch(CH_BS);
                            }
                                   break;
          case CH_INS:             /* 09/14/87 PMJ set insert mode */
                                             /* ON = end of LBUF */
                                          if ( insert )
                                             insert = 0;
                                          else
                                          {
                                             insert = p;
                                             while ( *insert != '\0' )
                                             {     insert++;
                                             }
                                          }
                                             break;
          case CH_DEL:                      /* 09/14/87 PMJ delete character */
                                          i = s;
                                          while ( *i != '\0' )
                                             { *i++ = *(i+1);
                                             }
                                          break;
          }

          }
     else
          {
          putch(ch);
          if (ch == CH_BS)
               {
               if (s > p)
                    {
                    s--;
                    putch(' ');
                    putch(CH_BS);
                    }
               }
          else  if (ch == CH_RTN)
               {
               putch(CH_LF);
               *s = '\0';
               return(s-p);
               }
          else             /* 09/14/87 PMJ if inset mode */
                               /* move everything to the right by one */
                      if (insert)
                        { insert++;
                          i = insert;
                          j = i - 1;
                          while ( i > s )
                            *i-- = *j--;
                        }
               *s++ = ch;
          }
     goto start;
     }

int getstr(buf)
char *buf;
          {
          char *s;
          char ch;
          int i;

          s = buf;
          while ((*p = ' ') || (*p == '\t')) p++;
          ch = *p;
          i = 0;
          while ((ch != '\t') && (ch != ' ') && (ch != '\0'))
               {
               *s++ = ch;
               p++;i++;
               ch = *p;
               }
          *s = '\0';
          return(i);
          }

int getval(val)
int *val;
     {
     char ch;
     int i,j;

     while ((*p = ' ') || (*p == '\t')) p++;
     ch = *p;
     i = j = 0;
     *val = 0;
     while (isdigit(ch))
          {
          i = i*10 + (ch - '0');
          j++;
          p++; ch = *p;
          }
     *val = i;
     return(j);
     }

int getcmd()
     {
     int i,j;
     char cmdstr[10];
     getstr(cmdstr);
     cupper(cmdstr);
     for (i=0; i<NUMCMDS; i++)
          {
          if (strcmp(cmdstr,cmdtab[i].cm) == 0)
               return(cmdtab[i].v);
          }
     return(INVALID);
     }

int cupper(s)
char *s;
     {
     while (*s != '\0')
          {
          *s = toupper(*s);
          s++;
          }
     }

#if DEBUG_ALONE

main(argc,argv)
int argc;
char *argv[];
     {
        /* Display copyright logo. */
        disp_logo ();
        procload (argv[1]);
          cmdprc();
          if(dbg_print_flag)
          {
               fclose(outf);
               dbg_print_flag = 0;
          }
     }
#endif

disp_logo()
{
        /* Display copyright logo. */
        printf ("\nTRINTEX TBOL SYMBOLIC DEBUGGER Version 4.0a\n");
        printf ("Copyright TRINTEX 1986\n\n");
     psmode = 0;
}

int quit()
     {
     return;
     }
int outmode()
     {
     psmode = 1 - psmode;
     /*Only enable printer if not enabled.*/
     if(psmode)
          if(dbg_print_flag == 0)
          {
               print_stream = outf = fopen("LPT1","w");
               dbg_print_flag = 1;
          }
     else
          if(dbg_print_flag)
          {
               fclose(outf);
               dbg_print_flag = 0;
          }
     printf("Display mode toggled \n");
     }

int reset()
     {
     printf("Debugger reset\n");
     }
