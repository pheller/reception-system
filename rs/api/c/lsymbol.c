/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*         **** PROCESS DISPLAY SYMBOL DEBUG COMMAND ****
*
*********************************************************************
*
* FILE NAME:               LSYMBOL.C
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
**********************************************************************/
#include <SMDEF.IN>
#include <ctype.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <SYMBOL.IN>
#include <RTASTR.IN>
#include <RTADEF.IN>
#include <DBGMACRO.IN>
/***********************************************************************
*
*                  **** DEFINITIONS ****
*
***********************************************************************/
#define FILESIZE (8 * 1024)
#define GO  0
#define NOGO 1

#define PRINT        1
#define MAX_NAME_LENGTH  29
#deinfe LSBMSK       0x00FF

#define NOTCONVERT   0
#define CONVERT      1

#define F1KEY     59
#define F2KEY     60

#define BREAK     1
#define NOBREAK   0
/***********************************************************************
*
*                    **** EXTERNALS ****
*
***********************************************************************/
extern void PREEMPT();
extern unsigned char FAR *ip;
extern unsigned char bcount;
extern unsighed char FAR  *strtloc;
extern unsigned char indata[];
extern unsigned char FAR  *begprgm;

extern struct rta rtatbl[];
extern unsigned char FAR  *partbl[50];
extern unsigned char cur_par;
extern struct glbvlist FAR  *(*glbmid);

extern FILE *symptr;
extern struct symbol symtbl[500];
extern unsigned int symread;
extern struct symbol *symrec;
extern struct symbol *symend;

extern unsigned char outdata[];
extern unsigned char psmode;
extern FILE *outf;                    /* Print or srceen mode flag. */
/***********************************************************************
*
*                 **** VARIABLE DECLARATIONS ****
*
***********************************************************************/
struct intary
{
unsigned int irtoff;
unsigned char cary[2];
};
/********************************************************************
*
*          **** PROCESS DISPLAY SYMBOL DEBUG COMMAND ****
*
*********************************************************************
*
* ROUTINE NAME:         dsymbol ()
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   04/01/86            R.G.R.           ORIGINAL
*
**********************************************************************/
dsymbol ()
{
struct symbol *dsymptr;          /* Symbol table pointer. */
unsigned int outinx;
unsigned int index;
struct rta *rtaentry;
unsigned int count;
unsigned char FAR  *data;
unsigned char FAR  *tbuf;
unsigned char rtinx;
unsigned char cnvflag;
unsigned char chr;
unsigned int aryinx;
unsigned int cnt;

/* Initialize symbol table pointer. */
dsymptr = &symtbl[0];

/* Loop and output to screen or printer symbol data. */
for (; ((dsymptr->fldname[0] != 0) && (dsymptr < symend)); ++dsymptr)
{
    /* Initialize array index. */
    aryinx = 0;

    if (dsymptr->typ == RTA_ARRAY)
        aryinx = 1;

    /* Display run time fields. */
    if ((dsymptr->typ == RTA_FIELD)
                    ||
       (dsymptr->typ == RTA_ARRY))
    {
        rtinx = dsymptr->loc & LSBMSK;
        for (cnt = dsymptr->len; cnt != 0; --cnt)
        {

            /* Initialize output buffer index. */
            outinx = 0;

            /* Move field name into output buffer. */
            for (index = 0; dsymptr->fldname[index] != 0; ++index)
                outdata[outinx++] = dsymptr->fldname[index];

            /* If field is an array then show index. */
            if (aryinx != 0)
            {
               /* Move in brace. */
               outdata[outinx++] = '(';

               /* Convert index to ascii. */
               GETBUF (tbuf, IALEN);
               count = binascii(aryinx,tbuf);
               data = tbuf;
               for (; count != 0; --count)
                   outdata[outinx++] = *(data++);

               /* Free up buffer. */
               free(tbuf);

               /* Increment array index. */
               ++aryinx;

               /* Move in left brace. */
               outdata[outinx++] = ')';

               /* Point to run time entry. */
               rtaentry = &rtatbl[rtinx++];

            }else{
               /* Point to run time entry. */
               rtaentry = &rtatbl[rtinx];
            }

            /* Move space into buffer. */
            outdata[outinx++] = ' ';

            /* Move in equal sign. */
            outdata[outinx++] = '=';

            /* Move in space. */
            outdata[outinx++] = ' ';

            /* Move data into output buffer. */
            data = rtaentry->strptr;
            count = rtaentry->strlngth;
            cnvflag = NOTCONVERT;

            /* Convert contents if register. */
            if ((rtinx >= INT1)  && (rtinx <= INT2))
            {
                GETBUF (tbuf,IALEN);
                count = binascii (*(unsigned int FAR*)data, tbuf);
                data = tbuf;
                cnvflag = CONVERT;
            }
            if ((rtinx >= DEC1)  && (rtinx <= DEC2))
            {
                GETBUF (tbuf, DALEN);
                count = bcd_ask (data, tbuf);
                data = tbuf;
                cnvflag = CONVERT;
            }

            for (; count != 0; --count)
            {
               if ((isprint(*data)) != 0)
               {
                   outdata[outinx++] = *(data++);
               }else{
                   outdata[outinx++] = 1;
                   ++data;
               }
            }
            if (cnvflag == CONVERT)
                    free (tbuf);

            /* Move in terminator character. */
            outdata[outinx] = 0;
            printf ("%s\n",outdata);

            /* Output to printer if required. */
            if (psmode == PRINT)
            {
               fprintf (outf,"%s\n",outdata);
               flushall();
            }

            /* Check for key stroke. */
            if (key_wait () == BREAK)
               goto dsymend;
        }
    }
}

dsymend:

aryinx = 0;

}
/********************************************************************
*
*          **** PROCESS CHAECK FOR KEY HIT ****
*
*********************************************************************
*
* ROUTINE NAME:         key_wait()
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   04/01/86            R.G.R.           ORIGINAL
*
**********************************************************************/
key_wait ()
{
unsigned int chr;

if (kbhit() != 0)
{
   if (getch() == 0)
   {
      if ((chr = getch()) == F1KEY)
          return(BREAK);
   }
   for (;;)
   {
      if (kbhit() != 0)
      {
         if (getch() == 0)
         {
             if ((chr = getch()) == F1KEY)
                return(BREAK);
         }
         break;
      }
/**** 2-23-87 S. Sayegh ... added to prevent communication buffer overflow or
               system slow down.             */
      PREEMPTY();
   }
}

return(NOBREAK);

}
