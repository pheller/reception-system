/*API     = 1.1.2    LAST_UPDATE = 1.1.2    */
/*---------------------------------------------------------------------
**    APIUTIL.C       API Utility Functions
**--------------------------------------------------------------------
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.         ORIGINAL
**  08/15/87        SLW       REWRITE
**  09/24/87        SLW       Global_proc - failed on null SYS GEV's
**  23 October '87      Algis           Change the updating the status of a
**                       field.    Field post processors will
**                       only fire if the subscriber changed
**                       a field.  The processor will not
**                       fire when TBOL changes a field when
**                       tabbing through the fields.
**  02/15/88       LB    ny2ty() parenthesized to fix bug reported in km_copy
     4/14/88        LRZ       change sys_var_num to an external in order
                                   to get the correct table size
**  7-12-88         SLW       date/time allocated but not freed
**-------------------------------------------------------------------*/

#include <APIUTIL.LNT>        /* lint args (from msc /Zg) */
#include <stdio.h>
#include <clock.h>       /* LH 4/7/87 */
#include <malloc.h>
#include <string.h>      /* for memcpy */
#include <time.h>        /* for sys date & time 09:18:86 R.G.R */
#include <SMDEF.IN>      /* Service manager definintions */
#include <DEBUG.IN>
#include <INSERR.IN>          /* Instruction error return codes */
#include <RTADEF.IN>          /* Run time array definitions */
#include <RTASTR.IN>          /* Run time array definitions */
#include <OBJUNIT.IN>         /* Object unit definitions */
#include <SMPPT.IN>      /* Service manager page partition */
#include <OPERTSTR.IN>
#include <SYS_TYPE.IN>        /* System variable type */
#include <APIUTIL.IN>         /* API utilities */
#include <ERROR.IN>

/**********************************************************************
*
*              **** EXTERNALS ****
*
***********************************************************************/
ICB_TIME clock_icb;
extern unsigned char Fields_changed_by_api;
extern systbl sys_var_table[];
extern GVAR FAR *glbmid[];    /* Mid-ptr of global tree link list */
extern unsigned int SYS_VAR_NUM;   /* lrz 4/14/88 */
/**********************************************************************
*
*              **** DEFINITIONS ****
*
***********************************************************************/
#define ENABLE 1
/*   #define SYS_VAR_NUM  255 lrz 4/14/88    */
#define TIME_SIZE 16
#define TIME_BUFFER_SIZE 6
#define DATE_BUFFER_SIZE 8
#define YEAR_BASE    1900

/**********************************************************************
*
*              **** VARIABLE DECLARATIONS ****
*
***********************************************************************/

/*---------------------------------------------------------------------
**  int update_sys_gev(gev_name, act_gcb)
**  unsigned int gev_name;
**  GCB *act_gcb;
**
**   Update the contents of a system global variable.  A system
**   variable is assumed to be undefined when it's length is 0.
**   gev_name is the GEV to be updated, act_gcb is the GCB which
**   contains the new GEV data ptr and len.
**-------------------------------------------------------------------*/

int update_sys_gev(gev_name, act_gcb)
unsigned int gev_name;
GCB *act_gcb;
{
    systbl *sys_var;   /* pointer to system variable table */
    char far *new_buffer;   /* pointer to replacement buffer */

    /* point to GEV slot */
    sys_var = &sys_var_table[gev_name - 1];

    /* update GCB according to type */
    if (sys_var->sys_type == SYS_INT_TYPE)
     /* system variable is integer */
     *sys_var->syspp.sys_address = *((unsigned int*)act_gcb->pointer);
    else
    {
     /* free former buffer, if any */
     if (sys_var->sys_length)
         free((char *)*sys_var->syspp.sys_str_addr);

     /* install new system variable      */
     *sys_var->syspp.sys_str_addr = act_gcb->length ?
                           (unsigned int *)act_gcb->pointer : 0;
     sys_var->sys_length = act_gcb->length;
    }
}

/********************************************************************
*
*         **** PROCESS GLOBAL EXTERNAL VARIABLE *****
*
*********************************************************************
* 
* FUNCTION:      global_proc (pointer to global variable entry)
* 
* DESCRIPTION:
*      This module will update the global variable by taking its
*  correponding native code definition and converting it to ascii.
* 
* INPUT:
*     . none
* 
* OUTPUT:
*    .none
* 
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   05/26/86        R.G.R.          ORIGINAL
*   09/17/86        R.G.R.           SUPPORT FOR SYS_DATE & SYS_TIME
**********************************************************************/

global_proc(gvarp)
GVAR FAR *gvarp;
(
    GCB date_time_gcb;
    systbl *sys_var;         /* Define pointer to system variable table */

#if DEBUG_RUN
    return(0);
#endif

    /* If global variable is a system variable then update it */
    if (gvarp->glbvnum <= SYS_VAR_NUM)
    (
     /* Mark this GEV as a system variable */
     gvarp->glb_state |= SYS_VARIABLE;
     sys_var = &sys_var_table[gvarp->glbvnam];

     /* If system variable is a integer then convert */
     if (sys_var->sys_type == SYS_INT_TYPE)
         put_int_in_rta(*sys_var->syspp.sys_address, (RTA *)gvarp, ASC_TYPE);

     /* Get date if system variable is date. 09:18:86 R.G.R. */
     else if (gvarp->glbvnam == SYS_DATE-1 ||
           gvarp->glbvnam == SYS_TIME-1)
     {
         /*
         Update by Les Hancock, 3/5/87. We assume that on the first
         call to this code the following conditions pertain:

          . The GEV's SYS_DATE and SYS_TIME have been initialized
          to values sent down the line by TPF.
  
          . The startup code has defined but not dispatched the
          CLOCK and ALARM tasks.
          
          . The startup code has initialized the_time.isvalid
          and the_time.was_set to zero.
          
         Furthur update, 4/10/87.  Changes have been made to integrate
         the clock into the reception system.  (See clock.c.) this
         function is TBOL's only point of access to the clock.  To set
         the clock, TBOL should invoke this routine as soon as possible
         after startup.
         
         We proceed by getting the current time and date gev's into
         gcb's, using time_and_date() to update the gcb's, then putting
         the new values back into the gev's.  The only reason for doing
         get_sys_gev at this point is to retrieve the gcb.pointe values.
         
         Still further update, 4/11/87.  Why update the GEV's at all?  In
         fact the GEV's value is used only during the first call to this
         function, when time_and_date() needs date_gcb.pointer and 
         time_gcb.pointer to set clock_icb.  Forever after, it uses
         clock_icb to set date_dcb.pointer and time_gcb.pointer.
         Therefore I see no reason to call put_sys_gev.  HOWEVER, we'd
         better be usre nobody tries to access those GEV's directly.  The
         smart thing would be to put all this logic into get_sys_gev()
         rather that to put it here.
         
         4/16/87. I put the call to time_and_date() into get_sys_gev().
         */

         /* Free this buffer - 7-12-88 SLW */
         if (gvarp->gvarlen)
          free(gvarp->gvarbuf);
         get_sys_gev(gvarp->glbvnam+1, &date_time_gcb);
         gvarp->gvarlen = date_time_gcb.length;
         if (gvarp->gvarbuf = malloc(gvarp->gvarlen))
          memcpy(gvarp->gvarbuf, date_time_gcb.pointer,
                                date_time_gcb.length);
         else
          fatal_error(API_PROC, BUF_ERR);
         
/* for testing
fprintf(stdprn, "gvarp->gvarbuf = %.*s\n", gvarp->gvarlen, gvarp->gvarbuf);
*/
     }
     /* if not int, we can assume SYS_STR_TYPE: */
     else if (put_but_in_rta((RTA *)gvarp,
                      (char *)*sys_var->syspp.sys_str_addr,
                      sys_var->sys_length) != NO_ERROR)
         return(-1);
    }
    return(0);
}

/********************************************************************
*
*         **** DELETE PARTITION EXTERNAL VARIABLE ****
*
*********************************************************************
* 
* FUNCTION:      delete_par_var (pointer to parition entry)
* 
* DESCRIPTION:
*      This module will attempt to delete a partition external variable.
* If a field structure is associated with the partition variable that's
* to be deleted then the partition variable entry will not be deleted.
* The data buffer will be released and the field structure state will
* be set to field changed.
* 
* INPUT:
*     . none
* 
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   06/20/86        R.G.R.          ORIGINAL
*   10/30/87      ML Pardee      Added a line of code to delete
*                      pvarbuf when the memory is freed.
*                      This eliminates a dangling pointer
*                      problem which corrupts memory.
* 
**********************************************************************/

delete_par_var (par_entry)
PVAR FAR *par_entry;
{
#if DEBUG_RUN
    del_extv(par_entry->pvarname, &W->pvar_mid);
    return;
#endif

    /* If there is no field structure associated with this partition
       variable then delete it from the list */
    if (par_entry->pv_field == 0)
     del_extv(par_entry->pvarname, &W->pvar_mid);
    else
    {
     /* There is a field structure associated with this
        partition variable, therfore do not delete par entry */
     if (par_entry->pvarlen)
     {
         free((unsigned char *)par_entry->pvarbuf);
         par_entry->pvarlen = 0;
     par_entry->pvarbuf = 0;        /* ML Pardee, 10/30/87 */
     }
     
     /* Show to DM & KM that the field has changed */
     
     par_entry->pv_field->s |= (FIELD_CHANGED /* | FIELD_UPDATE Algis */);
     Fields_changed_by_api = ENABLE;
    }
}

/********************************************************************
*
*         **** DELETE GLOBAL EXTERNAL VARIABLE ****
*
*********************************************************************
* 
* FUNCTION:      delete_glb_var (pointer to global entry)
* 
* DESCRIPTION:
*      This module will attempt to delete a global external variable.
* 
* INPUT:
*     . none
* 
* OUTPUT:
*    .none
* 
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   06/??/86        R.G.R.          ORIGINAL
* 
**********************************************************************/

delete_glb_var(glb_entry)
GVAR FAR *glb_entry;
{
    /* Free up global var data buffer */
    if (glb_entry->gvarlen)
    {
     free (glb_entry->gvarbuf);
     glb_entry->gvarlen = 0;
    }

    /* If global variable is a system variable then update it */
    if (glb_entry->glbvnam <= SYS_VAR_NUM)
     proc_ext_var(GLB_TYPE, (unsigned char FAR *)glb_entry);
}

/*---------------------------------------------------------------------
**  int proc_pev(pev_entry)
**  PVAR FAR *pev_entry;
**
**   If the given PEV has a field associated with it, indicate that
**   the field has been changed.
**-------------------------------------------------------------------*/

proc_pev(pev_entry)
PVAR FAR *pev_entry;
{
#if DEBUG_RUN
    return;
#endif
    
    /* Set field structure state only if a field structure entry is
       attached to this partition variable */
    if (pev_entry->pv_field)
    {
     /* Show to DM & KM that the field has changed */
     pev_entry->pv_field->s |= (FIELD_CHANGED /* | FIELD_UPDATE Algis */);
     Fields_changed_by_api = ENABLE;
    }
    
#if 0
/* How can we delete a PEV that has a field associated with it?  What
   about format, which calls proc_pev() before the field is modified? */
    /* delete nullified PEV's */
    if (!pev_entry->pvarlen)
     delete_par_var(pev_entry);
#endif
}

/*---------------------------------------------------------------------
**  int proc_gev(gev_entry)
**  GVAR FAR *gev_entry; 
**
**   If the given GEV is a system variable, then update it with the
**   passed contents.
**------------------------------------------------------------------*/

proc_gev(gvarp)
GVAR FAR *gvarp;
{
    unsigned char *temp_addr;
    systbl *sys_var;         /* Define pointer to system variable table */
    RTA rta;

#if DEBUG_RUN
    return;
#endif

    /* If global variable is a system variable then update it. 09:18:86 R.G.R */
    if (gvarp->glb_state & SYS_VARIABLE)
    {
     sys_var = &sys_var_table[gvarp->glbvnam];
     
     /* If system variable is an integer then convert */
     if (sys_var->sys_type == SYS_INT_TYPE)
         *sys_var->syspp.sys_address =
              asciibin(gvarp->gvarbuf, gvarp->gvarlen);

     /* Move data if system variable is a string (must be) */
     /* NOTE: because sys_var[] is not of type RTA, we must "cast it"
        sys_var[] has a char ** instead of a char * for the buffer,
        why, who can say? */
     else /* if (sys_var->sys_type == SYS_STR_TYPE) */
     {
         rta.strptr = (char *)*sys_var->syspp.sys_str_addr;
         rta.strlngth = sys_var->sys_length;

         put_buf_in_rta(&rta, gvarp->gvarbuf,
                     gvarp->gvarlen);

         (char *)*sys_var->syspp.sys_str_addr = rta.strptr;
         sys_var->sys_length = rta.strlngth;
     }
    }
    
    /* delete nullified non-system GEV's */
    else if (!gvarp->gvarlen && gvarp->glb_state == NORMAL)
     del_extv(gvarp->glbvnam, glbmid);
}

proc_ext_var(optype, extv_ptr)
unsigned char optype;
unsigned char FAR *extv_ptr;
{
    if (optype == PAR_TYPE)
     proc_pev((PVAR FAR *)extv_ptr);
    else
     proc_gev((GVAR FAR *)extv_ptr);
}

/*---------------------------------------------------------------------
**  ins_gev(name)
**  unsigned int name;
**
**   Do an ins_extv() for a GEV.
**-------------------------------------------------------------------*/

int FAR *loc_gev(name)
unsigned int name;
{
    return(loc_extv(name, glbmid));
}

int FAR *ins_gev(name)
unsigned int name;
{
    return(ins_extv(name, glbmid, sizeof(GVAR)));
}

int FAR *loc_pev(name)
unsigned int name;
{
    return(loc_extv(name, &W->pvar_mid));
}

int FAR *ins_pev(name)
unsigned int name;
{
    return(ins_extv(name, &W->pvar_mid, sizeof(PVAR)));
}

/********************************************************************
*
*         **** PROCESS INSERT INTO SPIDER WEB ****
*
*********************************************************************
* 
* FUNCTION:      insert_web(pointer to field structure)
* 
* DESCRIPTION:
*      This module will insert a new field structure within the field
* spider web.
* 
* INPUT:
*     . none
* 
* OUTPUT:
*    .none
* 
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   06/20/86        R.G.R.          ORIGINAL
* 
**********************************************************************/

insert_web (field_entry)
FIELD FAR *field_entry;
{
    WINDOW FAR *wind;
    FIELD  FAR *field;
    FIELD FAR *prev_field;
    REG int x1,x2,y1,y2;
    
    wind = field_entry->owning_window;
    x1 = field_entry->ox;
    y1 = field_entry->oy + field_entry->dy;
    
    if (field = wind->first_field)
    {
     do
     {
         y2 = field->oy + field->dy;
         if (y2 > y1)
         {
            prev_field = field;
            field = field->next;
            continue;
         }
         else
         {
            if (y2 == y1)
            {
            x2 = field->ox
            if (x2 > x1)
              break;
            prev_field = field;
            field = field->next;
            continue;
          }
          else
          {
              break;
          }
         }
      } while (field && field != wind->first_field);
      if (field)
      {
         if (field == wind->first_field)
          wind->first_field = field_entry;
         field_entry->next = field;
         field_entry->previous = field->previous;
         field->previous = field_entry;
         if (prev_field)
            prev_field->next = field_entry;
      }
      else
      {
         prev_field->next = field_entry;
         field_entry->previous = prev_field;
      }
    }
    else
     wind->first_field = field_entry;
}
         
/********************************************************************
*
*         **** Convert coordinates from NAPLPS to TRINTEX formats
*
*********************************************************************
* 
* FUNCTION:    tx2nx(tcord) - Convert x-cord from TRINTEX to NAPLPS.
*         nx2tx(tcord) - Convert x-cord from NAPLPS  to TRINTEX.
*         ty2ny(tcord) - Convert y-cord from TRINTEX to NAPLPS.
*         ny2ty(tcord) - Convert y-cord from NAPLPS  to TRINTEX.
* 
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   01/09/87        R.D.C.          ORIGINAL
* 
**********************************************************************/
/* Convert Y-cord from TRINTEX to NAPLPS form */

unsigned char ty2ny(tcord)
unsigned char tcord;   /* TRINTEX form of Y coordinate */
{
    /* Return NAPLPS form of Y */
    return((unsigned char)(200 - (int)tcord * 10));
}

/*******************************************************************/
/* Convert Y-cord from NAPLPS  to TRINTEX form */

unsigned char ny2ty(ncord)
unsigned char ncord;   /* NAPLPS form of Y coordinate  */
{
    /* Return TRINTEX form of Y */
    return((unsigned char)((200 - (int)ncord)/ 10));  /*LB 2/15/88*/
}

/*******************************************************************/
 /* Convert X-cord from TRINTEX to NAPLPS form */
unsigned char tx2nx(tcord)
unsigned char tcord;   /* TINTEXT form of X coordinate */
{
    /* Return NAPLPS form of X */
    return((unsigned char)(((int)tcord - 1) * 6 + 8));
}

/*******************************************************************/
/* Convert X-cord from NAPLPS  to TRINTEX form */

unsigned char nx2tx( ncord )
unsigned char ncord ;    /* NAPLPS form of X coordinate     */
{
    /* Return TRINTEX form of X */
    return((unsigned char)(((int)ncord - 8) / 6 + 1));
}

/*******************************************************************/

extern systbl sys_var_table[];
extern ICB_TIME clock_icb;
extern TIME the_time;

/********************************************************************
*
*          **** GET SYSTEM GLOBAL VARIABLE *****
*
*********************************************************************
* 
* FUNCTION:      get_sys_gev (name of GEV, pointer to a GCB)
* 
* DESCRIPTION:
*      Get contents of system global variable GEV into GCB.
*     Specifically, find the system variable associated with the
*     named GEV and copy its value pointer and length into the
*     value and length members of the GCB.
* 
*         4/16/87: put in call to time_and_date().  See global_proc() infra.
* 
* INPUT:
*     Arg1: number of the GEV entry in sys_var_table[],
*           indexing from 1.
* 
*     Arg2: pointer to the GCB.
* 
* OUTPUT:   None
* 
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   09/21/86        R.G.R.          ORIGINAL
*    4/15/87        Les H           Tidying Up
*    4/16/87        Les H           Put in time_and_date()
**********************************************************************/

get_sys_gev(gev_name, act_gcb)
unsigned int gev_name;
GCB *act_gcb;
{
    extern systbl sys_var_table[];  /* global table of system variables */
    
    /* Update GCB according to type */
    if (sys_var_table[--gev_name].sys_type == SYS_INT_TYPE)  /* integer type */
     act_gcb->pointer =
           (unsigned char *) sys_var_table[gev_name].syspp.sys_address;
    else /* must be string type */
    {
     if (gev_name == SYS_DATE - 1 || gev_name == SYS_TIME - 1)
     {
         if (time_and_date(
              (char *) *sys_var_table[SYS_DATE - 1].syspp.sys_str_addr,
              (char *) *sys_var_table[SYS_TIME - 1].syspp.sys_str_addr,
              &clock_icb, &the_time) == NG)
          fatal_error(API_PROV, BAD_DATE_TIME);
     }
     act_gcb->pointer = (char *)*sys_var_table[gev_name].syspp.sys_str_addr;
    }
    act_gcb->length = sys_var_table[gev_name].sys_length;
}

/********************************************************************
*
*          **** PUT SYSTEM GLOBAL VARIABLE *****
*
*********************************************************************
* 
* FUNCTION:      put_sys_gev (name of GEV, pointer to a GCB)
* 
* DESCRIPTION:
*      Put contents of GCB into system global variable GEV.
*      If GCB has no contents (length is zero), release the
*      GEV via free().
* 
*          Logic of the routine:
* 
*   if system variable is integer type
*      copy the integer from GEV to sys var
*   else   # must be a string type
*      if GEV string size == sys var string size
*      if sys var has a string
*         free sys var's string
*      if GEV string length is not zero
*         if malloc() can allocate a new buffer
*         copy address returned by malloc() into sys var's
*            buffer pointer
*         copy string from GEV into new buffer
*         copy GEV string length into sys var's string length
*         else  # malloc() failed
*         error return
*      else   # GEV string size == sys var string size
*      copy string from GEV into old buffer
* 
* INPUT:
*    Arg1: numer of the GEV entry in sys_var_table[],
*          indexing from 1.
* 
*    Arg2: pointer to the GCB
* 
* OUTPUT:  Return -1 for error, else 0.
*
* COMMENTS:
* 
*      WARNING: assumes that system variable strings are always
*     stored in buffers dynamicalle allocated by malloc().
* 
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   09/21/86        R.G.R.          ORIGINAL
*    4/15/87        Les H           Tidying Up
*    6 May '87          Algis            When moving a null in the Gev
*                         length of gev must be set to zero
*                         so that we know that it (the pointer
*                         to the gev) was freed!!
**********************************************************************/

int put_sys_gev(gev_name, act_gcb)
unsigned int gev_name;
GCB *act_gcb;
{
    char *malloc();
    extern systbl sys_var_table[];  /* global table of system variables */
    
    /* Update System variable according to type */
    
    /* if System variable is a integer type. set sys_address to act_pointer
       and return */
    
    if (sys_var_table[--gev_name].sys_type == SYS_INT_TYPE)
    {
     *sys_var_table[gev_name].syspp.sys_address = 
                       * (unsigned int *) act_gcb->pointer;
     return 0;
    }
    
    /* Sys variable type must be a string type */
    
    /* if act_gcb length does not equal Sys_var length the free buffer to
       Sys var.  Remember to set length to zero */
    
    if (act_gcb->length != sys_var_table[gev_name].sys_length)
     if (sys_var_table[gev_name].sys_length)
     {
         free((char *) *sys_var_table[gev_name].syspp.sys_str_addr);
         sys_var_table[gev_name].sys_length = 0;
         *sys_var_table[gev_name].syspp.sys_str_addr = NULL;
     }
    
    /* If act_gcb has a string ie. length in non zero
         get a buffer for system variable if needed
              if malloc error bad return
         copy from act_gcb to system variable pointer
         set system variable length to act_gcb length  */
    
    if (act_gcb->length)
    {
     if(!(sys_var_table[gev_name].sys_length))
     {
         if ((*sys_var_table[gev_name].syspp.sys_str_addr =
           (unsigned int *) malloc(act_gcb->length)) == NULL)
         {
          buf_err();               /* malloc error */
          return -1;               /* bad return  */
         }
     }
     memcpy((char *) *sys_var_table[gev_name].syspp.sys_str_addr,
                     (char *) act_gcb->pointer, act_gcb->length);
     sys_var_table[gev_name].sys_length = act_gcb->length;
    }
    return(0);                     /* good return */
}