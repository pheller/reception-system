/*API     = 1.1.2    LAST_UPDATE = 1.1.2    */
/*--------------------------------------------------------------------
**    TRCONTRL.C      Control Transver Verbs
**-------------------------------------------------------------------
**
**   call()           procedure call
**   link()           LINK
**   transfer()     TRANSFER
**   rtn()            RETURN from procedure call
**   godep()        GOTO_DEPENDING_ON
**   procexit()     EXIT from API driver
**   procexit_rc()  EXIT from API driver with return code
**   wait()           WAIT, suspend API process
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.         ORIGINAL
**  09/18/86        R.G.R.         CHANGE pushoper ()
**  09/21/86        R.G.R.         CHANGE link ()
**  09/21/86        R.G.R.         CHANGE PROC_RTN ()
**  07/28/87        SLW       Rewrite
**  01/05/88       RDC   Changes for WAIT verb
**  01/15/88       RDC   Made WAIT verb use LOS timer.
**  07/1?/88       SLW   Fix object-id allocation but not freed
**-------------------------------------------------------------------*/

#include <TRCONTRL.LNT>  /* lint args (from msc /Zg) */
#include <stdio.h>
#include <stdlib.h>
#include <SMDEF.IN>      /* Service manager definitions */
#include <DEBUG.IN>      /* Debugger compiler switch */
#include <RTASTR.IN>          /* Run time array structure */
#include <RTADEF.IN>          /* Run time array definition.*/
#include <OPERDEF.IN>         /* Number of operand definitions */
#include <OPERTSTR.IN>        /* Operand definitions */
#include <INSERR.IN>          /* Instruction error return codes */
#include <OBJUNIT.IN>         /* Object unit structures */
#include <SMPPT.IN>
#include <ISCB.IN>       /* Interpreter control block */
#include <IPCB.IN>       /* Intrepreter parm. control block */
#include <GEB_DEFS.IN>        /* uses SYS_NAVIGATE_OBJ_ID*/
#include <APIUTIL.IN>         /* API utility externs */
#include <NONFATAL.IN>

/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/
extern unsigned char FAR *begprgm;  /* Ptr to begining of program */
extern unsigned char begprcnt;         /* Packet offset to 1'st instr */
extern ISCB *instack;             /* Current interpreter stack ptr */
extern ISCB instck[ISCB_SIZE + 1];  /* Interpreter stack */
extern ISCB *level0;              /* Return level0 stack pointer */
extern unsigned char apirtn;      /* API system return flag */
extern IPCB *pstack;              /* Current parameter stack pointer */
extern struct rta rtatbl[];       /* Run time array */

extern unsigned char api_exit_code;/* Api exit code */

#if (DEBUG)
extern unsigned char Debug_flag;   /* Debug enable flag */
#endif

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/
#define ENABLE   1         /* Enable external variable search */
#define DISABLE   0        /* Disable external variable search */
#define HIBYTE   1         /* Used to get MSB of ins index */
#define LWBYTE   0         /* Used to get LSB of ins index */
#define INXSIZE   2        /* Transfer offset byte size */
#define INXSIZE   2        /* Size of stream_ptr */

#define RTN      1         /* System return indicators */
#define NORTN    0

#define NO_WARN_MSG    1       /* Don't warn TBOL prog */
#define NULL_MODE      0       /* Don't switch to text mode */

#define LSB    0
#define MSB    1

#define LINKVERB     0
#define TRANSFERVERB 1

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/

/* Union of integer and character array for fast hi byte access */
union intary
{
    unsigned char cary[2];          /* MSB and LSB of ivar */
    unsigned int  ivar;        /* Integer variable */
};
union sintary
{
    char scary[2];        /* MSB and LSB of ivar */
    int  sivar;           /* Integer variable */
};
/*--------------------------------------------------------------------
**  pushoper(nbroper, operand, save_flag)
**
**   Save all of the parameter registers by pushing them onto the
**   parameter stack.  nbroper is the number of operands to move into
**   the parameter registers (P1-P8).  nbroper will also be converted
**   to ascii and placed in the P0 register.  operand is the first
**   operand to move to the parameter registers.
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.         ORIGINAL
**  09/16/86        R.G.R.         CHANGE char cnt & count to int
**-------------------------------------------------------------------*/

pushoper(nbroper, operand, save_flag)
unsigned int nbroper;         /* Holds nbr of instruction operands */
OPER *operand;           /* Pointer to operand table entry */
unsigned int save_flag;  /* Save old parameters flag */
{
    RTA *parm;               /* Pointer to parameter registers */
    REG unsigned int count;       /* Counter used in loops.09:18:86 R.G.R.*/
    unsigned int cnt;             /* Counter used in loops.09:18:86 R.G.R.*/
    unsigned char FAR *tbuf;      /* Temporary buffer */
    unsigned char FAR *dataptr;     /* Temp. pointer to data buffer */
    IPCB *tpstack;           /* Current temp stack pointer */

    /* Loop to move current contents of paramter registers onto
       parameter stack */
    for (parm = &rtatbl[LOCP0], count = PREGS; count; --count, parm++)
     put_buf_in_rta((RTA *)--pstack, parm->strptr, parm->strlngth);

    /* Convert number of operands to ascii and store within
       parameter 0 register */
    put_int_in_rta(nbroper, &rtatbl[LOCP0], ASC_TYPE);

    /* Loop to move operand values into parameter registers and convert
       to ascii if necessary */
    for (parm = &rtatbl[LOCP0+1]; nbroper--; ++parm, ++operand)
    {
     /* Convert int operand to ascii */
     if (operand->op_type == INT_TYPE)
         put_int_in_rta(*((unsigned int FAR*)operand->buffer),
                  (RTA *)parm, ASC_TYPE);

     /* Convert bcd operand to ascii */
     else if (operand->op_type == DEC_TYPE)
         put_bcd_in_rta(operand->buffer, (RTA *)parm, ASC_TYPE);

     /* Fetch parameter register specially */
     else if (operand->rtoff.irtoff >= LOCP0 &&
           operand->rtoff.irtoff <  LOCP0+PREGS)
     {
         tpstack = pstack ((LOCP0+PREGS-1) - operand->rtoff.irtoff);
         put_buf_in_rta((RTA *)parm, tpstack->parm_buffer,
                         tpstack->parm_length);
     }

     /* Operand is an ascii string get buffer and move data */
     else
         put_buf_in_rta((RTA *)parm, operand->buffer, operand->buflen);
    }

    if (!save_flag)
     for (count = PREGS; count--; )
         make_buf_in_rta((RTA *)pstack++, 0);
}

/*--------------------------------------------------------------------
**  call()
**
**   Transfer TBOL interpreter control to a different TBOL procedure.
**   The first byte following the call opcode contains the number of
**   operands that the called procedure takes.  Up to eight operands
**   are allowed.  The operands are put in the parameter registers P1
**   - P8.  The original contents of all the parameter registers are
**   saved on the parameter stack and are restored when the procedure
**   returns.
**
**  Instruction format:
**
**   Inst.        byte1    operand#1  operand#2  ... operand#8
**   -------------------------------------------------------
**    call         P0           P1   P2   ...      P8
**          (# of operands)
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

call()
{
    ISCB *instck;        /* Temp. ptr to interpreter stack */
    unsigned char mode;  /* Instruction mode byte */
    unsigned int nbroper;     /* Holds nbr of instruction operands */
    union sintary ip_inx;     /* Used to get LSB & MSB of call offset */

#ifdef PH
phook(77);
#endif

    /* Get opcode mode byte */
    mode = getmode();

    /* Get number of operands within this instruction */
    r_getbyte();
    nbroper = *ip;

    /*d Perform range test on number of operands */

    /* Call to resolve operands enabling external searches */
    if ((api_rtn_code = procoper(nbroper, mode, 0)) != APIOK)
     /* Return control back to reception system */
     return;

    /* Call to update parameter register with new operand values */
    pushoper(nbroper, &optbl[0], 1)

    /* Transfer control to new module */
    /* Get MSB of program index */
    ip_inx.scary[HIBYTE] = *ip;

    /* Get LSB of program index */
    r_getbyte();
    ip_inx.scary[LWBYTE] = *ip;

    /* Save current interpreter instruction and program
       pointers and indexs by pushing there values onto
       the interpreter stack */
    r_getbyte();
    --instack;
    instck = instack;

#if DEBUG_ALONE
       /* Save current instruction pointer */
       instck->insc_save_stream = (Objstream)ip;
#endif

    /* Push current packet index into stack */
    if ((instck->insc_offset = Obj_get_offset(stream_ptr)) < 0)
    {
     api_rtn_code = CALL_ERR;
     return;
    }

    /* Push stream pointer onto stack */
    instck->insc_save_stream = stream_ptr;

    /* Push opcode of next instruction to execute onto the stack */
    instck->inscopcode = *ip;

    /* Show that no program was linked */
    instck->insc_stream = 0;

    /* Adjust program index */
    ip_inx.sivar -= 1;

    /* Set instruction pointer to new location */
    if ((api_rtn_code = Obj_seek(stream_ptr, ip_inx.sivar, 1)) != APIOK)
         return;
    r_getbyte();

#ifdef PH
phook(75,&instck->insc_offset,&ip_inx.sivar);
#endif

}

/*--------------------------------------------------------------------
**  start_of_prog()
**
**   Adjust program stream to start of program.
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

#define TBOL_CODE_OFFSET    22      /* offset of tbol code in an object */
#define TBOL_VERSION_OFFSET 2
#define TBOL4_0  0
#define TBOL5_0  1

start_of_prog()
{
    int offset;              /* Instruction offset. */
    int tmp;                 /* temporary */

    /* Get version -- TBOL 4.22 or TBOL 5.0? */
    if (Obj_seek(stream_ptr, TBOL_CODE_OFFSET+TBOL_VERSION_OFFSET, 0))
     return(NO_PROCESS);

    procop_getbyte();

    if (*ip & 0xf0)     /* not 4.22 */
     fatal_error(1, 18); /* not 5.0 compatible */

    procop_getbyte();        /* Get offset to program entry point. */

    offset = *ip;

    /* Adjust stream pointer to point to start of program. */
    if (Obj_seek(stream_ptr, offset+TBOL_CODE_OFFSET, 0) ||

    /* Get first opcode */
     Get_obj_byte(stream_ptr, ip))
     return(NO_PROCESS);

    return(NO_ERROR);
}

/*--------------------------------------------------------------------
**  link_or_transfer(verb)
**
**   Utility for link() and transfer(), q.v.
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.         ORIGINAL
**  09/21/86        R.G.R.         MODIFIED NUMBER OF OPERANDS
**                       PASSED TO PUSHOPER.
**-------------------------------------------------------------------*/

link_or-tran(verb)
unsigned int verb;
{
    ISCB *instck;       /* Temp. ptr to interpreter stack */
    unsigned char mode;     /* Instruction mode byte */
    unsigned int nbroper;   /* Holds nbr of instruction operands */
    union intary ip_inx;    /* Used to get LSB & MSB of call offset */
    struct oper *obj_oper;  /* Pointer to object id operand */
    char obj_id_ptr[20];    /* Pointer to object-id 7-12-88 SLW */

    /* Get opcode mode byte */
    mode = getmode();

    /* Get number of operands within this instruction */
    r_getbyte();
    nbroper = *ip;

    /*d Perform range test on nmber of operands */

    /* Call to resolve operands enabling external searches */
    if ((api_rtn_code = procoper(nbroper, mode, 0)) != APIOK)
     /* Return control back to reception system */
     return;

#if DEBUG_ALONE
     return;
#endif

    if (verb == LINKVERB)
    {
     /* Save current interpreter instruction and program
        pointers and indexs by pushing there values onto 
        the interpreter stack */
     --instack;
     instck = instack;

     /* Push current packet index into stack */
     if ((instck->insc_offset = Obj_get_offset(stream_ptr)) < 0)
     {
         api_rtn_code = LINK_ERR;
         return;
     }


#ifdef PH
phook(72,&instck->insc_offset,(&optbl[0])->buffer);
#endif

     /*Push stream pointer onto stack */
     instck->insc_save_stream = stream_ptr;

     /* Push opcode of next instruction to execute onto the stack */
     instck->inscopcode = *ip;


#if (DEBUG)
    if (Debug_flag)
     /* Save debugger environment */
     save_tbol_prog ();
#endif

     /* Call to link to new program object.
        Saves and sets up current instruction pointer */

     obj_oper = &optbl[0];
     if (obj_oper->buflen == 0)
      return;

      /* changed to memcpy 7-11-88 SLW */
      memcpy(obj_id_ptr, obj_oper->buffer, obj_oper->buflen);

      if (verb == TRANSFERVERB)
      {

#ifdef PH
phook(73,obj_id_ptr);
#endif

       /* New code to fix transfer to spec. algis 2 March '87 */
       /* Back up through stack to pass all calls made till we find a link or
          we are nack at level0 */
       while (instack != level0)
       {
           if (instack->insc_stream)
            break;
           ++instack;
       }

       /* If we are at level0 then make an entry for the transfer.  Show
          that this was a transfer by placing a NULL in insc_save_stream */
       if (instack == level0)
       {
           --instack;
           instack->insc_save_stream == NULL;
       }
       else
           Obj_deaccess(instack->insc_stream);
      }

      if (GetObjectStable((Objectid *)obj_id_ptr, &stream_ptr) != 0)
      {
       api_rtn_code = BUF_ERR;
       return;
      }

      /* PUsh this new program object stream pointer on tsack */
      instack->insc_stream = stream_ptr;

      /* Call to update parameter register with new operand values */
                  /* save parm bug w/transfer on 09:21:86 R.G.R */
      pushoper(nbroper - 1, &optbl[1], verb==LINKVERB);

      /* Get first instruction opcode for this program */
      start_of_prog();

#if DEBUG
    if (DEBUG_flag)
     /* Save debugger environment */
     get_tbol_prog();
#endif
}

/*--------------------------------------------------------------------
**  link()
**
**   API LINK verb. Temporarily transfer TBOL interpreter control to
**   a different program object.  The first byte following the
**   instruction opcode contains the number of operands that the
**   linked program takes.  Up to seven operands are allowed.  The
**   operands are put in the parameter registers P1 - P7.  The
**   original contents of the parameter registers are saved on the
**   parameter stack and are restored when the linked to program
**   returns.

**  Instruction format:
**
**   Inst.        byte1    operand#1  operand#2  operand#3 ... operand#8
**   -----------------------------------------------------------------
**    link         P0        object-id    P1      P2     ...    P7
**          (# of operands)
**
**-------------------------------------------------------------------*/

link()
{

#ifdef PH
phook(78);
#endif

    link_or_tran(LINKVERB);

#ifdef PH
phook(79);
#endif

}

/*--------------------------------------------------------------------
**  transfer()
**
**   API TRANSFER verb.  Temporarily transfers TBOL interpreter control to
**   a different program object.  The first byte following the
**   instruction opcode contains the number of operands that the
**   linked program takes.  Up to seven operands are allowed.  The
**   operands are put in the parameter registers P1 - P7.  The
**   original contents of the parameter registers are not saved
**   and can never be restored.
**
**  Instruction format:
**
**   Inst.        byte1    operand#1  operand#2  operand#3 ... operand#8
**   -----------------------------------------------------------------
**    transfer   P0     object-id    P1      P2     ...    P7     
**          (# of operands)
**
**-------------------------------------------------------------------*/

transfer()
{

#ifdef PH
phook(84);
#endif

    link_or_tran(TRANSFERVERB);

#ifdef PH
phook(85);
#endif

}

/*--------------------------------------------------------------------
**  set_rtn_code()
**
**   Set the return code so that it can be examined after a exit or
**   return.
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

set_rtn_code()
{
    /* Initialize pointer to operand */
#define exit_code (&optbl[0])

    Sys_rtn_code = get_int_from_rta((RTA *)exit_coe, exit_code->op_type);
}

/*--------------------------------------------------------------------
**  proc_rtn(rtn_code_flag)
**
**   API RETURN verb.  Transfer TBOL interpreter control back to the
**   calling program.  The interpreter driver variables are popped
**   from the interpreter stack and instruction execution commences
**   immediatly after the call instruction.
**
**  Instruction format:
**
**   Instruction    [operand]
**   -----------    ---------
**      rtn           [return code]
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.          ORIGINAL
**  09:21:86        R.G.R.          MOVED PARAMETER REGISTER
**                        RESTORATION CODE TO NEW LOCAL.
**-------------------------------------------------------------------*/

proc_rtn(rtn_code_flag)
unsigned int rtn_code_flag;
{
    ISCB *instck;        /* Temp. ptr to interpreter stack */
    REG unsigned int count;   /* temp counter */
    struct rta *parm;         /* Pointer to parameter registers */

    if (rtn_code_flag == ENABLE)
     /* Call to set return code */
     set_rtn_code();

    /* Abort if stack pointer is at level 0 */
    if (instack >= level0)
    {
     apirtn = RTN;
     return;
    }

    /* Restore current interpreter instruction and program
       pointers and indexs by popping there values from
       the interpreter stack */

    /* Initialize pointer to interpreter stack. 09:21:86 R.G.R */
    instck = instack;

#ifdef PH
phook(74,instack->insc_stream);
#endif

    /* Deaccess this program if it was linked from another */
    if (instck->oinsc_stream)
    {
     Obj_deaccess (instck->insc_stream);

     /* check if transfer done at level0 */
     if (instck->insc_save_stream == NULL)
     {
         ++instack;
         apirtn = RTN;
         return;
     }

#if DEBUG
     stream_ptr = instck->insc_save_stream;
     if (Debug_flag)
         res_tbol_prog(NULL_MODE);
#endif
    }

    /* Return from CALL verb
       Get current program packet index */
    stream_ptr = instck->insc_save_stream;

    /* Get instruction pointer from stack */
    *ip = instck->inscopcode;

    /* Reste stream pointer to return address */
    if (Obj_seek(stream_ptr, instck->insc_offset, 0))
    {
     api_rtn_code = RTN_ERR;
     return;
    }

    /* Adjust interpreter stack pointer */
    ++instack;

    /* Loop to restore parameter registers */
    for (parm = &rtatbl[LOCP0+8], count = PREGS; count--; pstack++, parm--)
    {
     put_buf_in_rta((RTA *)parm, pstack->parm_buffer, pstack->parm_length);
     make_buf_in_rta((RTA *)pstack, 0);
    }

#ifdef PH
phook(80);
#endif

}

/*--------------------------------------------------------------------
**  rtn_parm()
**
**   API RETURN verb.  See proc_rtn() above.
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.          ORIGINAL
**-------------------------------------------------------------------*/

rtn_parm()
{
    /* Call to process reutrn and enable return code */
    proc_rtn(ENABLE);

    if (Sys_rtn_code && apirtn == RTN)
     /* Stop further processing */
     api_exit_code = STOP;
}

/*--------------------------------------------------------------------
**  rtn()
**
**   API RETURN verb.  See proc_rtn() above.
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.          ORIGINAL
**-------------------------------------------------------------------*/

rtn ()
{
    /* Call to process return instruction */
    proc_rtn(DISABLE);
}

/*--------------------------------------------------------------------
**  godep()
**
**   API GOTO_DEPENDING_ON verb.  Do a TBOL GOTO to a location
**   indicated by the index operand.  If the index value is less than
**   one or is greater than the number of labels then the interpreter
**   will ignore the GOTO_DEPENDING_ON.  Otherwise the interpreter
**   will use index to index into the jump table following the first
**   operand and jump to the relative offset.
**
**  Instruction format:
**
**   Inst  operand#1  byte     word     word    ...  word
**   ----------------------------------------------------------
**   godep      index      #labels   label#1  label#2  ... label#n
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
**  02/25/86        R.G.R.          ORIGINAL
**-------------------------------------------------------------------*/

godep()
{
    int ipjmp;               /* Used to get LSB and MSB of call offset */
    REG int num;             /* Index value */
    REG unsigned char labelcnt;     /* Number of labels for this instr */

    /* Convert index (operand#1) if ascii field */
#define oper1 (&optbl[0])

#if (DEBUG || DEBUG_ALONE)
    /* Validate operands */
    if (oper1->op_type == DEC_TYPE || oper1->op_type == LIT_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    }
#endif

    num = get_int_from_rta((RTA *)oper1, oper1->op_type);

    /* Get number of labels for this instruction */
    labelcnt = *ip;

    /* Test index value for less than 0 or greater then
       # of labels */
    if (num <= 0 || num > labelcnt)
    {
     /* Fall through to next instruction for processing */
     if (api_rtn_code = Obj_seek(stream_ptr, labelcnt*2, 1))
         return;
    }
    /* Transfer control to label reference by index */
    else if ((api_rtn_code = Obj_seek(stream_ptr, num*2-2, 1)) ||
          (api_rtn_code = get_obj_word((struct Objcontext *)stream_ptr, (char
     fatal_error(1,15);

    /* load the next byte */
    r_getbyte();

#undef oper1
}

procexit_rc()
{
    /* Call to set return code */
    set_rtn_code ();

    if (Sys_rtn_code)
     /* Stop further processing */
     api_exit_code = STOP;

    apirtn = RTN;
}

procexit()
{
    apirtn = RTN;
}

/*--------------------------------------------------------------------
**  wait()
**
**   API WAIT verb. Issue a suspend so other tasks can run.
**
**  Instruction format:
**
**   Inst
**   -----
**   wait
**
**    DATE           PROGRAMMER      REASON
**-------------------------------------------------------------------
*   03/03/86        R.G.R.          ORIGINAL
*   01/05/88        R.D.C Quick and dirty to make WAIT verb
*                     take one half second.
**-------------------------------------------------------------------*/

wait()
   {

   static int api_timer_flag ;

   int timer_id ;

   timer_id = CREATE_TIMER() ;

   SET_TIMER ( timer_id, 5, &api_timer_flag, 11 ) ;

   api_timer_flag = 1 ;   /* Set timer global */
   while ( api_timer_flag != 0 )
      driver() ;         /* Attempt to do COMM work. */

   DELETE_TIMER( timer_id ) ;

   }
