/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    UP_CASE.C       API Utility functions
**--------------------------------------------------------------------
**
**  length()    LENGTH verb
**  up_case()   UPPERCASE verb
**  imin(a, b)
**  imax(a, b)
**  api_fil()
**  r_getbyte()
**  getmode()
**  get_buf(len)
**  copy_buf(buf, len)
**  put_buf_in_rta(rtaptr, buf, len)
**  make_buf_in_rta(rtaptr, len)
**  put_int_in_tra(i, rtaptr, type)
**  put_bcd_in_rta(bcd, rtaptr, type)
**  put_asc_in_rta(asc, rtaptr, type)
**  get_int_from_rta(rtaptr, type)
**  get_bcd_from_rta(buf, rtaptr, type)
**  get_ascii_from_rta(buf, rtaptr, type)
**
**  07/30/87        SLW       Rewrite and Original
**  11/19/87        SLW       Storing BCD into a BCD register
**                       copied wrong # of bytes
**-------------------------------------------------------------------*/

#include <UP_CASE.LNT>        /* lint args (from msc /Zg) */
#include <stdio.h>
#include <ctype.h>
#include <memory.h>
#include <SMDEF.IN>
#include <DEBUG.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <OPERDEF.IN>
#include <OPERTSTR.IN>
#include <INSERR.IN>
#include <APIUTIL.IN>         /* API utility externs */

/*---------------------------------------------------------------------
**  length()
**
**   The API LENGTH verb.  Puts length of operand 0 in operand 1.
**
**  Instruction format;
**
**   Instruction   operand#1  operand#2
**   ----------------------------------
**     uppercase   string      dest
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  07/27/87        SLW       Rewrite
**-------------------------------------------------------------------*/

length()
{
    put_int_in_rta(optbl[0].buflen, (RTA *)optbl[1].entry, optbl[1].op_type);
}

/*---------------------------------------------------------------------
**  up_case()
**
**   The API UPPERCASE verb.  Convert operand to uppper case.
**
**  Instruction format;
**
**   Instruction   operand#1
**   -----------------------
**     uppercase   string
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  07/28/87        SLW       Rewrite
**-------------------------------------------------------------------*/

up_case()
{
    OPER *result;
    unsigned char FAR *str_ptr;
    REG unsigned int str_len;

    result=&optbl[0];
    if (result->op_type == INT_TYPE ||
     result->op_type == DEC_TYPE)
     return;
    for(str_len=result->buflen,
     str_ptr=result->buffer; str_len-->0 ; str_ptr++)
     *str_ptr = toupper(str_ptr);
}

/*---------------------------------------------------------------------
**  imin(a, b)
**
**   returns less of a and b.
**-------------------------------------------------------------------*/

imin(a, b)
{
    return(a<b?a:b);
}

/*---------------------------------------------------------------------
**  imax(a, b)
**
**   returns greater of a and b.
**-------------------------------------------------------------------*/

imax(a, b)
{
    return(a>b?a:b);
}

/*---------------------------------------------------------------------
**  api_fail()
**
**   Fail API after a malloc failure
**-------------------------------------------------------------------*/

#define OUT_OF_MEM  13       /* see inserr.in */
void api_fail()
{
    api_rtn_code = OUT_OF_MEM;
    fatal_error(1,15);
}

/*---------------------------------------------------------------------
**  r_getbyte()
**
**   Fetch a byte from stream str, put it in byt.  Handle errors for API.
**-------------------------------------------------------------------*/

void r_getbyte()
{
    if (api_rtn_code = Get_obj_byte(stream_ptr, ip))
     fatal_error(1,15);
}

/*---------------------------------------------------------------------
**  getmode()
**
**   Get the mode byte for the current opcode, if any.
**-------------------------------------------------------------------*/

#define COMPLEX 0x80           /* Complex/simple opcode mask. */
unsigned char getmode()
{
    if (*ip & COMPLEX)
    {
     r_getbyte();
     return(*ip);
    }
    else
     return(0);
}

/*---------------------------------------------------------------------
**  get_buf(len)
**
**   Malloc a buffer of size len for the API.
**-------------------------------------------------------------------*/

unsigned char *get_buf(len)
unsigned int len;
{
    unsigned char *bp;

    if (!(bp = (unsigned char *)malloc(len)))
     api_fail();
    return(bp);
}

/*---------------------------------------------------------------------
**  copy_buf(buf, len)
**
**   Malloc a buffer of size len and put buf in it.
**-------------------------------------------------------------------*/

unsigned char FAR *copy_buf(buf, len)
unsigned char FAR *buf;
unsigned int len;
{
    unsigned char FAR *new-buf;     /* Pointer to new buffer. */

    if (!(new_buf = get_buf(len)))
     return(NULL);
    memcpy(new_buf, buf, len);
    return(new_buf);
}

/*---------------------------------------------------------------------
**  put_buf_in_rta(rtaptr, buf, len)
**  RTA *rtaptr;
**  unsigned char FAR *buf;
**  unsigned int len;
**
**   Malloc space for and copy a buffer into the RTA, freeing pointer
**   if necessary.  Don't free dest until used source, since source and
**   dest may be same.  Don't reallocate buffer if old one would do.
**-------------------------------------------------------------------*/

put_buf_in_rta(rtaptr, buf, len)
RTA *rtaptr;
unsigned char FAR *buf;
unsigned int len;
{
    REG int mallen;
    char *fp;

    /* Free up buffer within parameter register. */
    if (rtaptr->strlngth)
    {
     /* see if we can reuse the buffer */
     if (len &&                           /* do we need it? */
         (mallen=_msize(rtaptr->strptr)) >= len &&       /* big enough? */
         (mallen < 30 || mallen==len))       /* not waste of space? */
     {
         /* Store length of buffer */
         rtaptr->strlngth = len;

         /* Copy buffer to RTA, being careful to ignore null requests */
         memcpy(rtaptr->strptr, buf, len);
         return(NO_ERROR);
     }

     /* just free it */
     fp = rtaptr->strptr;
    }

    /* Copy buffer to RTA, being careful to ignore null requests */
    if (len &&
     !(rtaptr->strptr = copy_buf(buf, len)))
    {
     api_fail();
     return(PROG_ERR)R); /* ~0 */
    }

    if (len)
    {

        if (rtaptr->strlngth)  /* For a non-zero length source buffer,
                                * do what we always did. MLP 12/10
                                */
          free(fp);                /* now free dest, which may be source */

        rtaptr->strlngth = len;         /* Store length of buffer */
    }
    else if (rtaptr->strlngth)  /* For a zero length source buffer
                                 * with an existing destination. MLP 12/10
                                 */
        {
          free(fp);                /* Free dest, which may be source. MLP 12/
              rtaptr->strlngth = 0;     /* Clear the length of the buffer. ML
              rtaptr->strptr = (unsigned long) NULL;  /* Ensure no dangling p
                                       * This is for functions outsude of API
                                       * that count on a NULL pointer for the
                                       * identification of non-existant buffe
                                       * MLP 12/10/87
                                       */
    }

    return(NO_ERROR);         /* 0 */
}

/*---------------------------------------------------------------------
**  make_buf_in_rta(rtaptr, len)
**  RTA *rtaptr;
**  unsigned int len;
**
**   Malloc a new rta entry of length len, freeing the old one if
**   necessary.  Don't reallocate buffer if old one would do.
**-------------------------------------------------------------------*/

make_buf_in_rta(rtaptr, len)
RTA *rtaptr;
unsigned int len;
{
    REG int mallen;

    /* Free up buffer within parameter register. */
    if (rtaptr->strlngth)
    {
     /* see if we can reuse the buffer */
     if (len &&                           /* do we need it? */
         (mallen=_msize(rtaptr->strptr)) >= len &&       /* big enough? */
         (mallen < 30 || mallen==len))       /* not waste of space? */
     {
         /* Store length of buffer */
         rtaptr->strlngth = len;

         return(NO_ERROR);
     }

     /* just free it */
     free(rtaptr->strptr0;
    }

    /* Store length of buffer */
    rtaptr->strlngth = len;

    /* Copy buffer to RTA, being careful to ignore null requests */
    if (len &&
     !(rtaptr->strptr = malloc(len)))
    {
     api_fail();
     return(PROG_ERROR); /* ~0 */
    }
    return(NO_ERROR);         /* 0 */
}

/*---------------------------------------------------------------------
**  put_int_in_rta(i, rtaptr, type)
**  unsigned int i;
**  RTA *rtaptr;
**  unsigned char type;
**
**   Store int i into an rtaslot of type type.
**-------------------------------------------------------------------*/

put_int_in_rta(ival, rtaptr, type)
unsigned int ival;
RTA *rtaptr;
unsigned char type;
{
    char tmpbuf[IALEN];
    REG int tmplen;

    /* If destination is INT register, store source directly */
    if (type == INT_TYPE)
     *((unsigned int FAR*)rtaptr->strptr) = ival;

    /* If destination is DEC register convert to bcd and move */
    else if (type == DEC_TYPE)
     int_bcd(ival, rtaptr->strptr);

    /* Else store to ascii slot */
    else
    {
     /* Convert srce_len into ascii */
     tmplen = binascii(ival, tmpbuf);

     /* store buffer to RTA */
     put_buf_in_rta(rtaptr, tmpbuf, tmplen);
    }
}

/*---------------------------------------------------------------------
**  put_bcd_in_rta(bcd, rtaptr, type)
**  unsigned char *bcd;
**  RTA *rtaptr;
**  unsigned char type;
**
**   Store bcd value into an rtaslot of type type.
**-------------------------------------------------------------------*/

put_bcd_in_rta(bcd, rtaptr, type)
unsigned char *bcd;
RTA *rtaptr;
unsigned char type;
{
    char bcdbuf[DALEN];

    /* If destination is INT register, store source directly */
    if (type == INT_TYPE)
     *((unsigned int FAR*)rtaptr->strptr) = bcd_int(bcd);

    /* If destination is DEC register convert to bcd and move */
    else if (type == DEC_TYPE)
     memcpy(rtaptr->strptr, bcd, DECLEN);

    /* Else store to ascii slot */
    else put_buf_in_rta(rtaptr, bcdbuf, bcd_ask(bcd, bcdbuf));
}

/*---------------------------------------------------------------------
**  put_asc_in_rta(asc, rtaptr, type)
**  unsigned char *asc;
**  RTA *rtaptr;
**  unsigned char type;
**
**   Store ascii value into an rtaslot of type type.
**-------------------------------------------------------------------*/

put_asc_in_rta(ascbuf, asclen, rtaptr, type)
unsigned char *ascbuf;
unsigned int asclen;
RTA *rtaptr;
unsigned char type;
{
    /* If destination is INT register, store source directly */
    if (type == INT_TYPE)
     *((unsigned int FAR*)rtaptr->strptr) = asciibin(ascbuf, asclen);

     /* If destination is DEC register convert to bcd and move */
     else if (type == DEC_TYPE)
     {
      if (ask_bcd(ascbuf, asclen, rtaptr->strptr) != OK)
      {
          api_rtn_code = INVALID;
          return(PROG_ERROR);
      }
     }

     /* Else store to ascii slot */
     else put_buf_in_rta(rtaptr, ascbuf, asclen);
     return(NO_ERROR);
}

/*---------------------------------------------------------------------
**  get_int_from_rta(rtaptr, type)
**  RTA *rtaptr;
**  unsigned char type;
**
**   Fetch the int stored at rtaptr
**-------------------------------------------------------------------*/

get_int_from_rta(rtaptr, type)
RTA *rtaptr;
unsigned char type;
{
    return(type == DEC_TYPE ? bcd_int(rtaptr->strptr) :
        type == INT_TYPE ? *((unsigned int FAR*)rtaptr->strptr) :
                     asciibin(rtaptr->strptr, rtaptr->strlngth));
}

/*---------------------------------------------------------------------
**  get_bcd_from_rta(buf, rtaptr, type)
**  unsigned char *buf;
**  RTA *rtaptr;
**  unsigned char type;
**
**   Fetch the bcd contents of the rtaptr, converting from
**   INT or STR types to bcd if necessary.  Use the buffer buf as
**   storage for the bcd string if necessary.
**
**   Return NULL if string cannot be converted to BCD.
**-------------------------------------------------------------------*/

unsigned char *get_bcd_from_rta(buf, rtaptr, type)
unsigned char *buf;
RTA *rtaptr;
unsigned char type;
{
    if (type == DEC_TYPE)
     return(rtaptr->strptr);
    if (type == INT_TYPE)
     int_bcd(*((unsigned int FAR*)rtaptr->strptr), buf);
    else
     if ((api_rtn_code = ask_bcd(rtaptr->strptr,
                    rtaptr->strlngth, buf)) != APIOK)
         return(NULL);
    return(buf);
}

/*---------------------------------------------------------------------
**  get_ascii_from_rta(buf, rtaptr, type)
**  unsigned char *buf;
**  RTA *rtaptr;
**  unsigned char type;
**
**   Place the ascii contents of the traptr into buf, converting from
**   INT to DEC types to ascii if necessary.  Buf MUST be large enough
**   to fit the RTA string.
**-------------------------------------------------------------------*/

get_sacii_from_rta(buf, rtaptr, type)
unsigned char *buf;
RTA *rtaptr;
unsigned char type;
{
    /* Conver INT or DEC register to ascii. */
    if (type <= REG_TYPE)
     rtaptr->strlngth = (type == DEC_TYPE) ?
                  bcd_ask(rtaptr->strptr, buf) :
                  binascii(*((unsigned int FAR*)rtaptr->strptr), buf);
    else
     memcpy(buf, rtaptr->strptr, rtaptr->strlngth);
}
