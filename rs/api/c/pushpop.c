/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*----------------------------------------------------------------------
**    PUSHPOP.C       Push and Pop Verbs
**---------------------------------------------------------------------
**
**  Do the following API instructions:
**
**   PUSH push()
**   POP  pop()
**
**  07/28/87        SLW       Rewrite
**  10/1/87         SLW       Special pop hack to save a malloc
**                       was done wrong; pops into null strings
**                       tried to free an unalloced ptr.
**--------------------------------------------------------------------*/

#include <PUSHPOP.LNT>        /* lint args (from msc /Zg) */
#include <SMDEF.IN>      /* Service Mnaager definitions */
#include <DEBUG.IN>      /* Debugger compiler switch */
#include <OPERDEF.IN>         /* Operand definitions */
#include <OPERTSTR.IN>        /* Operand structure */
#include <RTADEF.IN>          /* Run time array definitions */
#include <RTASTR.IN>          /* Run time array definitions */
#include <INSERR.IN>          /* Instruction error erturn codes */
#include <INSCB.IN>      /* Push/pop stack */
#define RC_BAD_CONVERSION      27  /* from <rtncode.in>, an obsolete file */
#include <GEV_DEFS.IN>         /* API utility externs */

/*----------------------------------------------------------------------
**            Externals
**--------------------------------------------------------------------*/

extern int stack_index;
extern INSCV intstck[];

/*----------------------------------------------------------------------
**  push()
**
**   The API PUSH verb.  The first operand is pushed onto the push/pop
**   stack.    No conversion is done; the type is also pushed.  Will
**   return RC_OK, or RC_STACK_FULL.
**
**  Instruction format:
**
**   Instruction   operand#1
**   -----------------------
**     pop           variable
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  07/28/87        SLW       Rewrite
**--------------------------------------------------------------------*/

push()
{
    struct inscb *ip;

    if (stack_index == (INSCB_SIZE-1))
    {
     Sys_rtn_code = RC_STACK_FULL;
     return;
    }

#define arg (&optbl[0])

    ip = &intstck[++stack_index];

    /* insure ip buffer won't be freed by put_buf_in_rta() */
    ip->buflen = 0;

    /* store to intstck as if it is an RTA entry (buffer, buflen) */
    pub_buf_in_rta((RTA *)ip, arg->buffer, arg->buflen);

    /* store type on stack to give stack entries a general type, like RTA
       entries shoudl have */
    ip->op_type = arg->op_type;

#undef arg
}

/*----------------------------------------------------------------------
**  pop())
**
**   The API POP verb.  The entry at the top of the push/pop stack is
**   popped into the first operand.     A conversion is done if required.
**   Will return RC_OK, RC_STACK_EMPTY, or RC_BAD_CONVERSION.
**
**  Instruction format:
**
**   Instruction   operand#1
**   -----------------------
**     pop           variable
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  07/28/87        SLW       Rewrite
**--------------------------------------------------------------------*/

pop()
{
    RTE FAR *arge;
    struct inscb *ip;

    if (stack_index == END_OF_STACK)
    {
     Sys_rtn_code = RC_STACK_EMPTY;
     return;
    }

#define arg (&optbl[0])
    arge = arg->entry;

    ip = &intstck[stack_index--];
    Sys_rtn_code = RC_OK;
    switch (ip->op_type)
    {
      case INT_TYPE:
     put_int_in_rta(*((int FAR *)ip->buffer), (RTA *)arge, arg->op_type);
     break;
      case DEC_TYPE:
     put_bcd_in_rta(ip->buffer, (RTA *)arge, arg->op_type);
     if (api_rt_code != APIOK)        /* bcd conversion error */
         Sys_rtn_code = RC_BAD_CONVERSION;
     break;
      default:
     /* hack to save a malloc for string variables */
     if (arg->op_type > REG_TYPE)
     {
         if (arge->buflen)         /* added this line 10/1/87 SLW */
          free(arge->buffer);
         arge->buffer = ip->buffer;
         arge->buflen = ip->buflen;
         return;              /* don't free ip->buffer now */
     }
     put_asc_in_rta(ip->buffer, ip->buflen, (RTA *)arge, arg->op_type);
     break;
    }

    /* free poped stack entry */
    free(ip->buffer);

#undef arg
}

