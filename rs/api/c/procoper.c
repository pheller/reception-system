/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    PROCOPER.C      Load API Verb Operands
**--------------------------------------------------------------------
**
**   Resolve the API instruction operands by reading them from the
**   instruction stream into the optbl[] for use by the verb
**   functions.
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  01/31/86        R.G.R.         ORIGINAL
**  03/20/86        A.J.M.         UPDATE
**  07/28/87        SLW       Rewrite
**-------------------------------------------------------------------*/

#include <PROCOPER.LNT>  /* lint args (from msc /Zg) */
#include <stdio.h>       /* Standard I/O for NULL */
#include <SMDEF.IN>      /* Service manager definitions */
#include <OBJUNIT.IN>         /* Object unit structure */
#include <SMPPT.IN>      /* Service manager page template */
#include <OPERDEF.IN>         /* Operand definitions */
#include <OPERTSTR.IN>        /* Operand structure */
#include <RTASTR.IN>          /* Run time array structure */
#include <RTADEF.IN>          /* Run time array definitions */
#include <INSERR.IN>          /* Instruction error return codes */
#include <GEV_DEFS.IN>        /* GEV definitions */
#include <APIUTIL.IN>         /* api utility externs */

/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/
extern RTA rtatbl[];         /* Run time array */

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/
#define MDOPER1      0x80         /* Mode mask for operand #1 */
#define RTLIMIT       255         /* Run time index limit */
#define INDEXMASK    0x7fff       /* Indicates part, glob, or RTA */
#define DATATYPE     0x74         /* Inidcates part, glob, or RTA */
#define BYTE3       0x80     /* 3 byte operand mask */
#define EXTGLOBAL    0x03         /* Mask for indicating ext global */
#define EXTPARVAR    0x02         /* Mask for indicating ext par */
#define RTAOFF      0x00     /* Mask for indicating RTA var */
#define MSB         1             /* Used to get MSB of int var */
#define LSB         0             /* Used to get LSB of int var */
#define NOPARVAR     0            /* Used for checking locextv rtn */
#define NODATA      0             /* No data has been moved (nulls) */
#define DATA        1             /* Data has been moved into buffer*/
#define RTA_ADJUST   64      /* Run time array offset index adjust */
#define PAR_ADJUST   64      /* Par var array offset index adjust */
#define MAXOPERS     9            /* Max operands, this should be 8 */
#define LITBUFSIZE   128     /* allow 128 bytes of literals */

/* put r_getbyte in-line for procoper to save time */
#define r_getbyte()  if (api_rtn_code = Get_obj_byte(stream_ptr, ip))\
                 fatal-error(1,15);

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/
OPER optbl[MAXOPERS];

/* Literal data areas: */

unsigned char litbuf[LITBUFSIZE];   /* static literal space */
unsigned char *litstrs[MAXOPERS];   /* pointers to allocated literals */
unsigned int liti;           /* number of alloced literals */
unsigned int litsize;             /* number of static literal bytes used */
unsigned char *get_list(unsigned int);

/* External variable data areas: */

PVAR *pev_ops[MAXOPERS];     /* pointers to PEV's */
unsigned int pev_opi;             /* number of PEVs */
GVAR *gev_ops[MAXOPERS];     /* pointers to GEV's */
unsigned int gev_opi;             /* number of GEVs */

/*---------------------------------------------------------------------
**  procoper(nbr_opr, mode, mod_args)
**
**   Procoper is called before every verb that takes operands, either
**   in tboldrv(), or by the verb itself.  Operands are read from the
**   instruction stream and their address (entry), buffer pointer, and
**   buffer length are loaded into the operand table.  The
**   entry field (address) is a pointer into the RTA for registers and
**   other RTA slots, a PEV or GEV pointer for external variables, and
**   null for literals.  Except for literals, the buffer pointer and
**   length are copies of the first two fields in the entry (buffer
**   and buflen).
**
**   Instructions may access the operands contents through the buffer
**   and buflen fields in the optbl[], but they may only modify an
**   operand by changint the buffer and buflen inside the entry.
**
**   The "rtoff" field is also filled in for the operand, but in a
**   ver clumsy way that should be fixed.  Only a few verbs even pay
**   attention to the result.
**
**   The ins_oper_tbl[] is used to pass values to procoper, including
**   the number of parameters to expect and which parameters get
**   modified.  The mode field is stored for all verbs in the
**   instruction stream following the verb, and indicates which
**   operands are "complex".  This is a wasteful use of instruction
**   stream space in my opinion.
**
**   Can return with the following possible error conditions:
**
**       BAD_INX  - a decimal number was encountered as the
**                  3'rd byte index.
**
**       INX_OVER - 3'rd byte index + offset is greater than 255.
**
**       BAD_OFF  - Invalid run time offset encountered.
**
**       NO_PARV  - no partition variable found.
**
**       NO_GLBV  - no global variable found.
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  02/05/86        R.G.R          ORIGINAL
**  07/28/87        SLW       Rewrite
**-------------------------------------------------------------------*/

procoper(nbr_opr, mode, mod_args)
unsigned int nbr_opr;         /* Number of operands to process */
unsigned int mode;       /* Mode byte of complex opcode */
unsigned int mod_args;        /* Flag showing what args are modified */
{
    OPER *opentry;           /* Used for setting up operand table */
    OPER *optblp;            /* Used to check for GEV operands */
    unsigned char opcode;         /* *ip */
    unsigned char mask;      /* Used to mask mode byte */
    REG unsigned int bytecnt;     /* Used in SETPTR */
    REG unsigned int rtoff;       /* local rtoff */
    unsigned int rtoff_b;         /* local rtoff before indexing */
    unsigned char intreg;         /* flag to indicate integer register */
    RTE FAR *rteptr;              /* Ptr to run time entry data */
    RTA *rtaptr;            /* pointer to run time array */
    unsigned char FAR *tbuf;      /* Temporary pointer */
    unsigned int glb_name;        /* Global variable name */
    unsigned int par_name;        /* Partition variable name */
    unsigned int threebyte;       /* three-byte operand flag */

    /* Get first byte of first operand (or next instruc if no operands) */
    procop_getbyte();

    /* Initialize literals' buffer */
    pev_opi = gev_opi = liti = litsize = 0;

    /* Initialize poitner to operand table */
    opentry = &optbl[0];

    /* Setup to test for complex operand */
    mask = MDOPER1;

    /* Loop until all operands are processed */
    while (nbr_opr--)
    {
       /* Test for complex operand */
       if (mode & mask)
       {
       /* Place MSB of run time offset within operand table */
       opentry->rtoff.artoff[MSB] = *ip & DATATYPE;
       threebyte = *ip & BYTE3;

       /* Point to LSB of run time offset */
       procop_getbyte();

       /* Place LSB of run time offset within operand table */
       opentry->rtoff.artoff[LSB] = *ip;

       /* store run time offset before indexing in a temporary */
       /* this determines the operand type, not the indexed value */
       rtoff_b = opentry->rtoff.irtoff;

       /* Test for a three byte complex operand (index) */
       if (threebyte)
       {
          /* Point to index (3rd byte) */
          procop_getbyte();
          opcode = *ip;

          /* Return error if index is a decimal or reserved register */
          if (intreg = (opcode <= DEC2))
          if (opcode >= DEC1 || opcode < INT1)
             /* Return bad index indication */
             return(BAD_INX);

          /* Get pointer to RTA index */
          rtaptr = &rtatbl[opcode];

          /* Add index to run time offset (but not to local rtoff_b) */
/* Put the following call in-line to save time:
          opentry->rtoff.irtoff += get_int_from_rta(rtaptr,
                       intreg ? INT_TYPE : ASC_TYPE) - 1;
*/
          if (intreg)
          opentry->rtoff.irtoff += *((unsigned int *) rtaptr->strptr) - 1;
          else
          opentry->rtoff.irtoff +=
              asciibin(rtaptr->strptr, rtaptr->strlngth) - 1;
       }

       /* store run time offset in a temporary */
       rtoff = opentry->rtoff.irtoff;

      /* Determine operand type and find operand data */

       /* Is operand in low-order RTA? */
       if (rtoff_b <= RTA1)
       {
          /* adjust RTA's up if indexed over swap line */
          if (rtoff > RTA1)
          opentry->rtoff.irtoff += RTA_ADJUST;

          /* Get address of run time array entry */
lowrta:      rteptr = (RTE *) &rtatbl[rtoff];
          goto dorta;
       }
       /* is operand a low-order PEV (between PAR1 and PAR2)? */
       else if (rtoff_b <= PAR2)
       {
          /* adjust PEV's up if indexed over swap line */
          if (rtoff > PAR2)
          rtoff = (opentry->rtoff.irtoff += PAR_ADJUST);
          goto dopev;
       }
       /* Is operand a high-order RTA (between RTA2 and RTA3)? */
       else if (rtoff_b <= RTA3)
       {
          /* Get address of run time array entry */
          rteptr = (RTE *) &rtatbl[(rtoff-RTA2) + (RTA1+1)];
dorta:         opentry->op_type = (rtoff > DEC2) ? RTA_TYPE :
                    (rtoff > INT2) ? DEC_TYPE : INT_TYPE;
       }
       /* Is operand a high-order PEV (between PAR3 and PAR4) ? */
       else if (rtoff_b <= PAR4)
       {
dopev:         /* Compute partition name */
          par_name = rtoff - ((rtoff <= PAR2) ? (PAR1-1) : PAR2);

          /* Call to locate ext par variable and if a return indicates
          variable not there then return with error.
          */
          if (((int *)rteptr = ins_pev(par_name)) == NOPARVAR)
          return(NO_PARV);

          if (mod_args & mask)     /* will arg be modified? */
          /* Store external variable as operand */
          pev_ops[pev_opi++] = (PVAR *)rteptr;

          /* Update remaining operand table entry fields */
          opentry->op_type = PAR_TYPE;
       }
       /* Operand must be a GEV (> GLB1) */
       else
       {
          /* Compute global variable name */
          glb_name = rtoff - GLB1;

          /* Call to locate ext par variable and if a return indicates
          variable not there then return with error.
          */
          if (((int *) rteptr = ins_gev(glb_name)) = NOPARVAR)
          return (NO_GLBV);

          if (mod_args & mask)      /* will arg be modified? */
          /* Store external variable as operand */
          gev_ops[gev_opi++] = (GVAR *)rteptr;

          /* Process global variable if it is not already processed */
          for (optblp = &optbl[0]; optblp < opentry; optblp++)
          if (rtoff_b == optblp->rtoff.irtoff)
              goto noproc;
          if (global_proc((GVAR *) rteptr))
          return(NO_GLBV);
noproc:
          opentry->op_type = GLB_TYPE;
       }

       /* Now fill in operand table with rte, ptr, and length */
       opentry->entry  = rteptr;
       opentry->buffer = rteptr->buffer;
       opentry->buflen = rteptr->buflen;
       }
       else    /* we have a simple operand */ 
       {
       /* Test for literal */
       if (!(opentry->rtoff.irtoff = rtoff = *ip))
       {
          opentry->op_type = LIT_TYPE;

          /* Get literal length */
          procop_getbyte();
          bytecnt = opentry->buflen = *ip;

          /* Get buffer of size literal length */
          opentry->buffer = tbuf = get_lit(bytecnt);
          /* move data into literal */
          while (bytecnt--)
          {
          procop_getbyte();
          *tbuf++ = *ip;
          }
       }
       /* See if operand is low-order PEV (between PAR1 and PAR2) */
       else if (rtoff >= PAR1)
          goto dopev;
       else       /* Operand is a one byte run time array offset */
          goto lowrta;
       }
       procop_getbyte();
       opentry++;
       mask >>= 1;
    }

    return(OK);      /* Return with no error indication */
}

unsigned char *get_lit(size)
unsigned int size;
{
    unsigned char *lit;

    if (size < LITBUFSIZE-litsize) {   /* handle null lits */
       lit = litbuf + litsize;
       litsize += size;
    }
    else
    {
       if (!(lit = malloc(size)))
       {
       api_fail();
       return(NULL);       /* ~0 */
       }
       litstrs[liti++] = lit;
    }
    return(lit);
}

end_opers()
{
    while (liti)
     free(litstrs[--liti]);
    while (pev_opi)
     proc_pev(pev_ops[--pev_opi]);
    while (gev_opi)
     proc_gev(gev_ops[--gev_opi]);
}

procop_getbyte()
{
    /* see if another byte is immediately available (it will be) */
    if (++stream_ptr->curoff <= stream_ptr->fenceoff)
     *ip = *stream_ptr->curptr++;
    else
    {
     stream_ptr->curoff--;
     r_getbyte();
    }
}
