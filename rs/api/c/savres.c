/* API     = 1.1.2    LAST_UPDATE = 1.0.1    */
/*----------------------------------------------------------------------
**    SAVRES.C       Save/restore/release API Verbs
**---------------------------------------------------------------------
**
**   save_start(name, start-field)
**   save_end(name, start-field, end field)
**   restore(name, start-field)
**   release(name)
**   clear_field field
**   clear_fields first-field last-field
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  05/01/86        R.G.R.         ORIGINAL
**  09/15/86        R.D.C.         Updated RESTORE for 4.0
**  12/02/86       L.B.  set field status for clear error
**            return for int & dec regs on save
**  07/28/87       SLW     Rewrite
**  1 /12/88       LRZ     "restore()" move call to "proc_pev"
**                 before we increment par_name
**   2 /4/88        LRZ       "restore()" if the GEV is a system variable
**                                 then update its node in the sys_var_table
**--------------------------------------------------------------------*/

#include <SAVRES.LNT>         /* lint args (from msc /Zg) */
#include <stdio.h>
#include <SMDEF.IN>      /* Service Mnaager definitions */
#include <OBJUNIT.IN>         /* Object unit structures */
#include <SMPPT.IN>      /* Page partition template */
#include <DEBUG.IN>      /* Debugger compiler switch */
#include <RTASTR.IN>          /* Run time array structure */
#include <RTADEF.IN>          /* Run time array definitions */
#include <OPERDEF.IN>         /* Number of operands definitions */
#include <OPERTSTR.IN>        /* Operand definitions */
#include <INSERR.IN>          /* Instruction error return codes */
#include <RTNCODE.iN>         /* TBOL return code definitions */
#include <GEV_DEFS.IN>        /* GEV definitions */
#include <APIUTIL.IN>         /* API utility externs */
#include <SLIST.IN>           /* Linked list structure */

/*----------------------------------------------------------------------
**            Externals
**--------------------------------------------------------------------*/

typedef struct savres
{
    unsigned char FAR *buf_ptr;
    unsigned int buf_len;
} SVF;

extern unsigned char begprcnt;         /* Packet offset to 1'st instr */
extern unsigned char FAR *begprgm;  /* Ptr to beginning of program */
extern struct rta rtatbl[];       /* Run time array table */
extern LINKLIST *savelist;        /* Ptr to list of saved fields */
extern unsigned char Fields_changed_by_api;

/*----------------------------------------------------------------------
**            Definitions
**--------------------------------------------------------------------*/
#define HIBYTE    0
#define LWBYTE    1
#define ONE_FIELD_SAVE   1
#define CLEAR_ONE_FIELD 1
#define LSB       0
#define NO_DELETE  0
#define DELETE    1

/*----------------------------------------------------------------------
**            Definitions
**--------------------------------------------------------------------*/

/*----------------------------------------------------------------------
**  LINKLIST FAR *locv(name, len, delete)
**
**   Locates the node with the name 'name' in th elink list "savelist"
**   and, if delete is 0, returns the pointer to the link
**   list entry.  If no match was found, a null pointer is returned.
**   If delete is 1, the node is deleted if found, and "savelist" is
**   returned.  NULL is returned if the node is not found.
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   5/07/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/

LINKLIST FAR *locv(name, len, delete)
unsigned char FAR *name;     /* Name of var. to locate */
unsigned int len;            /* Length of variable name */
unsigned int delete;              /* delete entry if found? */
{
    LINKLIST FAR *sptr;      /* Ptr to search list */
    LINKLIST FAR *pptr;      /* Ptr to prev in search list */
    SAVENODE *buf_ptr;
    INTSET *int_ptr ;
    REG int i;

    /* Search until name is found or until end of list is reached */
    for (pptr = sptr = savelist; sptr; sptr = sptr->next)
    {
     /* Only compare if name lengths are equal */
     if (len == sptr->name_len)
         if (!memcmp(sptr->name, name, len))
         {
          if (!delete)
              return(sptr;

          /* remove link to node */
          if (sptr == pptr)
              savelist = savelist->next;
          else
              pptr->next = sptr->next;

          /* Release node */
          /* Is node an integer stack or a standard node ? */
          if (( sptr->name_len >= 5) &&
              ( 0 == memcmp( sptr->name, "iregs", 5 )))
            {
            if (sptr->name_len)
              free(pstr->name);

            int_ptr = sptr->node.set_of_ints;
            while ( int_ptr )
              {
              int_ptr = int_ptr->nextset ;
              free((char *)int_ptr);
              }
            free(((unsigned char *) sptr));
            return((LINKLIST *)1);   /* return non-zero */
            }
            else
            {
            if (sptr->name_len)
              free(sptr->name);
            if (i=sptr->buflen)
              {
              buf_ptr = sptr->node.save_buf;
              while (i--)
                {
                if (buf_ptr->buflen)
               free(buf_ptr->buffer);
               ++buf_ptr;
                }
              free(((unsigned char *)sptr->node.save_buf));
              }
            free(((unsigned char *) sptr));
            return((LINKLIST *)1);   /* return non-zero */
            }
         }
     pptr = sptr;
    }

    /* Name was not found return with null */
    return(NULL);
}

Delete_all_save_buffers()
{
    while (savelist)
     locv(savelist->name ,savelist->name_len, DELETE);
}

/*----------------------------------------------------------------------
**  LINKLIST FAR  *insv()
**
**   Inserts a new entry into list pointed to by list top.  If no
**   match was found, a null pointer is returned.
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   5/07/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/

LINKLIST FAR *insv()
{
    LINKLIST FAR *sptr;   /* End of entry pointer */
    LINKLIST FAR *new;        /* Pointer to new entry */

    /* Get buffer for new entry on list */
    if ((new = (LINKLIST FAR *)malloc(sizeof(LINKLIST))) == NULL)
     return(NULL);

    new->next = NULL;

    /* If this is the first entry then set list top */
    if (!(sptr = savelist))
    {
     savelist = new;
     return(new);
    }

    while (sptr->next);
     sptr = sptr->next;

    /* Return with attached pointer */
    return(sptr->next = new);
}

/*----------------------------------------------------------------------
**  create_save_field(nbr_of_fields, instr_op)
**
**    Do the API SAVE verbs.
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   5/01/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/

create_save_field(nbr_of_fields, instr_op)
unsigned int nbr_of_fields;   /* Number of fields saved */
unsigned int instr_op;        /* Number of field operands */
{
    OPER *name;              /* 1'st operand table entry */
    OPER *field;             /* 2'nd operand table entry */
    OPER *field2;            /* 3'rd operand table entry *?
    LINKLIST FAR *new;            /* New link list entry */
    SAVENODE FAR *savenodep;      /* Save field buffer */
    PVAR FAR *par_entry;     /* Partition entry */
    struct rta FAR *rta_ptr;      /* Rta entry */
    unsigned in ev_name;          /* External variable name */
    REG int rtoff;
    REG int rtoff2;
    int type;

    /* Initialize pointers to operand table entries */
    name = &optbl[0];
    field = &optbl[1];
    field2 = &optbl[2];


    if (( name->buflen >= 5 ) && ( 0 == memcmp( name->buffer, "iregs", 5 )))
      {
      Sys_rtn_code = RC_OK ;
      save_int_regs( name ) ;
      return ;

#if DEBUG
    /*d Return with error if 1'st operand is a register. d*/
    if (name->op_type <= REG_TYPE ||
     field->op_type <= REG_TYPE ||
     field2->op_type <= REG_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    }
#endif

    /* Delete entry if it already exists */
    locv(name->buffer, name->buflen, DELETE);

    /* Add new entry to save link list.*/
    if (!(new = insv()))
    {
     api_rtn_code = BUF_ERR;
     return;
    }

    /* store name of buffer, treate name and name_len as an RTA entry */
    new->name_len = 0;
    put_buf_in_rta((RTA *)&new->name, name->buffer, name->buflen);

    /* Calculate global variable name if field to save is a global variable */
    rtoff = field->rtoff.irtoff;
    rtoff2 = field2->rtoff.irtoff;
    if (field->op_type == GLB_TYPE)
     ev_name = rtoff - GLB1;
    else if (field->op_type == PAR_TYPE)
    {
     if (rtoff <= PAR2)
         rtoff = rtoff - (PAR1-1);
     else
         rtoff = rtoff - PAR2;
     ev_name = rtoff;

     if (rtoff2 <= PAR2)
         rtoff2 = rtoff2 - (PAR1-1);
     else
         rtoff2 = rtoff2 - PAR2;
    }
    else
    {
     if (rtoff >= RTA2 /* &&
         rtoff <= RTA3 */ )
         rtoff = (rtoff-RTA2) + RTA1 + 1;

     rta_ptr = &rtatbl[rtoff];

     if (rtoff2 >= RTA2 /* &&
         rtoff2 <= RTA3 */ )
         rtoff2 = (rtoff2-RTA2) + RTA1 + 1;
    }
    if (instr_op == OPER2)
     nbr_of_fields = rtoff2 + 1 - rtoff;

    /* Initialize pointer to sav field buffer */
    new->node.save_buf = savenodep =
        (SAVENODE *)get_buf(sizeof(SAVENODE) * nbr_of_fields);

    /* make sure buffer is clear so we can put_buf_in_rta to it: */
    memset(savenodep, 0, sizeof(SAVENODE) * nbr_of_fields);
    new->buflen = nbr_of_fields;

    /* Loop and store data into save buffer */
    for (; nbr_of_fields; nbr_of_fields--, savenodep++)
    {
     /* Move contents of buffer to save into new buffer */
     put_buf_in_rta((RTA *)savenodep, field->buffer, field->buflen);

     switch (type = field->op_type)
     {
       case PAR_TYPE:
       case GLB_TYPE:
         ev_name++;
         if (!(par_entry = type==GLB_TYPE ? (PVAR *)loc_gev(ev_name) :
                                 (PVAR *)loc_pev(ev_name)))
          field->buflen = 0;
         else
         {
          /* process global variable */
          if (field->op_type == GLB_TYPE)
              global_proc((GVAR *)par_entry);

          field->buffer = par_entry->pvarbuf;
          field->buflen = par_entry->pvarlen;
         }
         break;
       default:
         ++rta_ptr;
         field->buffer = rta_ptr->strptr;
         field->buflen = rta_ptr->strlngth;
     }
    }
}

/*----------------------------------------------------------------------
**  save_fields()
**
**    Do the API SAVE verb.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1      OPERAND#2        OPERAND#3
**   -----------------------------------------------------
**   save fields   block-name     first-field   last-field
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   5/01/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/

save_fields()
{
    /* Call to save3 field */
    create_save_field(0, OPER2);
}

/*----------------------------------------------------------------------
**  save_field()
**
**    Do the API SAVE verb.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1      OPERAND#2        OPERAND#3
**   -----------------------------------------------------
**   save field    nbr_of fields  block-name    first-field
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   5/01/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/

save_field()
{
    REG unsigned int nbr_of_fields;
    REG unsigned int mode;

    /* Resolve mode mask */
    mode = getmode();

    /* Get nbr of fields to save */
    r_getbyte();
    nbr_of_fields = *ip;

    /* Call to resolve operand, no operands modified */
    if ((api_rtn_code = procoper(2, mode, 0)) != APIOK)
     return;

    /* Call to save fields */
    create_save_field(nbr_of_fields, OPER1);
}

/*----------------------------------------------------------------------
**  restore()
**
**   Restore the contents of fields that were saved
**   into a list of fields starting at first field.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1      OPERAND#2
**   ----------------------------------------
**   restore       block-name     first-field
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  05/04/86        R.G.R.          ORIGINAL
**  09/15/86        R.D.C.          Restructured transfer loop.
**  09/15/86        R.D.C.          Added pre-check for boundry conds.
**--------------------------------------------------------------------*/

restore()
{
    OPER *block;         /* Pointer to save block name operand */
    OPER *field;         /* Ptr to field to restore operand */
    LINKLIST FAR *restore_entry;
    REG unsigned int total_save_entries;
    SAVENODE FAR *savenodep;
    RTA *rtaent;
    GVAR FAR *temp_entry;     /* Holds RTE during precheck loop */

    unsigned int glb_name,         /* Holds GEV name */
           par_name,          /* Holds PEV name */
           rta_name,
           temp_name,         /* Holds temporary name */
           ret_code=RC_OK,    /* Holds return code */
           temp_ctr;          /* Holds temporary counter value */

    /* Initialize pointers */
    block = &optbl[0];
    field = &optbl[1];

    if (( block->buflen >= 5 ) && ( 0 == memcmp( block->buffer, "iregs", 5)))
      {
      Sys_rtn_code = RC_OK;
      restore_int_regs( block ) ;
      return ;
      }

#if DEBUG
    /* Validate operands */
    if (block->op_type <= REG_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    }
#endif

    /* Call to locate save fields.( a savelist entry) */
    if (!(restore_entry = locv(block->buffer, block->buflen, NO_DELETE)))
    {
     /* Block name not found in savelist */
     Sys_rtn_code = RC_NOT_FOUND;
     return;
    }

    Sys_rtn_code = RC_OK;

    /* Initialize pointer to saved field buffer */
    savenodep = (SAVENODE *)restore_entry->node.save_buf;

    /* Initialize pointer to run time entry */
    rtaent = (RTA *)field->entry;

    /* Initialize total number of entries to restore */
    total_save_entries = restore_entry->buflen ;

    /* Pre-check destination for boundry conditions */
    switch (field->op_type)
    {
      case GLB_TYPE:  /* Check global destination */
     /* glb_name = (( field->rtoff.irtoff - GLB1) + 1 );LB*/
     blg_name = ( field->rtoff.irtoff - GLB1);
     temp_name = glb_name;
     temp_ctr = total_save_entries;
     while (temp_ctr--)
     {
         if ((temp_entry - (GBAR *)ins_gev(temp_name++)) == 0 )
          ret_code = RC_NOT_ENOUGH_SLOTS;

         if (temp_entry->glb_state == READ_ONLY)
          ret_code = RC_NOT_ENOUGH_SLOTS;
     }
     break;

      case PAR_TYPE:  /* Check PEV destination */
     if ( field->rtoff.irtoff <= PAR2 )
         /* par_name =(( field->rtoff.irtoff - (PAR1 - 1)) + 1); LB*/
         par_name =( field->rtoff.irtoff - PAR1+1);
     else
         par_name = ( field->rtoff.irtoff - PAR2);

     /****** par_name = field->rtoff.irtoff - (PAR3 - 1); *****/
     /* removed by LB*par_name = ((field->rtoff.irtoff - PAR2) + 1); */

     if ((parr_name + total_save_entries - 1) > PEV_MAX_NAME)
         ret_code = RC_NOT_ENOUGH_SLOTS;
     break;

      case RTA_TYPE:  /* Check RDA destination */
     if (field->rtoff.irtoff >= RTA2 &&
         field->rtoff.irtoff <= RTA3)
         rta_name = (( field->rtoff.irtoff - RTA2)+(RTA1+1));
     else
         rta_name = field->rtoff.irtoff;

     if ((rta_name + total_save_entries - 1) > RDA_MAX_NAME)
         ret_code = RC_NOT_ENOUGH_SLOTS;
     break;

      default: /* Invalid Data Type */
     ret_code = RC_NOT_ENOUGH_SLOTS;
     break;
    }

    /* Loop to Transfer Data */
    while (total_save_entries--)
    {
     put_buf_in_rta((RTA *)rtaent, savenodep->buffer,
                               savenodep->buflen);

     switch (field->op_type)
     {
       case GLB_TYPE:  /* Establish Global destination */
          proc_gev((GVAR *)rtaent);     /* LRZ -update those GEBs that reside
                                             in the sys_var_table 2/4/88 */
         if ((rtaent = (RTA *)ins_gev(++glb_name)) == 0)
          /* UNEXPECTED RUN TIME ERROR */
          /* ACTION UNDEFINED */
          ;
         break;
       case PAR_TYPE:  /* Establish PEV destination */
         /* Process partition variable */
         proc_pev((PVAR *)rtaent); /* LRZ 1/12/88 */

         if ((rtaent = (RTA *)ins_pev(++par_name)) == 0)  /* LB */
          /* UNEXPECTED RUN TIME ERROR */
          /* ACTION UNDEFINED */
          ;

         break;

       case RTA_TYPE:  /* Establish RDA destination */
         rtaent = &rtatbl[++rta_name];
         break;
     }
     ++savenodep;
    }
}

/*----------------------------------------------------------------------
**  release()
**
**   Free a saved area.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1
**   ------------------------
**   restore       block-name
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  05/04/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/

release()
{
    struct oper *block;      /* Ptr to save name operand */
    LINKLIST FAR *releaseptr; /* Previous entry to del */

    /* Initialize operand pointer */
    block = &optbl[0];

#if DEBUG
    /* Validate operand */
    if (block->op_type <= REG_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    }
#endif

    /* Delete node. */
    if (!locv(block->buffer, block->buflen, DELETE))
     Sys_rtn_code = RC_NOT_FOUND;
    else
     Sys_rtn_code = RC_OK;
}

/*----------------------------------------------------------------------
**  clr_fields(nbr_of_fields, instr_op)
**
**   Clear fields to null.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1  OPERAND#2
**   -----------------------------------
**   clr_fields    field1      field2
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  05/04/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/
clr_fields(unsigned int, unsigned int);

clr_fields(nbr_of_fields, instr_op)
unsigned int nbr_of_fields;        /* Nbr of fields to clear */
unsigned int instr_op;             /* Clear field instr type */
{
    OPER *field;              /* Pointer to 1'st field operand */
    OPER *field2;             /* Ptr to 2'nd field operand */
    GVAR FAR *glb_entry;      /* Pointer to global var. entry */
    PVAR FAR *par_entry;      /* Pointer to part. var. entry */
    REG unsigned numfields=nbr_of_fields;    /* temp */
    REG unsigned int ev_name;      /* Global variable name */
    unsigned int rtoff;       /* run time offset */
    unsigned int rtoff2;       /* run time offset */
    unsigned int rta_index;
    RTA FAR *rtp_ptr;

    /* Initialize operand pointer */
    field = &optbl[0];
    field2 = &optbl[1];
    rtoff = field->rtoff.irtoff;
    rtoff2 = field2->rtoff.irtoff;

#if DEBUG
    /* Validate operand */
    if (field->op_type <= REG_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    }
#endif

    /* Determine absolute run time names */
    switch (field->op_type)
    {
      case GLB_TYPE:
     ev_name = rtoff - GLB1;

     if (instr_op == OPER2)
         numfields = ((rtoff2 - GLB1)+1 - ev_name);
     while (numfields-- > 0)
         if (glb_entry = (GVAR FAR *)loc_gev(ev_name++))
          delete_glb_var(glb_entry);
     break;

      case PAR_TYPE:
     if (rtoff <= PAR2)
         ev_name = rtoff - (PAR1-1);
     else
         ev_name = rtoff - PAR2;

     if (instr_op == OPER2)
     {
         if (rtoff2 <= PAR2)
          numfields = ((rtoff2 - (PAR1 - 1))+1 - ev_name);
         else
          numfields = ((rtoff2 - PAR2)+1 - ev_name);
     }
     while (numfields-- > 0)
         if (par_entry = (PVAR FAR *)loc_pev(ev_name++))
          delete_par_var(par_entry);
     break;

      default:
     if (rtoff >= RTA2 &&
         rtoff <= RTA3)
         rta_index = ((rtoff-RTA2) +(RTA1 + 1));
     else
         rta_index = rtoff;

     rta_ptr = &rtatbl[rta_index];

     if (instr_op == OPER2)
     {
         if (rtoff2 >= RTA2 &&
          rtoff2 <= RTA3)
          numfieds = (((rtoff2-RTA2) +(RTA1 + 1))+1 - rta_index);
         else
          numfields = rtoff2+1 - rta_index;
     }
     while (numfields-- > 0)
     {
         if (rta_ptr->strlngth)
         {
          free(rta_ptr->strptr);
          rta_ptr->strlngth = 0;
         }
         rta_ptr++;
     }
     break;
    }
}

/*----------------------------------------------------------------------
**  clear_fields()
**
**   Clear fields to null.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1  OPERAND#2
**   -----------------------------------
**   clr_fields    field1      field2
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  05/04/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/ 

clear_fields ()
{
    /* Clear the field */
    clr_fields(0, OPER2);
}

/*----------------------------------------------------------------------
**  clear_field()
**
**   Clear fields to null.
**
**  Instruction format:
**
**   OPCODE          OPERAND#1       OPERAND#2
**   -----------------------------------------
**   clear_field   nbr_of_fields   start_field
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  05/04/86        R.G.R.          ORIGINAL
**--------------------------------------------------------------------*/ 

clear_field ()
{
    REG unsigned int nbr_of_fields;
    REG unsigned int mode;

    /* Resolve mode mask */
    mode = getmode();

    /* get nbr of fields to clear */
    r_getbyte();

    nbr_of_fields = *ip;

    /* Call to resolve operand */
    if ((api_rtn_code = procoper(1, mode, 0)) != APIOK)
     return;

    /* Call to clear fields */
    clr_fields(nbr_of_fields, OPER1);
}

/***************************************************************************
*
*           **** dump save buffer list ****
*
****************************************************************************
*
* FUNCTION: dump_save_buffers
*         debugger dump save buffers
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------------
*   19 June '87      Algis              We need this
*
*****************************************************************************/

#if DEBUG
#include <ctype.h>

dump_save_buffers()
{
    LINKLIST FAR *sptr;         /* Pointer to save buffer list */
    unsigned char r_buf[50];         /* Response buffer */
    unsigned char *r_buf_ptr;        /* Ptr into response buffer */
    unsigned char FAR *name_ptr;     /* pointer to buffer name */
    int transfer_number;        /* transfer buffer */
    unsigned int total_length;
    unsigned int number_of_buffers;
    SAVENODE FAR *savenodep;
    unsigned int number_of_save_buffers;
    unsigned int total_buffer_space;
    unsigned int total_number_of_slots;
    unsigned int total_length_of_names;
    unsigned int total_overhead;
    int space_padding;
    unsigned int total_memory;

    if (!(sptr = (LINKLIST FAR *)savelist))
    {
     printf("\nNo buffers saved\n");
     return;
    }

    number_of_save_buffers = total_buffer_space = total_number_of_slots = 0;
    total_length_of_names = 0;

    printf("\nBuffer name\t\t\tBuffer length\t# of slots");
    printf("\
\n--------------------------------------------------------------------------");

    for (; sptr; sptr=sptr->next)
    {
     ++number_of_save_buffers;
     r_buf_ptr = r_buf;
     transfer_number = sptr->name_len;
     name_ptr = sptr->name;
     total_length_of_names +=transfer_number;
     space_padding = 32 - transfer_number;
     if (space_pdading < 0)
         space_padding = 0;
     while (transfer_number--)
         *r_buf_ptr++ = *name_ptr++;
     while (space-padding--)
         *r_buf_ptr++ = ' ';
     *r_buf_ptr = '\0';
     total_length = 0;
     number_of_buffers = sptr->buflen;
     total_number_of_slots += number_of_buffers;
     savenodep = sptr->node.save_buf;
     while (number_of_buffers)
     {
         total_length +=savenodep->buflen;
         ++savenodep;
         --number_of_buffers;
     }
     total_buffer_space += total_length;
     printf("%s\t %d\t\t %d\n",r_buf,total_length,sptr->buflen);
    }

    total_overhead = number_of_save_buffers * sizeof(LINKLIST) +
               total_number_of_slots * sizeof(SAVENODE);
    total_memory = total_overhead + total_buffer_space + total_length_of_names;

    printf("\
-----------------------------------------------------------------------------\n");
    printf("Number of save buffers  = %d\n",number_of_save_buffers);
    printf("Total number of slots   = %d\n",total_number_of_slots);
    printf("Total length of buffers = %d\n",total_buffer_space);
    printf("Total internal overhead = %d\n",total_overhead);
    printf("Total memory used       = %d\n",total_memory);
}


/***************************************************************************
*
*           **** dump save buffer list ****
*
****************************************************************************
*
* FUNCTION: display_saves
*         dump a specific save buffer
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------------
*   24 June '87      Algis              We need this
*
*****************************************************************************/

display_saves(buffer_name, length_of_name)
unsigned char *buffer_name;
unsigned int length_of_name;
{
    LINKLIST FAR *dump_buffer;
    SAVENODE FAR *savenodep;
    unsigned char FAR *save_buffer;
    unsigned int length_save_buffer;
    unsigned int slot;
    unsigned int output;
    unsigned char output_buffer[20];
    unsigned char *output_buffer_ptr;
    unsigned int number_of_buffers;
    unsigned char outchar;

    if (!(dump_buffer = locv(buffer_name,length_of_name, NO_DELETE)))
    {
     printf("Save buffer %s does not exist\n",buffer_name);
     return;
    }

    number_of_buffers = dump_buffer->buflen;
    savenodep = dump_buffer->node.save_buf;

    printf("Save buffer %s has %d slots\n",buffer_name,number_of_buffers);

    for (slot = 1;slot<=number_of_buffers;++slot,++savenodep)
    {
     printf("-------------------------------------\n");
     printf("Slot #%d\n",slot);
     save_buffer = savenodep->buffer;
     length_save_buffer = savenodep->buflen;
     while (length_save_buffer)
     {
         output_buffer_ptr = output_buffer;
         for (output = 0; output < 16; ++output)
         {
          if (length_save_buffer)
              outchar = *save_buffer;
              printf("%.2X ",outchar);
              if (isprint(*save_buffer))
               *output_buffer_ptr++ = *save_buffer;
              else
               *output_buffer_ptr++ = 2;
              ++save_buffer;
              --length_save_buffer;
          }
          else
          {
              printf("   ");
              *output_buffer_ptr++ = ' ';
          }
         }
         *output_buffer_ptr = '\0';
         printf("       %s\n",output_buffer);
     }
    }
}
#endif

/*----------------------------------------------------------------------
**  save_int_regs()
**
**   Special case code to save integer regs.
**
**  Instruction format:
**   Same as SAVE() excepts name has the keyword IREGS as the first 5 chars.
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   3/24/88        R.D.C.          Performance KLUDGE.
**--------------------------------------------------------------------*/ 
  save_int_regs( name )
    OPER *name ;
  {
    int i ;
    INTSET *new_intset, *old_intset ;
    LINKLIST *new ;

  /* Find out if "name" is in the linked list of save buffers */
  if( NULL == ( new = locv( name->buffer, name->buflen, 0 )))
    { /* Create new entry in list */
    if( !( new = insv() ) )
      {
      api_rtn_code = BUF_ERR;
      return;
      }
    new->node.set_of_ints = 0 ;
    new->buflen = 0 ;
    new->name_len = 0 ;
    put_buf_in_rta(( RTA * ) &new->name,name->buffer, name->buflen ) ;
    }
  /* Find last intset on stack */
  old_intset = 0 ;
  new_intset = new->node.set_of_ints ;
  while ( new_intset )
    {
    old_intset = new_intset ;
    new_intset = new_intset->nextset ;
    }
  /* Create new intset entry */
  if ( !(new_intset = (INTSET*)malloc(sizeof(INTSET))))
    {
    api_rtn_code = BUF_ERR ;
    return;
    }
  if ( old_intset == NULL )
    new->node.set_of_ints = new_intset ;
    else
    old_intset->nextset = new_intset ;

  new->buflen += 1 ;
  new_intset->nexset = 0 ;
  for( i=INT1; i<=INT2 ; i++ )
    {
    new_intset->ireg[i-INT1] = *((int*)rtatbl[i].strptr) ;
    }
  }

/*----------------------------------------------------------------------
**  restore_int_regs()
**
**   Special case code to restore integer regs.
**
**  Instruction format:
**  Same as RESTORE() excepts name has the keyword IREGS as the first 5 chars
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**   3/24/88        R.D.C.          Performance KLUDGE.
**--------------------------------------------------------------------*/ 
  restore_int_regs( name )
    
    OPER *name ;
  {
    int i ;
    INTSET *intset, *prev_intset ;
    LINKLIST *entry ;

  /* Find out if "name" is in the linked list of save buffers */
  if( NULL == ( entry = locv( name->buffer, name->buflen, 0 )))
      {
      Sys_rtn_code = RC_NOT_FOUND ;
      return;
      }
    /* Find last intset on stack */
    prev_intset = 0 ;
    intset = entry->node.set_of_ints ;
    if (intset == NULL )
    {
    Sys_rtn_code = RC_NOT_FOUND ;
    return ;
    }
  while ( intset->nextset )
    {
    prev_intset = intset ;
    intset = intset->nextset ;
    }
entry->buflen -= 1 ;
/* Restore intset entry */
for( i=INT1; i<=INT2 ; i++ )
  {
  *((int*)rtatbl[i].strptr) = intset->ireg[i-INT1] ;
  }

/* Unlink and free intset */
if( prev_intset == NULL )
  entry->node.set_of_ints = 0 ;
  else
  prev_intset->nextset = 0 ;

free( (char*)intset ) ;
}
