/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    TBOLDRV.C       TBOL evaluator
**--------------------------------------------------------------------
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  01/17/86        R.G.R.         ORIGINAL
**  03/21/86        ALGIS          UPDATE
** 17 November      Algis          Initialization of api_exit code
**                   moved to happen before debug mode.
**                   Internal comments.
** 16 December '86  Algis           Push pop stack is changed and is
**                   index by a counter.
**  07/28/87    SLW       Rewrite.
**---------------------------------------------------------------*/

#include <TBOLDRV.LNT>   /* lint args (from msc /Zg) */
#include <stdio.h>
#include <SMDEF.IN>
#include <SMKEYS.IN>
#include <DEBUG.IN>
#include <ERROR.IN>
#include <DINSERR.IN>
#include <APITYP.IN>
#include <RTASTR.IN>
#include <RTADEF.IN>
#include <OPERDEF.IN>
#include <DEBUG.IN>
#include <INSERR.IN>
#include <APIDRV.IN>
#include <SLIST.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <ISCB.IN>
#include <SSCB.IN>
#include <IPCB.IN>
#include <INSCB.IN>
#include <GEV_DEFS.IN>
#include <APIUTIL.IN>    /* GEV definitions */

/*--------------------------------------------------------------------
**            Externals
**------------------------------------------------------------------*/

extern brk();           /* Break instruction. */
extern cje();           /* Compare/jump if equal. */
extern cjne();          /* Compare/jump if not equal. */
extern cjl();           /* Compare/jump if less than. */
extern cjg();           /* Compare/jump if greater than. */
extern cjle();          /* Compare/jump less than or equal. */
extern cjg();           /* Compare/jump gretaer than or equal. */
extern jmp();           /* GOTO (unconditional jump). */
extern def_fld();       /* Define field instruction process. */
extern deffldprg();     /* Define field instruction process. */
extern deffldprgparam();    /* Define field instruction process. */
extern set_attribute();     /* Set attribute instruction process. */
extern open_file();     /* Open file instruction process. */
extern close_file();        /* Close file instruction process. */
extern close_all();     /* Close file instruction process. */
extern read_file();     /* Read file instruction process. */
extern read_line();     /* Read file-key instruction process. */
extern write_file();        /* Write file instruction process. */
extern write_line();        /* Write file-key instruction process. */
extern enable_comm();       /* Connect to modem instruction process. */
extern disable_comm();      /* Disconnect from modem. */
extern tx_msg_no ();        /* Send no resp req instruction process. */
extern tx_msg_yes ();       /* Send resp req instruction process. */
extern rcv_msg();       /* Receive instruction process. */
extern cancel();        /* Cancel instruction process. */
extern navig();         /* Navigate instruction process. */
extern navign();        /* Navigate next instruction process. */
extern navigb();        /* Navigate back instruction process. */
extern navigf();        /* Navigate first instruction process. */
extern navigl();        /* Navigate last instruction process. */
extern fetch();         /* Fetch instruction process. */
extern fetchrq();       /* Fetch instruction process. */
extern oepn_wind();     /* Open window instruction process. */
extern close_wind();         /* Close window instruction process. */
extern close_open_wind();   /* Close & open a window. */
extern nop ();          /* No operation. */
extern kill();          /* Kill instruction process. */
extern pcache();        /* Purge cache instruction process. */
extern move();          /* Move instruction process. */
extern move_abs();      /* Move absolute instruction process. */
extern swap();          /* Swap instruction process. */
extern add();           /* Add instruction process. */
extern sub();           /* Subtract instruction process. */
extern mul();           /* Multiply instruction process. */
extern dvd();           /* Divide instruction process. */
extern dvdrem();        /* Divide-remainder process. */
extern string();        /* String instruction process. */
extern substr();        /* Sub-string instruction process. */
extern instr();         /* In string instruction process. */
extern up_case();       /* Upper case instruction process. */
extern fill();          /* Fill instruction process. */
extern push();          /* Push instruction process. */
extern pop();           /* Pop instruction process. */
extern and();           /* And instruction process. */
extern or();            /* Or instruction process. */
extern xor();           /* Xor instruction process. */
extern test();          /* Test instruction process. */
extern length();        /* Length instruction process. */
extern format();        /* Format instruction process. */
extern makemap();       /* Make format instruction process. */
extern edit();          /* Edit instruction process. */
extern syncsv();        /* Sync save instruction process. */
extern syncrel();       /* Sync release instruction process. */
extern timon();         /* Timer on instruction process. */
extern wait();          /* Wait instruction process. */
extern start();         /* Start program. */
extern stop();          /* Stop program. */
extern call();          /* Procedure-id instruction process. */
extern link();          /* Link instruction process. */
extern rtn();           /* Return instruction process. */
extern procexit();      /* Exit instruction process. */
extern godep();         /* Go to depending on. */
extern proc_error();         /* Error instruction process. */
extern save_field();         /* Save only one field. */
extern note_file ();         /* Note instruction process. */
extern point_file ();        /* Point instruction process. */
extern sound_default ();    /* Make sound module "asm". */
extern set_sound ();         /* Set and make sound "asm". */
extern sort ();         /* Sort fields. */
extern look_up();       /* Lookup instruction process. */
extern set_key();       /* Set logical key. */
extern set_key_prog();       /* Set logical key with prg specified. */
extern set_f();         /* Set function. */
extern set_f_prog();         /* Set logical function with prgm. */
extern transfer ();     /* Transfer instruction process. */
extern save_fields();        /* Save a range of fields. */
extern restore();       /* Restore saved fields. */
extern release();       /* Restore saved fields. */
extern clear_filed();        /* Clear a field. */
extern clear_fields();       /* Clear fields. */
extern set_back_grnd();     /* Set back ground color. */
extern trig_func();     /* Trigger a function. */
extern file_screen();        /* File a screen. */
extern show_screen();        /* Show a filed screen. */
extern upload();        /* Upload to host. */
extern download();      /* Request a download from host. */
extern access();        /* Access instruction process. */
extern timer_off();     /* Disable timer enabled by timer on. */
extern movebl();        /* Move an entire block. */
extern opn_e_wind();         /* Open an error window. */
extern rtn_parm();      /* Return with return code. */
extern procexit_rc();        /* Exit with return code. */
extern refresh_fields();    /* Refresh screen I/O fields. */
extern api_erase();     /* Erase field to a given color (EAP)*/
extern set_cursor();         /* set cursor to a given field (EAP)*/
extern set_f_p_parm();       /*Set function with program and parameters*/
extern opn_e_w_name();       /* Open an error window. */

#if  (DEBUG || DEBUG_ALONE)
extern unsigned char FAR *dbg_ip;    /* Debugger instruction pointer. */
extern unsigned char Debug_flag;     /* Debug enable flag. */
extern unsigned char debug_re_entry; /* Debug re_entry flag. */
extern unsigned char debu_init;      /* Debug init flag. */
#endif

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/
#define RTN   1
#define NORTN 0
#ifdef COMPLEX
#undef COMPLEX
#endif
#define COMPLEX 0x80

#define RESOLVE 0
#define LSB     0
#define MSB     1

#define API_START 1
#define RE_ENTERING 0

#define CONTINUE  0xFFFF

#define TBOL_CODE_OFFSET 22   /* offset of tbol code in an object */

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/

/* Declare instruction process routine table. */
unsigned int (*iprocess [])()  =
{
    brk, cje, cjne, cjl, cjg, cjle, cjge, jmp, def_fld, deffldprg,
    set_attribute, open_file, close_all, close_file, read_line, read_file,
    write_line, write_file, enable_comm, disable_comm, tx_msg_no,
    tx_msg_yes, rcv_msg, cancel, navig, navign, navigb, navigf, navigl,
    fetch, fetchrq, open_wind, opn_e_wind, close_wind, close_open_wind,
    kill, pcache, move, move_abs, swap, add, sub, mul, dvd, dvdrem, fill,
    and, or, xor, test, length, format, makemap, edit, string, substr,
    instr, up_case, push, pop, syncsv, syncrel, timon, wait, start, stop,
    set_key, set_key_prog, set_f, set_f_prog, call, link, rtn, transfer,
    procexit, godep, proc_error, save_field, save_fields, restore, release,
    clear_field, clear_fields, note_file, point_file, sound_default,
    set_sound, sort, look_up, set_back_grnd, trig_func, file_screen,
    show_screen, upload, download, access, nop, timer_off, movebl, rtn_parm,
    procexit_rc, refresh_fields, api_erase/*(EAP)*/, set_cursor/*(EAP)*/,
    brk, brk, opn_e_wind, deffldprgparam/*(RC)*/, set_f_p_parm/*(RC)*/,
    opn_e_w_name
};

/* Allocate space for run time array table. */
RTA rtatbl [256];

unsigned char *ip;            /* Current instruction pointer. */
Objstream stream_ptr;              /* Program stream pointer. */

IPCB *pstack;                 /* Current parameter stack ptr */
IPCB  parmstck[IPCB_SIZE + 1];          /* Parameter stack */

ISCB *instack;                /* Call,link:rtn stack ptr */
ISCB instck[ISCB_SIZE + 1];        /* Call,link:rtn stack */
ISCB *level0;                 /* Dynamic level 0 stack ptr */

int   stack_index;            /* push:pop stack */
INSCB intstck[INSCB_SIZE];         /* Push:Pop stack */

SSCB *api_suspend;            /* Current api suspend stack ptr */
SSCB sscb_stack[MAXWINDOWS+1];          /* Window stack */

GVAR FAR *glbmid;             /* Mid ptr of global tree list */

unsigned int api_rtn_code;         /* Api return code */
unsigned char apirtn;              /* Return to system flag */

LINKLIST FAR *savelist;       /* Pointer to save field list */

unsigned char data;           /* Current instruction bytes */
unsigned char api_exit_code;       /* API exit code */
unsigned int define_field_flag;    /*Flag used to call insert web */

/*---------------------------------------------------------------------
**  tboldrv(prg, start_flag)
**
**   This routing will process a program object until an exit
**   instruction is encountered.  The pointer to the program object
**   should be pointing to the first instruction opcode so that
**   processing can begin immediately.
**
**    DATE           PROGRAMMER       REASON
**--------------------------------------------------------------------
**  01/17/86        R.G.R.           ORIGINAL
**-------------------------------------------------------------------*/

tboldrv(prog, start_flag)
TBOLPROG FAR *prog;
unsigned int start_flag;
{
    REG unsigned char ins_inx;          /* Instruction table index. */
    REG unsigned char mode;        /* Instruction mode byte. */
    struct procop *resop;          /* Pointer to resolve operand tbl*/
    SSCB *bottom_of_stack;         /* Bottom of stack pointer. */
    ERR_MSG error_code;       /* Holds api rtn error. */
    unsigned int op_rtn;

#if DEBUG
    unsigned int debug rtn;
          int prev_offset;
          int cur_offset

    /* Save current instruction pointer. */
    dbg_ip = ip;
    if (Debug_flag)
    {
     if (debug_init)
     {
         if ((prev_offset = Obj_get_offset(stream_ptr)) < 0)
          return (PROG_ERROR);
     }
     ip = &data;
    }
#endif
#ifdef PH phook(70, prog, &start_flag); #endif

    /* If this is the start of a new program then begin program */
    if (start_flag == API_START)
    {
/* #if PROTOTYPE       removed -- SLW */
#if !DEBUG         /* replaces PROTOTYPE */

     /* Initialize stack pointers only if at bottom of window stack. */
     if (api_suspend >= &sscb_stack[MAXWINDOWS + 1])
     {
         pstack = &parmstck[IPCB_SIZE];
         instack = &instck[ISCB_SIZE];
     }

     /* Set up level0 stack pointer. This is used to determine if a
        return to API caller is needed when a return instruction is
        executed.
     */
     level0 = instack;

     /* Get first opcode, bind parameters */
     if ((op_rtn = first_opcode(prog)) != NO_ERROR)
         return(op_rtn == STOP ? NO_ERROR : op_rtn);

#endif
/* #endif        removed -- SLW */

#if DEBUG
     if (Debug_flag)
     {
         /* Initialize stack pointers only if at bottom of window stack. */
         bottom_of_stack = &sscb_stack[MAXWINDOWS+1];

         if (debug_init == 0)
         {
          if (api_suspend >= bottom_of_stack)
          {
              pstack = &parmstck[IPCB_SIZE];
              instack = &instck[ISCB_SIZE];
          }

          /* Set up level0 stack pionter. This is used to determine if
             a return to API caller is needed when a return instruction
             is executed.
          */
          level0 = instack;
         }

         if ((debug_rtn = debug_startup(prog,START)) != NO_ERROR)
         {
          if (debug_rtn != CONTINUE)
          {
              if (debug_rtn == STOP)
              {
               if (api_exit_code)
               {
                   api_exit_code = 0;/* algis clear exit code */
                   return(STOP);
               }
               else
                   return(NO_ERROR);
              }
              else
              {
               return(debug_rtn);
              }
          }
         }
     }
     else
     {
         /* Initialize stack pointers only if at bottom of window stack. */
         bottom_of_stack = &sscb_stack[MAXWINDOWS+1];

         if (api_suspend >= bottom_of_stack)
         {
          pstack = &parmstck[IPCB_SIZE];
          instack = &instck[ISCB_SIZE];
         }

         /* Set up level0 stack pointer. This is used to determine if a
            return to API caller is needed when a return instruction is
            executed.
         */
         level0 = instack;

         if ((op_rtn = first_opcode(prog)) != NO_ERROR)
         {
          if (op_rtn == STOP)
              return(NO_ERROR);
          else
              return (op_rtn);
         }
     }
#endif

    }
    else    /* API Program re-entry. */
    {
     /* Return with error if currently at bottom of stack. */
     if (api_suspend >= &sscb_stack[MAXWINDOWS + 1])
         return (PROG_ERROR);

     /* Pop stream ptr, opcode, level0, and api event for this process */
     stream_ptr = api_suspend->s_stream;
     *ip     = api_suspend->s_opcode;
     level0       = api_suspend->w_level0;
     Sys_api_event = api_suspend->api_event;

     /* Return with no error if window was open with a close-open
        window verb.
     */
     if (stream_ptr == 0)
     {
         /* Adjust stack pointer. */
         ++api_suspend;
         return(NO_ERROR);
     }

     /* Seek to correct spot */
     if ((Obj_seek(stream_ptr, api_suspend->s_offset, 0)) != 0)
         return (PROG_ERROR);

     /* Adjust stack pointer */
     ++api_suspend;

#if DEBUG
     if (Debug_flag)
         if ((debug_rtn = debug_startup(prog,RE_ENTERING)) != NO_ERROR)
         {
          if (debug_rtn == STOP)
          {
              if (api_exit_code)
               return(STOP);
              else
               return(NO_ERROR);
          }
          else
          {
              return(debug_rtn);
          }
         }
#endif
    }

    /* place wait icon on streen */

    wait_icon();

    /* Set return to system flag to don't return. */

    apirtn = NORTN;
    api_rtn_code = APIOK;
    api_exit_code = 0;

#ifdef PH
phook(76,stream_ptr,&start_flag);
#endif

    /* Loop until an "EXIT" instruction is encountered. */
    while (apirtn != RTN)
    {
     ins_inx = *ip & ~COMPLEX;

     /* If required resolve this instruction operands. */
     resop = &ins_oper_tbl[ins_inx];
     if (resop->instr_state == RESOLVE)
     {
         api_rtn_code = APIOK;
         mode = getmode();
         if (api_rtn_code = procoper(resop->nbr_of_oper, mode,
                         resop->mod_args))
          goto doerr;
     }
     /* Call routine to process instruction */

#ifdef PH
phook(49);
#endif

     (*iprocess [ins_inx])();

     /* free literals allocated for this instruction, if any,
        and process external variables */
     end_opers();

doerr:

#if DEBUG_ALONE

     /* Define field optimiation. */
     if (apirtn == RTN)
         tbol_end_proc ();

     /* Report error if out of system resources. */
     if (api_rtn_code != APIOK)
     {
         printf("FATAL ERROR: contact technical support.\n");
         printf("RETURN CODE = %u - %s\n", api_rtn_code, api_err_msg[api_rtn_code]));
         printf("api_return_code = %d\n", api_rtn_code);

         api_rtn_code = APIOK;
     }
     /* Restore instruction pointer. */
     /*  ip = dbg_ip;   */

     /* Return to debugger. */
     return (NO_ERROR);
#endif
#if DEBUG
     if (Debug_flag)
     {
         /* Define field optimiation. */
         if (apirtn == RTN)
          tbol_end_proc();

         /* Report error if out of system resources. */
         if (api_rtn_code != APIOK)
         {
          printf("FATAL ERROR: contact technical support.\n");
          printf("RETURN CODE = %u = %s\n", api_rtn_code, api_err_msg[api_rtn_code]));
          printf("api_return_code = %d\n", api_rtn_code);

          api_rtn_code = APIOK;
         }
         /* Restore instruction pointer. */
         if (ins_inx != 71)       /** C. Hsieh  12/29/86 **/
          ip = dbg_ip;

         if ((cur_offset = Obj_get_offset(stream_ptr)) < 0)
          return (PROG_ERROR);

         /* Adjust debug instruction pointer to reflect new location. */
         if (ins_inx != 71)        /** C. Hsieh  12/29/86 **/
         {
          if (cur_offset >= prev_offset)
              ip += (cur_offset - prev_offset);
          else
              ip -= (prev_offset - cur_offset);
         }

         /* If necessary save debugger environment. */
         if (debug_re_entry == 1)
         {
          /* clear debug re_entry flag. */
          debug_re_entry = 0;

          /* Save debugger environment. */
          if (save_debug_env() == NO_PROCESS)
              return (NO_PROCESS);
         }
         /* Return to debugger. */
         return (NO_ERROR);
     }
     else
     {
         if (api_rtn_code != 0)
         {
          error_code.bytes[CALLER] = API_PROC;
          error_code.bytes[TYPE] = api_rtn_code;
          fatal_error(error_code.bytes[CALLER], error_code.bytes[TYPE]);
          return;
         }
     }
#endif

/* #if PROTOTYPE    removed - SLW */
#if !DEBUG         /* replaces PROTOTYPE */
     if (api_rtn_code != 0)
     {
         fatal_error(error_code.bytes[CALLER] = API_PROC,
               error_code.bytes[TYPE] = api_rtn_code);
         return;
     }
#endif
/* #endif   removed - SLW */
    }

    /* clean up tbol proc, Define field optimization */
    tbol_end_proc();


#ifdef PH
phook(71);
#endif

    return (api_exit_code == 0 ?
     /* Return control to caller. */
         NO_ERROR :
     /* Stop further processing. */
         STOP);
}

/********************************************************************
*
*          **** TBOL INTERPRETER END PROCESS ****
*
*********************************************************************
*
* FUNCTION:           tbol_end_proc()
*
* DESCRIPTION:
*
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   10/17/86        R.G.R.          ORIGINAL
*
**********************************************************************/

tbol_end_proc ()
{
#if DEBUG
    unsigned char db_flag;
#endif
    REG unsigned char artn;

#if DEBUG
    db_flag=Debug_flag;
    Debug_flag = 0;
#endif

/* reset stack pointer instack till equal to level0 */

    artn = apirtn;
    while (instack < level0)
     proc_rtn(0);
    apirtn = artn;

#if DEBUG
    Debug_flag = db_flag;
#endif

    /* Define field optimization */
    if (define_field_flag == 1)
    {
     define_field_flag = 0;
     op_undef_field();
    }

    /* If cursor to field flag is enabled then force cursor movement. */
    if (cursor_flag)
    {
     /* Clear cursor flag. */
     cursor_flag = 0;
     op_clr_cursor();
    }
}

/********************************************************************
*
*          **** BIND TBOL PARAMETERS ****
*
*********************************************************************
*
* FUNCTION:           bind_parameters(prog)
*
* DESCRIPTION:
*        This routine will move the parameters associated with the
* current program object into the interpreter parameter registers.
*
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   01/17/86        R.G.R.          ORIGINAL
*
**********************************************************************/

bind_parameters(prog)
TBOLPROG FAR *prog;
{
    unsigned int total_length;          /* Total parameter length. */
    REG unsigned int parm_length;  /* Length of a parameter. */
    unsigned char FAR *parm;       /* Pointer to parameters. */
    RTA *parm_reg;            /* Pointer to parm reg entry. */
    REG unsigned int preg_count;   /* Number of registers to initialize. */
    unsigned char tmpbuf[IALEN];   /* temp buf for ascii form of #parms */
    unsigned int tmplen;      /* length of tmpbuf */

    /* Return if there are no parameters to bind */
    if (prog->p1 == 0)
     preg_count = 0;
    else
    {
     /* Initialize pointer to parameters. */
     parm = prog->p1;
     parm_reg = &rtatbl[LOCP0 + 1];

     /* Get total length of parameters to be bound */
     total_length = xchg(*(int *)parm) - 2;
     parm += 2;

     for (preg_count=0; total_length && preg_count < 8; preg_count++,
                                      parm_reg++)
     {
         /* Get length of this parameter. */
         parm_length = xchg(*(int *)parm) - 2;
         parm += 2;

         /* Return error if parameter length is zero. */
         if (!parm_length)
          return(PROG_ERROR);

         /* Store parameter pointer in RTA */
         put_buf_in_rta(parm_reg, parm, parm_length);

         /* Adjust parameter pointer */
         parm += parm_length;

         /* Adjust total length. */
         total_length -= parm_length + 2;
     }
    }

    /* format parameter count */
    tmplen = binascii(preg_count, tmpbuf);

    /* store parameter count in P0 */
    put_buf_in_rta(&rtatbl[LOCP0], tmpbuf, tmplen);

    return(NO_ERROR);
}

/********************************************************************
*
*         **** POINT TO FIRST INSTRUCTION OPCODE ****
*
*********************************************************************
*
* FUNCTION:           first_opcode(prog)
*
* DESCRIPTION:
*        This routine will move setup the interpreter instruction
* pointer to point to the first instruction opcode.
*
*
*     DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*   06/11/89        R.G.R.          ORIGINAL
*
**********************************************************************/

first_opcode(prog)
TBOLPROG FAR *prog;
{
    int offset;              /* Instruction offset. */
    REG int status;          /* object status */
    int tmp;                 /* temporary */

    /* If an error occured during a fetch then return with
       no process executed.
    */

    status = prog->prog_obj.status;
    if (status == OBJREFERROR)
     return(PROG_ERROR);

    if (status == OBJREFOBJUNDEFINED)
     return(STOP);

    if (status == OBJREFNOTACCESSED ||
     status == OBJREFFETCHED)
    {
     if ((stream_ptr = Obj_ref_get(&prog->prog_obj)) == NULL)
         return(NO_PROCESS):
    }
    else
    {
     /* Object has been accessed, therefore set up stream pointer. */
     stream_ptr = prog->prog_obj.varptr.streamptr;

     if (Obj_seek(stream_ptr, 0, 0))
         return (NO_PROCESS);
    }

    /* adjust stream_ptr to point to start of TBOL program */
    if (start_of_prog() == NO_PROCESS)
     return(NO_PROCESS);

    /* Now bind tbol parameters. */
    if (bind_parameters(prog) == PROG_ERROR)
     return(PROG_ERROR);

    return(NO_ERROR);
}
