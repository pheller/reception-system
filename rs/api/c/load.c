/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** PROCESS LOAD COMMAND FILE ****
*
*********************************************************************
*
* FILE NAME:               LOAD.C
*
* DESCRIPTION:
*       Contains the process load command module.
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   1?/05/85            R.G.R            ORIGINAL
*
*   16 December '86    Algis             push pop updated to 4.0
*
**********************************************************************/
#include <SMDEF.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <stdio.h>
#include <stdlib.h>
#include <INSERR.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <DBGMACRO.IN>
#include <PACKSTR.IN>
#include <apityp.in>
#include <SYMBOL.IN>
#include <ctype.h>
/**********************************************************************
*
*                  **** DEFINITIONS ****
*
**********************************************************************/
#define FILESIZE (8 * 1024)
#define GO  0
#define NOGO 1
#define MSB  1
#define LSB  0

#define RTN  1
#define NORTN 0
/**********************************************************************
*
*                    **** EXTERNALS ****
*
**********************************************************************/
extern unsigned char FAR *ip;
extern unsigned char FAR *strtloc;
extern unsigned char indata[];

extern struct rta rtatbl[];
extern struct glbvlist FAR  *(*glbmid);
extern struct linklist FAR *(*savelist);

extern WINDOW FAR *W;

extern unsigned char *instack;
extern unsigned char instck[100];

extern int           stack_index;
extern unsigned char intstck[];

extern unsigned char *pstack;
extern unsigned char parmstck[100];

extern unsigned char reset;
extern unsigned char goflg;
extern unsigned char FAR *strtaddr;

extern unsigned char membrknum;
extern unsigned char brknum;

extern unsigned char *Key_Functions;
extern unsigned char key_function_tbl[];

extern unsigned char apirtn;
/**********************************************************************
*
*                 **** VARIABLE DECLARATIONS ****
*
**********************************************************************/
typedef union intary
{
   unsigned char cary[2];
   unsigned int ivar;
}IARY;

#if DEBUG_ALONE

unsigned char bcount;
unsigned char  FAR *dbg_ip;
FILE *fileptr;
unsigned char indata[FILESIZE];
unsigned int numread;

FILE *symptr;
struct symbol symtbl[500];
unsigned int symread;
struct symbol *symrec;
struct symbol *symend;

unsigned char psmode;
unsigned char Debug_flag;
#endif

TBOLPROG tbol_str;
/********************************************************************
*
*               **** PROCESS LOAD COMMAND ****
*
*********************************************************************
*
* ROUTINE NAME:        procload (pointer to file name)
*
* DESCRIPTION:
*              This module will process the load ("L") debug command.
* The TBOL pseudo code and symbol table file will be read from disk
* and placed in system memory.
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   1?/05/85            R.G.R            ORIGINAL
*
**********************************************************************/
procload (fname)

unsigned char *fname;
{
unsigned char index;
unsigned char filename[16];
unsigned char symname[16];
unsigned int cnt;
unsigned char i;
unsigned char *tbuf;
union intary offset;
union intary length;
union intary index3;
union intary name_length;
union intary main;
unsigned int index1;
unsigned int index2;
struct rta *rt;
unsigned char time_date[21];

#if DEBUG_ALONE

/* Show error if filename is not given. */
if(*fname == 0)
{
   /* No filename preesnt show error. */
   printf ("FILENAME REQUIRED!\n");

   /* Return to system. */
   return;
}

/* Copy filename into buffers and add extension. */
/* Search for null and add extension. */
for (index = 0; fname[index] != 0; ++index)
{
    symname [index] = filename [index] = toupper(fname[index]);
}

/* Move . into filename */
filename[index] = '.';
symname[index] = '.';

/* Move extensions. */
++index;
filename[index] = 'C';
symname[index] = 'S';

++index;
filename[index] = 'O';
symname[index] = 'Y';

++index;
filename[index] = 'D';
symname[index] = 'M';

++index;
filename[index] = 0;
symname[index] = 0;


/* Open the file for read. */
if ((fileptr = fopen (filename, "rb")) == NULL)
{
   /* File will not open show error to user. */
   printf ("OPEN FILE ERROR!\n");

   /* Return to system. */
   return;
}

/* Loop and store exit's in object code array. */
for (cnt = 0; cnt < FILESIZE; ++cnt)
{
     indata[cnt] = O_EXIT;
}

/* Read in 8k bytes of data. */
if ((numread = fread (indata,1,FILESIZE,fileptr)) == 0)
{
   printf ("FILE READ ERROR!\n");
   fclose(fileptr);
   return;
}

if ((fclose (fileptr)) != 0)
{
   /* File close error. */
   printf ("FILE CLOSE ERROR.\n");

   /* Return to system. */
   return;
}

/* Open up symbol file. */
if ((symptr = fopen (symname, "r")) == NULL)
{
   /* File will not open show error to user. */
   printf ("OPEN SYMBOL FILE ERROR\n");

   /* Return to system. */
   return;
}

symend = &symtbl[500];
symread = 0;
for (symrec = &symtbl[0]; symread != EOF; ++symrec)
{
     if (symrec >= symend)
     {
        break;
     }

    /* Read in symbol file data. */
    if ((symread = fscanf (symptr, "%s%ld%d%d",&symrec->fldname[0],
                    &symrec->typ, &symrec->len, &symrec->loc)) == 0)
    {
       printf ("SYMBOL FILE READ ERROR\n");
       fclose (symptr);
       return;
    }
}

/* Show termination of records. */
symrec->fldname[0] = 0;

if ((fclose (symptr)) != 0)
{
   /* File close error. */
   printf ("CLOSE SYMBOL FILE ERROR.\n");

   /* Return to system. */
   return;
}

/* Display length of program. */
length.cary[MSB] = indata[0];
length.cary[LSB] = indata[1];


/* Now get location of main procedure.*/
main.cary[MSB] = indata[2];
main.cary[LSB] = indata[3];

/* Get length of program name. */
name_length.cary[MSB] = indata[4];
name_length.cary[LSB] = indata[5];

/* Display date and time. */
index2 = 0;
for (index1 = name_length.ivar + 6; index2 != 20; ++index1, ++index2)
     time_date[index2] = indata[index1];
time_date[index2] = 0;

printf ("%s\n", time_date);
printf ("PROGRAM LENGTH = %u\n", length.ivar);


index2 = 0;
while(length.ivar != 0)
{
   indata[index2++] = indata[index1++];
   --length.ivar;
}

/* Now store exit. */
while (index2 != FILESIZE)
    indata[index2++] = O_EXIT;

/* Call to initialize api. */
api_init ();

/* Initialize pointers and counts. */
strtaddr = strtloc = ip = &indata[0];

bcount = 255;
reset = 0;
goflg = NOGO;


/* Initialize root pointers to GLB and Partition list      */
if ( ( W = (WINDOW *)malloc(sizeof(WINDOW)) ) == NULL)
    return;
W->pvar_mid = 0;

/* Clear number of break count. */
membrknum = 0;
brknum = 0;

/* Initialize KM pointers */
Key_Functions = &key_function_tbl[0];

/* Clear run time debug flag. */
Debug_flag = 0;

/* Clear api system return flag. */
apirtn = NORTN;

/* Set up tbol object prog structure. */
tbol_str.prog_obj.status = OBJREFACCESSED;
tbol_str.p1 = 0;
tbol_str.prog_obj.varptr.objidptr = 0;

#endif

/*d5err_print()
{
}

field_web()
{
}


crt_fld()
{
}
*/
