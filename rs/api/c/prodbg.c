/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** STARTUP DEBUGGER ****
*
*********************************************************************
*
* FILE NAME                PRODBG.C
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
#include <conio.h>
#include <SMDEF.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <SMKEYS.IN>
#include <stdio.h>
#include <stdlib.h>
#include <INSERR.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <DBGMACRO.IN>
#include <PACKSTR.IN>
#include <apityp.in>
#include <SYMBOL.IN>
#include <ctype.h>
#include <ISCB.IN>
#include <DBGSTACK.IN>
/*********************************************************************
*
*                  **** DEFINITIONS ****
*
*********************************************************************/
#define FILESIZE (8 * 1024)
#define GO  0
#define NOGO 1
#define MSB  1
#define LSB  0
#define CONTINUE  0xFFFF
#define RTN  1
#define NORTN 0

/*********************************************************************
*
*                    **** EXTERNALS ****
*
*********************************************************************/
extern unsigned char FAR *ip;
extern unsigned char FAR *strtloc;
extern unsigned char indata[];

extern struct rta rtatbl[];
extern struct glbvlist FAR *(*glbmid);
extern struct linklist FAR *(*savelist);

extern WINDOW FAR *W;

extern unsigned char reset;
extern unsigned char goflg;
extern unsigned char FAR *strtaddr;

extern unsigned char membrknum;
extern unsigned char brknum;
extern Objstream stream_ptr;

extern ISCB *level0;

extern TBOLPROG tbol_str;
extern unsigned char data;
extern unsigned char apirtn;

extern struct EVENT event;

extern unsigned int Sys_api_event;
extern unsigned char alt_display_flag;

/*********************************/
extern unsigned char indata[];
extern unsigned char bcount;
extern unsigned char psmode;
extern struct symbol symtbl[];
extern struct symbol *symrec;
extern struct symbol *symend;

/*********************************************************************
*
*                 **** VARIABLE DECLARATIONS ****
*
*********************************************************************/
typedef union lonptr
{
unsigned char *ptr;
unsigned int iary[2];
}LPTR;

typedef union intary
{
   unsigned char cary[2];
   unsigned int ivar;
}IARY;

typedef struct prog_type
{
unsigned char typ_ary[23];
}PTYP;

PTYP prog_typ_array[] =
{
{"SELECTOR              \0"},
{"FIELD INITIALIZER     \0"},
{"ELEMENT INITIALIZER   \0"},
{"PAGE INITIALIZER      \0"},
{"FIELD POST PROCESSOR  \0"},
{"ELEMENT POST PROCESSOR\0"},
{"PAGE POST PROCESSOR   \0"},
{"ELEMENT HELP          \0"},
{"PAGE HELP             \0"},
{"FILTERED FUNCTION     \0"},
{"OVER_RIDE FILTER FUNC.\0"},
{"RE-ENTER              \0"},
{"TRINTEX_ASSISTANT     \0"},
};

unsigned char FAR *dbg_ip;

FILE *fileptr;
unsigned int numread;

FILE *symptr;
unsigned int symread;
unsigned char symname[16];

unsigned char debug_init;
unsigned char debug_re_entry;

DWS dbg_w_stack[DBG_STACK_SIZE+1];     /* Allow for 16 windows and 30 calls. */
DWS *dbg_w_ptr;
DWS *dbg_w_end;

int tbol_prog_offset;

unsigned char mem_flag;
/********************************************************************
*
*                **** SAVE DEBUGGER ENVIRONMENT ****
*
*********************************************************************
*
* ROUTINE NAME:         save_debug_env ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
save_debug_env()
{

/* Adjust debug env. save stack pointer to next empty entry. */
--dbg_w_ptr;

/* Save debug environment. */
dbg_w_ptr->stack_ip = ip;

if ((dbg_w_ptr->stack_stream_offset = Obj_get_offset(stream_ptr)) < 0)
   return(NO_PROCESS);

return(NO_ERROR);

}
/********************************************************************
*
*                **** RESTORE DEBUGGER ENVIRONMENT ****
*
*********************************************************************
*
* ROUTINE NAME:         restore_debug_env ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
restore_debug_env()
{

if (dbg_w_ptr == dbg_w_end)
    return(NO_PROCESS);

/* Restore debug environment. */
strtloc = strtaddr = ip = dbg_w_ptr->stack_ip;

/* Restore current stream pointer. */
if (Obj_seek(stream_ptr, dbg_w_ptr->stack_stream_offset, 0) != 0)
    return(NO_PROCESS);

/* Adjust debug env. save stack pointer to next entry. */
++dbg_w_ptr;

return(NO_ERROR);
}
/********************************************************************
*
*                **** INITIALIZE DEBUGGER ****
*
*********************************************************************
*
* ROUTINE NAME:         debug_startup ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
debug_startup (prog,run_flag)

TBOLPROG FAR *prog;
unsigned int run_flag;

{

unsigned int  index;
union intary prg_length;
union intary filename_length;
unsigned char symname[80];
union intary main_offset;
union intary start_offset;
unsigned int count;
unsigned int count1;
unsigned int cnt;
unsigned int main_loc;
RTA *rt;
PTYP *typtr;
char tbuf[20], d_t[16], ver[6];
int verflag, i, j;

/* Return if debugger has been initialized. */
if(debug_init)
   return(NO_ERROR);

if (run_flag == START)
{
    if (prog->prog_obj.status == OBJREFOBJUNDEFINED)
       return(STOP);

    /* Switch screen to text mode. */
    wait_icon();
    if(!alt_display_flag){
        event.event = DEBUG_MODE;
        do_display_event (event);
    }

    /* If an error occurre during a fetch then return with error. */
    if(prog->prog_obj.status == OBJREFERROR)
    {
         printf ("OBJECT REFERENCE ERROR\n");
         wait_for_key();
         return(PROG_ERROR);
    }

    if((prog->prog_obj.status == OBJREFNOTACCESSED)
                             ||
       (prog-<prog_obj.status == OBJREFFETCHED))
    {
       if ((stream_ptr = Obj_ref_get(&prog->prog_obj)) == NULL)
       {
                printf("TRINTEX OBJECT GET ERROR\n");
                wait_for_key();
                return(NO_PROCESS);
            }
        } else {
            /* Object has been accssed, therefore set up stream pointer */
            stream_ptr = prog->prog_obj.varptr.streamptr;

            if (Obj_seek(stream_ptr, 0, 0) != 0) {
                printf("RESET STREAM POINTER FAILURE\n");
                wait_for_key();
                return (NO_PROCESS);
            }
        }
    } else {
        /* Sswitch screen to text mode. */
        wait_icon();
        if(!alt_display_flag) {
            event.event = DEBUG_MODE;
            do_display_event(event);
        }

        /* Rest program stream pointer,. */
        if (Obj_seek(stream_ptr, 0, 0) != 0) {
            printf("RESET STREAM POINTER FAILURE\n");
            wait_for_key();
            return (NO_PROCESS);
        }
    }

    /* Adjust stream pointer to point to start of program. */
    if ((Obj_seek(stream_ptr, 22, 0)) != 0) {
        printf("ADJUST STREAM POINTER FAILURE\n");
        wait_for_key();
        return(NO_PROCESS);
    }

    /* Get length(MSB) of tbol program. */
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET MSB PROGRAM LENGTH FAILURE\n");
       wait_for_key();
       return (NO_PROCESS);
    }
    prg_length.cary[MSB] = *ip;

    /* Get length (LSB of tbol program. */
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET LSB PROGRAM LENGTH FAILURE\n");
       wait_for_key();
       return (NO_PROCESS);
    }
    prg_length.cary[LSB] = *ip;

    /* Get offset (MSB) program entry point "main procedure". */
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET MSB PROGRAM ENTRY FAILURE\n");
       wait_for_key();
       return (NO_PROCESS);
    }
    main_offset.cary[MSB] = *ip;

    /* Get offset(LSB) program entry point "main procedure". */
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET LSB PROGRAM ENTRY FAILURE\n");
       wait_for_key();
       return (NO_PROCESS);
    }
    main_offset.cary[LSB] = *ip;

    /* Get length (MBS) of filename. */
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET MSB FILENAME LENGTH FAILURE\n");
       wait_for_key();
       return (NO_PROCESS);
    }
    filename_length.cary[MSB] = *ip;

    /* Get length (LSB) of filename. */
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET LSB FILENAME LENGTH FAILURE\n");
       wait_for_key();
       return (NO_PROCESS);
    }
    filename_length.cary[LSB] = *ip;

    /* Move filename into temporary buffer and attach extension. */
    index = 0;
    for (count = 0; count < filename_length.ivar; count++)
    {
        if ((Get_obj_byte (stream_ptr,ip)) != 0)
        {
           printf ("GET FILENAME FAILURE\n");
           wait_for_key();
           return (NO_PROCESS);
        }
        symname[index] = toupper(*ip);
        index++;
    }
    symname[index++] = '.';
    symname[index++] = 'S';
    symname[index++] = 'Y';
    symname[index++] = 'M';
    symname[index] = 0;

    /*****************************************************************/
    /* move date and time stamps into temporary buffer and attach extension */
    index = 0;
    for (count = 0; count < 20; count++)
    {
         if (( Get_obj_byte (stream_ptr,ip)) != 0 )
         {
              printf ("GET DATE TIME STAMPS FAILURE.\n");
              wait_for_key();
              return (NO_PROCESS);
         }
         tbuf[index] = toupper(*ip);
         index++;
    }

    for (i =0; i<15 ;i++)
         d_t[i] = tbuf[i];
    d_t[15] = 0 ;
    for ( j = 0 ; j < 5 ; ver[j++] = tbuf[i++] );
    ver[5] = 0 ;
    if ( d_t[2] == '/' && d_t[5] == '/' && d_t[11] == ':' )
         verflag = 1 ;
    else
    {
         verflag = 0 ;
         strcpy(d_t,"XX/XX/XX");
         strcpy(ver,"00.0");
    }

    /*****************************   C. Hsieh   *****************************/
    start_offset.ivar = 0;

    /* Compute location to 1'st byte of pseudo_code and start procedure. */
    if (start_offset.ivar != 0)
    {
       count = prg_length.ivar - (filename_length.ivar + 2) - start_offset.ivar - ?

      main_loc = main_offset.ivar - (filename_length.ivar + 2) - start_offset.iva ?

    }else{

       count = prg_length.ivar - (filename_length.ivar + 2) - 6;
       main_loc = main_offset.ivar - (filename_length.ivar + 2) - 4;

    }


    /*************************************************************************/
    if ( verflag == 1 )
    {
         count -= 20 ;
         main_loc -= 20 ;
    }
    /************************************************   C. Hsieh ************/

    count += 2;
    /* Loop and store exits in pseudo code array. */
    for (cnt = 0; cnt < FILESIZE; ++cnt)
         indata[cnt] = O_EXIT;

    /************************************************************************/
    if ( verflag == 0 )
         /* Reset program stream pointer. */
         if ( (Obj_seek(stream_ptr,28+filename_length.ivar,0)) != 0 )
         {
              printf ("ADJUST STREAM POINTER FAILURE\n");
              wait_for_key();
              return (NO_PROCESS);
         }

    /***************************************************  C. Hsieh ***********/

    /* Save program offset. */
    if ((tbol_prog_offset = Obj_get_offset(stream_ptr)) < 0)
    {
        printf ("GET PROGRAM OFFSET FAILURE\n");
        wait_for_key();
        return (NO_PROCESS);
    }

    /* Now move program pseudo code. */
    count1 = count;
    index = 0;

    for(; count != 0; --count)
    {
        if ((Get_obj_byte (stream_ptr,ip)) != 0)
        {
           printf ("GET PROGRAM FAILURE\n");
           wait_for_key();
           return (NO_PROCESS);
        }
        indata[index++] - *ip;
    }

    /* Display logo and enable printer. */
    disp_logo ();

    /************************************************************************/ 
    printf("Program compiled with TBOL COMPILER Version %s\n",ver);
    printf("\Program compiled %s\n",d_t);
    /*****************************************************  C. Hsieh  *******/ 
    /* Display symbol file name. */
    printf ("SYMBOL FILE = %s\n",symname);

    /* Display length of program. */
    printf ("PROGRAM LENGTH = %u\n",count1);

    /* Show that no records have been read. */
    symrec = &symtbl[0];
    symrec->fldname[0]=0;

    /* Open up symbol file. */
    if ((symptr = fopen (symname, "r")) == NULL)
        printf ("OPEN SYMBOL FILE ERROR\n");
        /* goto con_debug; */
    else
    {
         symend = &symbtl[499];
         symred = 0;
         for (symrec = &symtbl[0]; symread != EOF; ++symrec)
         {
              if (symrec >= symend)
              {
                   printf ("SYMBOL TABLE OVERFLOW.\n");
                   break;
              }
              /* Read in symbol file data. */
              if ((symread = fscanf (symptr,"%s%ld%d%d",&symrec->fldname[0],
                            &symrec->typ, &symrec->len, &symrec->loc)) == 0)
                   printf ("FAILURE IN READING SYMBOL FILE.\n");
         }

         /* Show termination of records. */
         symrec->fldname[0] = 0;

         if ((fclose (symptr)) != 0)
            printf ("FAILURE TO CLOSE SYMBOL FILE.\n");
    }

    debug_startup1 (prog,run_flag,main_offset.ivar,main_loc); /* C. Hsieh  */
}

/**********************************************************
 divide function debug_startup() into two functions because of
 compiler optimization.
*************************************  C. Hsieh **********/

debug_startup1 (prog,run_flag,main_offset_ivar,main_loc)
TBOLPROG FAR *prog;
unsigned int   run_flag;
unsigned int    main_offset_ivar;
unsigned int   main_loc;
{
     PTYP *typtr;
     /* Set up program structure. */
   if (run_flag == START)
   {
     tbol_str.prog_obj.status = prog->prog_obj.status;
     tbol_str.p1 = prog->p1;
     tbol_str.prog_obj.varptr.streamptr = prog->prog_obj.varptr.streamptr;
     /* Get first instruction opcode. */
     main_offset_ivar += 22;
     if ((Obj_seek(stream_ptr,main_offset_ivar,0)) != 0)
     {
          printf ("ADJUST TO MAIN MODULE ERROR.\n");
          wait_for_key ();
          return(NO_PROCESS);
     }
     /* Get opcode of first instruction to execute. */
     if ((Get_obj_byte (stream_ptr,ip)) != 0)
     {
          printf("GET OPCODE OF FIRST INSTRUCTION ERROR.\n");
          wait_for_key ();
          return (NO_PROCESS);
     }
          /* Now bind tbol parameters. */
     if ((bind_parameters(prog)) == PROG_ERROR)
     {
          printf ("BIND PARAMETERS ERROR.\n");
          wait_for_key ();
          return (PROG_ERROR);
     }
     /* Initialize pointers and counts. */
     strtaddr = strtloc = ip = &indata[main_loc];

     /* Dispaly program type. */
     typtr = &prog_type_array[Sys_api_event - 1];
          printf ("PROGRAM TYPE = %s\n",typtr);
     }
   bcount = 255;
   reset = 0;
   goflg = NOGO;
   /* Clear number of break count. */
   membrknum = 0;
   brknum = 0;
   if (run_flag == RE_ENTRY)
   {
   /* Call to restore debugger environment. */
      if (restore_debug_env () = NO_PROCESS)
      {
        printf ("RESTORE DEBUGGER ENVIRONMENT ERROR.\n");
             wait_for_key ();
             return(NO_PROCESS);
       }
        /* Move opcode to process within data. */
        data = *ip;
        /* Set up debug tbol structure. */
        tbol_str.prog_obj.status = OBJREFACCESSED;
        tbol_str.p1 = 0;
        tbol_str.prog_obj.varptr.streamptr = stream_ptr;
        /* Inform user that debugger has entered from re-entry req. */
        printf ("TBOL CODE RE_ENTRY!\n");
   }
   /* Set debug initialization flag. */
   debug_init = 1;
   /* Reset system return flag. */
   apirtn = NORTN;
   /* Call for command processing. */
   cmdprc();
   /* Clear debugger initialization flag. */
   debug_init = 0;
   /* Switch to graphics mode. */
   if(!alt_display_flag){
   event.event = USER_MODE;
       do_display_event (event);
      }
   /* Return with no error indication. */
   return (STOP);
}

wait_for_key ()
{
     printf ("TO CONTINUE HIT ANY KEY!\n");
     getch();
     /* Switch to graphics mode. */
     if(!alt_display_flag){
         event.event = USER_MODE;
         do_display_event (event);
        }
}

/********************************************************************
*      **** WARN PROGRAMMER OF POSSIBLE UPCOMING TBOL PROGRAM ****
*********************************************************************
*
* ROUTINE NAME:         warn_prog ()

* DESCRIPTION:
*           This module gets called before a call is made to
* do_function_event. Since the do_function_event module
* can call upon the tbol driver (tboldrv) it became necessary
* to call on the debugger in a recursive manner. THis module will
* inform the system programmer of possible re-entrency.
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
**********************************************************************/
warn_prog (msg_id)

unsigne dint msg_id;            /* Id of warn msg. */

{

if (msg_id)
{
   /* Inform programmer of possible re-entrancy. */
   printf ("THIS API INSTRUCTION WILL ISSUE A CALL TO THE SERVICE MANAGER,\n");
   printf ("WHICH MAY IN TURN INVOKE A NEW TBOL PROGRAM.\n"0;

   /* Wait for keystroke. */
   wait_for_key ();
}

/* Reset debug initialization flag. */
debug_init = 0;

}

/***********************************************************************
*   
*                **** GET TBOL PROGRAM ****
*
***********************************************************************
*
* ROUTINE NAME:         get_tbol_prog ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* ---------------------------------------------------------------------- 
*   11/05/85            R.G.R            ORIGINAL
*
**********************************************************************/
get_tbol_prog ()
{
unsigned int  index;
union intary prg_length;
union intary filename_length;
unsigned char symname[80];
union intary main_offset;
union intary start_offset;
unsigned int count;
unsigned int count1;
unsigned int cnt;
unsigned int main_loc;
RTA *rt;
PTYP *typtr;
char tbuf[20], d_t[16], ver[6];
int  i, j, verflag ;


/* Adjust stream pointer to point to start of program. */
if ((Obj_seek(stream_ptr,22,0)) != 0)
{
   printf ("ADJUST STREAM POINTER FAILURE\n"0;
   wait_for_key();
   return;
}

/* Get length (MSB) of tbol program. */
if ((Get_obj_byte (stream_ptr,ip)) != 0)
{
   printf ("GET MSB PROGRAM LENGTH FAILURE\n");
   wait_for_key();
   return;
}
prg_length.cary[MSB] = *ip;

/* Get length (LSB) of tbol program. */
if ((Get_obj_byte (stream_ptr,ip)) != 0)
{
   printf ("GET LSB PROGRAM LENGTH FAILURE\n");
   wait_for_key();
   return;
}
prg_length.cary[LSB] = *ip;

/* Get offset (MSB) program entry point "main procedure". */
if ((Get_obj_byte (stream_ptr,ip)) != 0)
{
   printf ("GET MSB PROGRAM ENTRY FAILURE\n");
   wait_for_key();
   return;
}
main_offset.cary[MSB] = *ip;

/* Get offset(LSB) program entry point "main procedure". */
if ((Get_obj_byte (stream_ptr,ip)) != 0)
{
   printf ("GET LSB PROGRAM ENTRY FAILURE\n");
   wait_for_key();
   return;
}
main_offset.cary[LSB] = *ip;

/* Get length (MSB) of filename. */
if ((Get_obj_byte (stream_ptr,ip)) != 0)
{
   printf ("GET MSB FILENAME LENGTH FAILURE\n");
   wait_for_key();
   return;
}
filename_length.cary[MSB] = *ip;

/* GET length (LSB) of filename. */
if ((Get_obj_byte (stream_ptr,ip)) != 0)
{
   printf ("GET LSB FILENAME LENGTH FAILURE\n");
   wait_for_key();
   return;
}
filename_length.cary[LSB] = *ip;

* Move filename into temporary buffer and attach etension. */
index = 0;
for (count = 0; count < filename_length.ivar; count++)
{
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET FILENAME FAILURE\n");
       wait_for_key();
       return;
    }
    symname[index] = toupper(*ip);
    index++;
}
symname[index++] = '.';
symname[index++] = 'S';
symname[index++] = 'Y';
symname[index++] = 'M';
symname[index] = 0;

/*****************************************************************/
/* move date and time stamps into temporary buffer and attach extension */
index = 0;
for (count = 0; count < 20; count++)
{
     if (( Get_obj_byte (stream_ptr,ip)) != 0 )
     {
          printf ("GET DATE TIME STAMPS FAILURE.\n");
          wait_for_key();
          return (NO_PROCESS);
     }
     tbuf[index] = toupper(*ip);
     index++;
}

for (i=0; i<15 ;i++)
     d_t[i] = tbuf[i];
d_t[15] = 0 ;
for ( j = 0 ; j < 5 ; ver[j++] = tbuf[i++] );
ver[5] = 0 ;
if ( d_t[2] == '/' && d_t[5] == '/' && d_t[11] == ':' )
     verflag = 1 ;
else
{
     verflag = 0;
     strcpy(d_t,"XX/XX/XX");
     strcpy(ver,"00.0");

}
/*****************************   C. Hsieh   *****************************/ 

start_offset.ivar = 0;

/* COmpute location to 1'st byte of pseudo_code and start procedure. */
if (start_offset.ivar != 0)
{
   count = prg_length.ivar - (filename_length.ivar + 2) - start_offset.ivar - ?

   main_loc = main_offset.ivar - (filename_length.ivar + 2) - start_offset.iv ?

}else{
   count = prg_length.ivar - (filename_length.ivar + 2) - 6;

   main_loc = main_offset.ivar - (filename_length.ivar + 2) - 4;

}

count +=2;
/******************************************************************************/
if ( verflag == 1)
{
     count -= 20 ;
     main_loc -= 20 ;
}
/************************************************   C. Hsieh *****************/
/* Loop and store exits in speudo code array. */
for (cnt = 0; cnt < FILESIZE; ++cnt)
     indata[cnt] = O_EXIT;

/****************************************************************************/
if ( verflag == 0 )
     /* Reset program stream pointer. */
     if ( (Obj_seek(stream_ptr,28+filename_length.ivar,0)) != 0 )
     {
          printf ("ADJUST STREAM POINTER FAILURE\n");
          wait_for_key();
          return (NO_PROCESS);
     }
/***********************************************  C. Hsieh *****************/

/* Save program offset. */
if ((tbol_prog_offset = Obj_get(offset(stream_ptr)) < 0)
{
    printf ("GET PROGRAM OFFSET FAILURE\n");
    wait_for_key();
    return;
}

/* Now move program pseudo code. */
count1 = count;
index = 0;
for(; count != 0; --count)
{
    if ((Get_obj_byte (stream_ptr,ip)) != 0)
    {
       printf ("GET PROGRAM FAILURE\n");
       wait_for_key();
       return;
    }
    indata[index++] = *ip;
}

/* Display LOGO and enable printer. */
disp_logo();

/****************************************************************************/

printf("Program compiled with TBOL COMPILER Version %s\n",ver);
printf("Program compiled %s\n",d_t);

/***********************************************  C. Hsieh *****************/

/* Display symbol file name. */
printf ("SYMBOL FILE = %s\n",symname);

/* Display length of program. */
printf ("PROGRAM LENGTH = %u\n",count1);

/* Open up symbol file. */
if ((symptr = fopen (symname, "r")) == NULL)
{
    printf ("OPEN SYMBOL FILE ERROR\n");
    goto con_debug;

}

symend = &symtbl[499];
symread = 0;
for (symrec = &symtbl[0]; symread != EOF; ++symrec)
{
    if (symrec >= symend)
    {
        printf ("SYMBOL TABLE OVERFLOW.\n");
        break;
    }
    /* Read in symbol file data. */
    if ((symread = fscanf (symptr,"%s%ld%d%d",&symrec->fldname[0],
                    %symrec->typ, &symrec->len, &symrec->loc)) == 0)
       printf ("FAILURE IN READING SYMBOL FILE.\n");
}

/* Show termination of records. */
symrec->fldname[0] = 0;

if ((fclose (symtr)) != 0)
   printf ("FAILURE TO CLOSE SYMBOL FILE.\n");

con_debug :

bcount = 255;
reset = 0;
goflg = NOGO;

/* Clear number of break count. */
membrknum = 0;
brknum = 0;

/* Get first instruction opcode. */
if ((Obj_seek(stream_ptr,0,0)) != 0)
{
   printf ("ADJUST TO FIRST OPCODE ERROR.\n");
   wait_for_key ();
   return(NO_PROCESS);
}

     main_offset.ivar += 22 ;
     if ((Obj_seek(stream_ptr,main_offset.ivar,0)) != 0)
     {
          printf ("ADJUST TO MAIN MODULE ERROR.\n");
          wait_for_key ();
          return(NO_PROCESS);
     }

     /* Get opcode of first instruction to execute. */
     if ((Get_obj_byte (stream_ptr,ip)) != 0)
     {
               printf ("GET OPCODE OF FIRST INSTRUCTION ERROR.\n");
               wait_for_key ();
               return (NO_PROCESS);
     }

/* Initialize pointers and counts. */
strtaddr = strtloc = ip = &indata[main_loc];

/* Display program type. */
typtr = &prog_typ_array[Sys_api_event - 1];

printf ("PROGRAM TYPE = %s\n",typtr);

/* Move opcode to process within data. */
data = *ip;

/* Set up debug tbol structure. */
tbol_str.prog_obj.status = OBJREFACCESSED;
tbol_str.p1 = 0;
tbol_str.prog_obj.varptr.streamptr = stream_ptr;

/* Inform user that debugger has entered from re-entry request. */
printf ("TBOL CODE RE_ENTRY!\n");

/* Set debug initialization flag. */
debug_init = 1;

/* Reset system return flag. */
apirtn = NOTRN;

}

/**********************************************************************
*
*                **** SAVE TBOL PROGRAM ****
*
***********************************************************************
*
* ROUTINE NAME:         save_tbol_prog ()
*
* DESCRIPTION:
*           This module gets called before a call is made to
* do_function_event. Since the do_function_event module
* can call upon the tbol driver (tboldrv) it became necessary
* to call on the debugger in a recursive manner.
*
*     DATE            PROGRAMMER          REASON
* ----------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************** /
save_tbol_prog ()

{
/* Adjust debug env. save stack pointer to next empty entry. */
--dbg_w_ptr;

/* Save debug environment. */
dbg_w_ptr->stack_ip = ip;

if ((dbg_w_ptr->stack_stream_offset = Obj_get_offset(stream_ptr)) < 0)
   return(NO_PROCESS);

/* Save stream pointer. */
dbg_w_ptr->stack_stream = stream_ptr;

}

/**********************************************************************
*
*                **** RESTORE TBOL PROGRAM ****
*
***********************************************************************
*
* ROUTINE NAME:         res_tbol_prog ()
*
* DESCRIPTION:
*           This module gets called after a return from a
* do_function_event call. Since the do_function_event module
* can call upon the tbol driver (tboldrv) it became necessy
* to call on the debugger in a recursive manner.
*
*     DATE            PROGRAMMER          REASON
* ----------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
***********************************************************************/
res_tbol_prog (mode)

unsigned int mode;
{

if (mode)
{
   /* Switch screen to text mode. */
     wait_icon();
     if(!alt_display_flag){
            event.event = DEBUG_MODE;
            do_display_event (event);
        }
}
if (dbg_w_ptr == dbg_w_end)
{
    printf ("DEBUGGER STACK ERROR.\n");
    return;
}

/* Restore current stream pointer. */
stream_ptr = dbg_w_ptr->stack_stream;

/* Get the new tbol program. */
get_tbol_prog ();

if (Obj_seek(stream_ptr, dbg_w_ptr->stack_stream_offset, 0) != 0)
{
    printf ("RESET STREAM POINTER FAILURE\n");
    return;
}

/* Restore instruction opcode location. */
strtloc = strtaddr = ip = dbg_w_ptr->stack_ip;

/* Move opcode to process within data. */
data = *ip;

/* Adjust debug env. save stack pointer to next entry. */
++dbg_w_ptr;

}
