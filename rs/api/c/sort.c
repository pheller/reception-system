/* API     = 1.1.2    LAST_UPDATE = 1.0.1    */
/*---------------------------------------------------------------------
**    SORT.C         Sort and Lookup Verbs
**--------------------------------------------------------------------
**
**  Do the following API instructions:
**
**   SORT sort()
**   LOOKUP    look_up()
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  05/22/86       R.G.R.     ORIGINAL
**  10/06/86       R.D.C.     Updated look_up for V4.0
**  07/30/87       SLW     Rewrite
**  02/09/88       LRZ     fix bug in sort() - call memcmp instead of
**                 asscomp
**-------------------------------------------------------------------*/

#include <SORT.LNT> /* lint args (from msc /Zg) */
#include <SMDEF.IN> /* Service Manager definitions */
#include <DEBUG.IN> /* Debugger compiler switch */
#include <OPERDEF.IN>        /* Operand definitions */
#include <OPERTSTR.IN>       /* Operand structure */
#include <RTADEF.IN>         /* Run time array definitions */
#include <RTASTR.IN>         /* Run time array definitions */
#include <INSERR.IN>         /* Instruction error return codes */
#include <OBJUNIT.IN>
#include <SMPPT.IN> /* Service manager page template */
#include <GEV_DEFS.IN>       /* System return code definitions */
#include <APIUTIL.IN>        /* API utility externs */

/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/
extern struct rta rtatbl[];    /* Run time aray. */

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/
#define DISCONTINUE_SORT 0
#define CONTINUE_SORT     1
#define GT      3
#define NOTFOUND    0
#define FOUND       1

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/

/*---------------------------------------------------------------------
**  sort()
**
**  The API SORT verb.   Sort a range of fields using bubble sort
**  according to some key which is assumed to start at the beginning
**  of each field, and is <key length> bytes long.
**
**  Instruction format:
**
**  opcode     operand#1   operand#2   operand#3
**  ---------------------------------------------
**  sort       first-field last-field  key-length
**
**    DATE       PROGRAMMER     REASON
**--------------------------------------------------------------------
**  05/16/86       R.G.R.      ORIGINAL
**-------------------------------------------------------------------*/

sort()
{
    struct oper *first_field;      /* Pointer to first field operand. */
    struct oper *last_field;       /* Pointer to last field operand. */
    struct oper *key_length;       /* Pointer to key length operand. */
    struct oper *lesser;  /* Pointer to lesser field. */
    struct oper *next_field;       /* Pointer to next field to compare. */
    struct oper *save_entry;       /* Temporary for holding buf ptrs. */
    RTE FAR *l_entry;          /* Pointer to lesser entry. */
    RTE FAR *next_entry;  /* pointer to next entry. */
    unsigned int k_length;     /* Holds key length value. */
    unsigned int end_index, rt_index;    /* Holds run time offset for sort. */
    unsigned char sort_flag;       /* Sort loop flag. */
    unsigned int par_name;     /* Partition variable name. */

    /* Initialize pointers. */
    first_field = &optbl[0];
    last_field = &optbl[1];
    key_length = &optbl[2];
    lesser = &optbl[3];
    next_field = &optbl[4];
    save_entry = &optbl[5];

#if DEBUG
    /* Validate operands. */
    if (first_field->op_type <= LIT_TYPE ||
    last_field->op_type <= LIT_TYPE)
    {
    api_rtn_code = OPER_ERR;
    return;
    }
#endif

    k_length = get_int_from_rta((RTA *)key_length, key_length->op_type);

    /* Set flag to start sort loop. */
    sort_flag = CONTINUE_SORT;

    /* Loop to perform sort. */
    while (sort_flag == CONTINUE_SORT)
    {
    /* Disable sort flag. */
    sort_flag = DISCONTINUE_SORT;

    /* Initialize lesser entry. */
    l_entry = first_field->entry;
    lesser->buffer = l_entry->buffer;

    /* Protect against length compare overflow. */
    lesser->buflen = imin(k_length, l_entry->buflen);

    /* Loop through all fields replacing greater with lesser fields. */
    rt_index = (first_field->rtoff.irtoff + 1);

    /* Adjust index for RDA's  RDC 3/11/88  */
    if( first_field->op_type == RTA_TYPE )
      {
      if( rt_index > RTA1 )
     rt_index -= RTA2 - RTA1 - 1 ;

      end_index = last_field->rtoff.irtoff ;
      if( end_index > RTA1 )
     end_index -= RTA2 - RTA1 - 1 ;
      } ;


    for (; rt_index <= /* RDAs use adjusted end index */
((first_field->op_type==RTA_TYPE)? end_index :last_field->rtoff.irtoff)
               ;++rt_index)
    {
     /* Get next entry occording to type. */
     if (first_field->op_type == RTA_TYPE)
     {
     /* Get next entry to compare. */
     (RTA *)next_entry = &rtatbl[rt_index];
     next_field->buffer = next_entry->buffer;
     }
     else
     {
     if (first_field->op_type == PAR_TYPE)
     {
         /* Compute partition name. */
         par_name = rt_index - ((rt_index<=PAR2) ? (PAR1-1) : PAR2);

         /* Get next entry to compare. */
         if (((int *)next_entry = loc-pev(par_name)) == 0)
         continue;

         /* Process partition variable. */
         proc_pev((PVRA *)next_entry);
     }
     else
     {
         /* Get next entry to comapre. */
         if (((int *)next_entry = loc_gev(rt_index - GLB1)) == 0)
         continue;
         else
         /* Process global variable. */
         global_proc((GVAR *)next_entry);
     }
     }
     next_field->buffer = next_entry->buffer;

     /* Protect against compare overflow. */
     next_field->buflen = imin(k_length, next_entry->buflen);

     /* Perform comparison. */
     if (memcmp(lesser->buffer,next_field->buffer,next_field->buflen) >0)
               /* do a memcmp instead of asccomp - LRZ 2/9/88 */
     {
          /* Swap lesser (next field) with greater field. */
          save_entry->buffer = next_entry->buffer;
          save_entry->buflen = next_entry->buflen;
          next_entry->buffer = l_entry->buffer;
          next_entry->buflen = l_entry->buflen;
          l_entry->buffer = save_entry->buffer;
          l_entry->buflen = save_entry->buflen;

          /* Set sort flag to continue sort. */
          sort_flag = CONTINUE_SORT;
         }

         /* Set up next lesser field. */
         lesser->buffer = next_entry->buffer;

         /* Protect against compare overflow. */
         lesser->buflen = imin(k_length, next_entry->buflen);

         l_entry = next_entry;
     }
    }
}

/*---------------------------------------------------------------------
**  look_up()
**
**   The API LOOKUP verb.  Locate a particular entry in a table of
**   data held in a single variable.
**
**  Instruction format:
**
**  opcode        operand#1   operand#2   operand#3  operand#4   operand5
**  ------------------------------------------------------------------
**  look_up         key    table    entry-length  offset   strt_pos
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/16/86        R.G.R.        ORIGINAL
**  10/06/86        R.D.C.        Updated for V4.0
**  12/15/87        SLW      Rewrite for efficiency, and
                        does variable-length lookup
                        if record length is 0.
**  To do variable length lookup, specify record length of 0.  If
**  offset is 0, then lookup will find an exact match only for the key.
**  If offset is 1, then lookup will assume the records are in alphabetical
**  order and will stop as soon as a record is greater than or equal to the
**  key, and the strt_pos will be set to that point.  SYS_RETURN_CODE is
**  set for variable length lookups:
**
**   RET_NOT_FOUND - The key was not found
**   RET_OK          - The key was matched exactly
**-------------------------------------------------------------------*/

look_up()
{
    struct oper *key;          /* Pointer to key operand. */
    struct oper *table;   /* Pointer to table operand. */
    struct oper entry_len;     /* Pointer to entry-length operand. */
    struct oper *offset;  /* Pointer to key-field offset operand. */
    struct oper *strt_pos;     /* Pointer to a varialble to receive the start
                      /* position of the "found record". */
    register int e_len;    /* Holds value of entry length. */
    int o_set;             /* Holds value of offset. */
    unsingned int index;   /* Holds current buffer index. */
    unsigned char FAR *tp;      /* Table pointer */
    unsigned char FAR *ep;      /* End table pointer */
    register int keylen;   /* length of key */
    char buf[256] ;

    /* Initialize pointers */
    key       = &optbl[0] ;
    table     = &optbl[1] ;
    entry_len = &optbl[2] ;
    offset    = &optbl[3] ;
    strt_pos  = &optbl[4] ;

#if DEBUG
    /* Validate operands. */
    if (key->op_type <= REG_TYPE ||
     table->op_type <= LIT_TYPE ||
     strt_pos->op_type == LIT_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    |
#endif

    e_len = get_int_from_rta((RTA *)entry_len, entry_len->op_type);
    o_set = get_int_from_rta((RTA *)offset, offset->op_type) - 1;

    /* Perform lookup. */

    memcopy( buf, key->buffer, key->buflen ) ;
    buf[key->buflen] = 0;

    keylen = key->buflen;
    if (e_len)      /* fixed-length records */
    {
     /* NOTE this loop will terminate if it the next record isn't
        long enough to contain both the offset and the key-field. */
     for (tp=table->buffer+o_set, ep=table->buffer+table->buflen;
           tp+keylen <= ep; tp+= e_len)
         if (!strncmp(key->buffer, tp, keylen))
          goto found;
    }
    else      /* variable-length records */
    {
     /* doesn't care if length matches, because in practice an arbitrary
        number of nulls may be attached.  To avoid matching to a longer
        string (i.e. whale matching whalers), check for the terminating
        null if it could fit in the variable size. */

     Sys_rtn_cde = RC_NOT_FOUND;
     index = 1;
     for (tp=table->buffer, ep=table->buffer+table->buflen;
           tp<ep; tp += *tp + 1, index++)
         if ((e_len=strncmp( buf, tp+1, keylen(*tp>keylen))) <= 0 &&
          (!e_len || o_set>=0))
          goto found;
     put_int_in_rta(0, (RTA *)offset->entry, offset->op_type);
    }

    index = 0;
    goto notfound;

found:
    index = tp - table->buffer - o_set + 1 ; /* correct index origin */
    goto notfound;

found2:
    put_int_in_rta(index, (RTA *)offset->entry, offset->op_type);
    index = tp - table->buffer + 1;          /* correct index origin */
    Sys_rtn_code = e_len ? RC_SOME_ONES : RC_OK;

notfound:
    /* Save index of "found record" to operator 5 (start position) */
    put_int_in_rta(index, (RTA *)strt_pos->entry, strt_pos->op_type);
}

