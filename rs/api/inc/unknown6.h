/* window.c */
/*global*/  int proc_window(unsigned int );
/*global*/  int open_wind(void);
/*global*/  int opn_e_wind(void);
/*global*/  int opn_e_w_name(void);
/*global*/  int close_wind(void);
/*global*/  int close_open_wind(void);
