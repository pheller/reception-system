/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** OPERAND TABLE STRUCTURE ****
*
*********************************************************************
*
* FILE NAME:            OPERTSTR.IN
*
* DESCRIPTION:  
*           This file contains the operand table structure.
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   02/10/86            R.G.R.           ORIGINAL
*   03/21/86            ALGIS            UPDATE
*
**********************************************************************/
/* Union for run time offset byte access. */
union offset
{
    unsigned int irtoff;            /* Used to access complete offset. */ 
    unsigned char artoff[2];        /* Used for offset byte access. */     
};

/* General structure of run time entry. */
typedef struct runtime_entry
{
   unsigned char FAR *buffer;        /* Ptr to string buffer. */
   unsigned int buflen;              /* Length of string. */
}RTE;

/* Structure of table which contain resolve operands. */
typedef struct oper
{
   unsigned char FAR *buffer;        /* Ptr to operand string buffer. */
   unsigned int buflen;              /* Length of operand string. */
   RTE  FAR *entry;                  /* Ext. run time entry pointer. */
   unsigned char cnv_type;           /* Operand converted type. */
   union offset rtoff;               /* Run time offset. */
   unsigned char op_type;            /* Operand type. */
}OPER;

/* Define converted operand types. */
#define NOTYP    0                   /* No type specified. */
#define ASCTYP   1                   /* Type is ascii. */
#define BINTYP   2                   /* Type is int. */
#define BCDTYP   3                   /* Type is pack bcd. */

#define INT_TYPE   0                 /* Integer register. */
#define DEC_TYPE   1                 /* Decimal register. */
#define LIT_TYPE   2                 /* Literal type. */
#define RTA_TYPE   3                 /* Run time array. */
#define PAR_TYPE   4                 /* Partition variable. */
#define GLB_TYPE   5                 /* Global variable. */

#define REG_TYPE   1                 /* Register type. */
#define ASC_TYPE   2                 /* Ascii field type. */
#define EXT_TYPE   4                 /* External variable type. */
/* procoper.c */
/*global*/  int procoper(unsigned int ,unsigned int ,unsigned int );
/*global*/  unsigned char *get_lit(unsigned int );
/*global*/  int end_opers(void);
/*global*/  int procop_getbyte(void);
/* pushpop.c */
/*global*/  int push(void);
/*global*/  int pop(void);
