/* RSK     = 1.1.4    LAST_UPDATE = 1.1.2    */
/*
   This file is consistent with 6.2.0 and includes updates by LH
   current as of 6/20/88.
   
   NOTE: this version of cmc.c does not require the 7.0 version of
   sys_type.in.
*/
#include <stdlib.h>
#include <omnibus.in>
#include <sys_type.in>
#include <string.h>
#ifdef TEST
#include <assert.h>
#include <ctype.h>
#include <time.h>
#endif
#if VERSION == (MAC | FINDER | MPW)
#define __SEG__  cmc
#endif

/*
   1/28/88   LH    at the request of Dave Miller, moved size calculation
   from send_cmc() to format_cmc().
   
   This version compatible with 6.0.4.  Changes made to accomodate
   the Mac port.  LH 1/28/88.
   
   12/8/87    LH   replaced MEMMOVE() with MEMMOVE() for MSC 5.0 compile.
   
   9/18/87    LH   Original code.  Comments follow (updated 6/20/88) :
   
   
           CMC REPORTING ON FATAL ERROR
   
   The function cmc() is meant to be called from fatal_error(), e.g.:
   
      if(( err_code.return_code = setjmp(Mark) ) != 0 )
      {
         unitid = err_code.bytes[CALLER];
         errorcode = err_code.bytes[TYPE];
         cmc(unitid, errorcode);
            .
            .
            .
      }
   At present cmc() sends the following up to TCS:
   
      . Type 0 DIA Header
      . Type 9 DIA Header
      . FM9 Message Text
   
   Here are the specifics for various fields.  Invariant fields have
   been tagged with a *.
   
      TYPE 0 DIA HEADER
      
         Byte 0, header length -------* FM0LEN (0x10)
              1, header type ---------* FM0_ (0x80, concatenated hdr present)
              2, function code -------* 0 (application message)
              3, data mode -----------* 0
              4 - 7, source ID -------* 0,0,0,0
              8, logon sequence no ---* 0
              9, message sequence no -- appropriate value
              10 - 13, destination ID * 0,2,2,0
              14 - 15, text length ---- 0, appropriate value (length of FM9
                                        header + length of text)
      
      TYPE 9 DIA HEADER
      
         Byte 0, header length -------* FM9LEN (6)
              1, header type ---------* FM9 (9, no concatenated hdr present)
              2, function code -------* 3 (alert)
              3, reason code ---------* 2 (reception-originated alert msg)
              4, flags ---------------* 0x90 (store by key, ASCII)
              5, text length ---------* appropriate value
      
      FM9 MESSAGE TEXT
      
         Byte 0-6, system user ID ----- user id, right-padded with '?'
              7 ----------------------* ' '
              8 ----------------------* ' '
              9, system origin -------* 'T' (Trintex)
              10 - 12, msg origin ----* 'PCM' (PC message)
              13 - 14, unit ID -------- appropriate ASCII decimal digits
                                        ('10' typical')
              15 - 16, error code ----- appropriate ASCII decimal digits
                                        ('02' typical)
              17, severity level -----* 'E'
              18 ---------------------* ' '
              19, error threshold ----* '001'
              22 ---------------------' ' '
              23 - 30, date ----------- ('05231988' typical)
              31 - 36, time ----------- ('143023' typical)
              37 - 41, api event ------ ('00003' typical)
              42 - 49, memory to start- ('00227472' typical)
              50 - 54, DOS version ---- ('03.30' typical)
              55 - 62, RS version ----- ('6.01.XX ' typical)
              63 - 73, window ID ------ ('NOWINDOWIDX' typical)
              74 - 77, last 2 bytes of window ID
                  in ASCII hex format - ('0104' typical)
              78 - 88, selected object - ('NOSELECTORX' typical)
              89 - 92, last 2 bytes of selected object ID
                  in ASCII hex format - ('0104' typical)
              93 - 103, base object --- ('PIOT0010MAP' typical)
              104 - 107, last 2 bytes of base object ID
                  in ASCII hex format - ('0104' typical)
              108 - 120, keyword ------ ('QUOTE TRACK  ' typical)
   
   The code presented in this module (cmc.c) is the front end of
   the CMC handler.  The back end, which packetizes the code and
   sends it up the line, can be found in cmc_ibm.c.
   
   IMPORTANT NOTE, added 6/20/88.  Specifications for certain items
   may change; some have not been published.  In the foregoing table
   constant fields are marked *.  When updating this module, check
   to be sure those constants stay the same.  For example, the
   source and destination ID in the type 0 DIA header may become
   variable as Prodigy sites proliferate.  Hard-coded values should
   be viewed with suspicion.  Programmer's paranoia is ALWAYS justified.
   
   Here are the raw specs for global values that go into the FM9 message
   text, as they currently appear in the reception system:
   
      unit ID  := numeric value from 1 through 16 (see err.in)
      error code := numeric value from 0 through 72 (see *err*.in)
      API event := numeric value from 1 through 13 (see gev_defs.in)
      user ID := seven alpha characters
      window ID := eleven alpha characters followed by two binary bytes
      selected object ID := eleven alpha characters followed by two binary bytes
      base object ID := eleven alpha characters followed by two binary bytes
      keyword := thirteen alpha characters
   
   CMC assumes that all the above values will be present in the form stated,
     except the window ID, the selected object data, and the user ID, any or
     all of which may be missing (null string and zero length in the GEV).
  
(End of 6/20/88 note.)

CMC handling works as follows:

  somebody has a fatal error and calls fatal_error()
     fatal_error() does a longjmp() to sys_error()
        sys_error() passes unit id and error code to cmc()
           cmc() calls format_cmc() to build CMC message
           cmc() calls send_cmc() to send message up to TCS
              send_cmc() puts message into packets and,
                 for each packet,
                    in a countdown timer loop,
                       if the TXSEM semaphore is available,
                          claims TXSEM
                          calls send_dd() to send the packet to TCS
                             send_dd() clears TXSEM when t'mission complete
              when no more packets to send, send_cmc() takes TXSEM forever
        sys_error() calls dcon_lnk() to disconnect
        sys_error() calls exit(1)

Here are some possible scenarios:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CM task is running
TXSEM is free
CM has fatal error

         . loop until TXSEM is free -- happens immediately
         . claim TXSEM, transmit CMC packet via send_dd()
         . wait for TXSEM to be freed -- should happen soon
         . claim TXSEM forever

-- Should always work, since there are no fatal errors in the send_dd()
   path.  (FREE_MEMORY() never does an error return.)  Has been tested.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SM task is running
TXSEM is free
SM has fatal error

         . loop until TXSEM is free -- happens immediately
         . claim TXSEM, transmit CMC packet via send_dd()
         . wait for TXSEM to be freed
         . claim TXSEM forever
-- As above, should always work.  Has been tested.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SM task is running
TXSEM is not free
SM has fatal error

         . loop until count is exhausted or TXSEM is free
         . CM wakes up at interrupt level and clears TXSEM
         . claim TXSEM, transmit CMC packet via send_dd()
         . wait for TXSEM to be freed
         . claim TXSEM forever

-- This has been tested.  It worked, and should always work.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CM task is running
TXSEM is not free
CM has fatal error

         . loop until count is exhausted or TXSEM is free

-- Since CM is looping, it can't free the semaphore.  In fact, tests
   show that this case doesn't work.
   
   This can happen if GET_MEMORY() in respond() returns an error.
   Since respond() is invoked only at connect and disconnect, and
   the amount of memory in question is just four bytes, it seems wise
   to use static memory in respond().  Sam S. is willing to make the
   change.
   
   LH, 5/24/88: Updated to include new information requested by
   Larry A.
   
   LH, 5/25/88: Updated yet again to provide ASCII hex characters
   for the last two bytes of the base object ID.
   
   LH, 5/31/88: Bug fix -- the call
   ljrp(SYS_WINDOW_ID, ljrp(SYS_VERSION, rjzp((LONG_S)_osminor, ptr, 2), 8), ?)
   was made with 8 in place of the 13.  This had to be corrected.  At the same
   time I added a bit of logic to ljrp() to avoid problems in the future where
   field widths might exceed <fieldSize>.
   
   LH, 6/16/88: Bug fix -- fm0.txtlen was 0x1d00, should've been updated to
   0x7b00.  See comments of even date.
   
   LH, 6/21/88: Requested by Larry -- if user ID is less than 7 bytes, pad
   with '?'.  (Previously we simply inserted '???????' if user ID was 0
   length.)
*/

/****************************************************************************
*                                                                           *
*                                 CONSTANTS                                 *
*                                                                           *
****************************************************************************/
#define DESTINATION_ID "\0\2\2\0"              /* destination of CMC report */
#define SOURCE_ID "\0\0\0\0"
#define MAX_TEXT_SIZE   200          /* max size for text sent back up line */
#define LOCAL static     /*  define to keyword "static" for production code */

/****************************************************************************
*                                                                           *
*                                   MACROS                                  *
*                                                                           *
****************************************************************************/
#define min(a,b)   (((a> < (b)) ? (a) : (b))

/****************************************************************************
*                                                                           *
*                                 INCLUDES                                  *
*                                                                           *
****************************************************************************/
#include <pkt.h>
#include <sem.h>
#include <error.in>
#include <d7dia.in>
#include <diahdr.in>
#include <gev_defs.in>
#include <stdio.h>

/****************************************************************************
*                                                                           *
*                                 TYPEDEFS                                  *
*                                                                           *
****************************************************************************/

typedef struct
{
   DIAFM0 fm0;
   DIAFM9 fm9;
   CHAR_U txtbuf[MAX_TEXT_SIZE];
} ERROR_PACKAGE;

/****************************************************************************
*                                                                           *
*                           PROTOTYPES                                      *
*                                                                           *
****************************************************************************/
extern void send_cmc(ERROR_PACKAGE *, INT_U);
CHAR_S * near ljrp(INT_U, CHAR_S *, INT_S);
CHAR_S * near rjzp(LONG_S, CHAR_S *, INT_S);
static INT_U format_cmc(ERROR_PACKAGE *, CHAR_U, CHAR_U);
void cmc(CHAR_U, CHAR_U);
#if TEST
static void checker(ERROR_PACKAGE *, CHAR_S, CHAR_S);
static void show(ERROR_PACKAGE *);
static void fakeTime(CHAR_S [], CHAR_S []);
#endif

/****************************************************************************
*                                                                           *
*                            GLOBAL VALUE                                   *
*                                                                           *
****************************************************************************/
INT_U msg_seq_no;                    /* seq number of last FM0 -- see iht.c */


/****************************************************************************
*                                                                           *
*                               GLOBAL FUNCTION                             *
*                                                                           *
****************************************************************************/
/*
   LH, 9/9/87.  This function is part of an effort to regularize and streamline
   reporting of fatal errors.  We want to report a given error up the line.
   The fatal error handler (see sys_err.c) invokes cmc(), passing it a unit ID
   and error code.
   
   Some of the code that follows was adapted from Mike Gordon's ermunit.c,
   from Connie Hsieh's builddia.c, and from Sam Sayegh's iht.c/pkt.c.
   
   Since a possible fatal error is OUT OF MEMORY, we can't use dynamic memory
   space to build the DIA headers and text.  Instead we'll put them on the
   stack by declaring <error_package> as a local variable.  IMPORTANT: be
   sure to leave seven bytes of slop in MAX_TEXT_SIZE.  We need that much
   extra space in the package for the TCS prefix and suffix info.  If
   MAX_TEXT_SIZE gets very big we'll have to make <error_package> static.
*/
void
cmc(unit_id, error_code)
CHAR_U unit_id, error code;
{
   ERROR_PACKAGE error_package;     /* be sure stack is big enough for this */
   
   send_cmc(&error_package, format_cmc(&error_package, unit_id, error_code));
}

/****************************************************************************
*                                                                           *
*                               LOCAL FUNCTION                              *
*                                                                           *
****************************************************************************/
/*
   Note from LH, 5/20/88: get_sys_gev() is risky at this point, since we're
   suffering from a fatal error.  Look directly in sys_var_table[] for
   GEV values.
   
   format_cmc() builds the error package to be send up the line and returns
   its byte count for use as an argument to send_cmc().
*/
static INT_U
format_cmc(err_pkg_ptr, unit_d, error_code)
ERROR_PACKAGE *err_pkg_ptr;
CHAR_U unit_id, error_code;
{
   CHAR_U *ptr, *ptr1;
   CHAR_U buf[14];
   INT_S i;                                        /* for conversions, etc. */
   extern CHAR_U seq_num; /* message sequence number (global from d7omcm.c) */
   extern LONG_U StartMem;            /* amt of memory available at startup */
   extern systbl sys_var_table[];
   
   ptr = err_pkg_ptr->txtbuf;
/*
   New treatment of user ID, as of 6/21/88: if it's less than seven
   characters in length, it'll be padded with '?'.  If the length is
   greater than seven chars, excess will be truncated.
*/
   ptr1 = *(CHAR_S **) sys_var_table[SYS_USER_ID - 1].syspp.sys_str_addr;
   i = (i = sys_var_table[SYS_USER_ID - 1].sys_length) > 7 ? 7 : i;
   MEMMOVE(ptr, ptr1, i);                /* user ID */
   for (ptr += i; i < 7; ++i, ++ptr)
      *ptr = '?';                              /* pad with '?' if necessary */
   *ptr++ = ' ';                                     /* fill to 8 positions */
   *ptr++ = ' ';                                                /* reserved */
/*
   LH, 6/16/88: I haven't heard a suggestion to change the next byte to
   'P', but will ask if that's in the works.
*/
   *ptr++ = 'T';                                  /* system origin: Trintex */
   *ptr++ = 'P';                              /* message origin: PC message */
   *ptr++ = 'C';                                               /*  2nd char */
   *ptr++ = 'M';                                      /*  3rd and last char */
   *ptr++ = unit_id / 10 + '0';                      /* 1st char of unit ID */
   *ptr++ = unit_id % 10 + '0';                      /* 2nd char of unit ID */
   *ptr++ = error_code / 10 + '0';                /* 1st char of error code */
   *ptr++ = error_code % 10 + '0';                /* 2nd char of error code */
   *ptr++ = 'E';                                          /* severity level */
   *ptr++ = ' ';                                                /* reserved */
   *ptr++ = '0';                                    /* error threshold: 001 */
   *ptr++ = '0';
   *ptr++ = '1';
   *ptr++ = ' ';                            /* remainder of the information */
/*
   Format for data added 5/24/88:
   
      Item                Format (D = decimal digit, A = alphanumeric,
