/* RSK     = 1.1.4    LAST_UPDATE = 1.       */
/********************************************************************
*
*                **** IBM PHYSICAL KEY TO ****
*                **** TRINTEX LOGICAL KEY ****
*
********************************************************************* 
*
* FILE NAME:   ibm_ptol.in
*
* DESCRIPTION:
*         Include file for service manager containing;
*                   Map of IBM physical keys to
*              Trintex logical kens.
*
*         NOTE:     This include file will need to be changed
*              when transporting to other machines.
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*    25 APRIL '86       A.J.M.           ORIGINAL
*
*    25 November '86    Algis            add prt_srn key to f9
*
**********************************************************************/

unsigned char           physical_to_logical[] =  {

     NULL_KEY,       /* _null_               0       */
     NULL_KEY,      /* _soh_       1    */
     NULL_KEY,      /* _stx_       2    */
     NULL_KEY,      /* _etx_       3    */
     NULL_KEY,      /* _eot_       4    */
     NULL_KEY,      /* _enq_       5    */
     NULL_KEY,      /* _ack_       6    */
     NULL_KEY,      /* _beep_      7    */
     BACKSPACE_KEY,      /* _backspace_      8    */
     TAB_FORWARD_KEY,    /* _tab_forward_    9    */
     NULL_KEY,      /* _line_feed_      10   */
     NULL_KEY,      /* _vertical_tab_   11   */
     NULL_KEY,      /* _form_feed_      12   */
     COMMIT_KEY,         /* _carriage_return_     13   */

     NULL_KEY,      /*             14   */
     NULL_KEY,      /*             15   */
     NULL_KEY,      /*             16   */
     NULL_KEY,      /*             17   */
     NULL_KEY,      /*             18   */
    NULL_KEY,       /*             19   */
     NULL_KEY,      /*             20   */
     NULL_KEY,      /*             21   */
     NULL_KEY,      /*             22   */
     NULL_KEY,      /*             23   */
     NULL_KEY,       /*          24     */
     NULL_KEY,      /*             25   */
     NULL_KEY,      /*             26   */

     CLOSE_WINDOW_KEY,   /* _escape_         27   */

     NULL_KEY,      /*             28   */
     NULL_KEY,       /*          29     */
     NULL_KEY,      /*             30   */
     NULL_KEY,      /*             31   */

     SPACE_BAR,          /* _space_     32   */

     TEXT_KEY,      /* _exclamation_point_   33   */
     TEXT_KEY,      /* _double_quote_   34   */
     TEXT_KEY,      /* _pound_sign_          35   */
     TEXT_KEY,      /* _dollar_sign_    36   */
     TEXT_KEY,      /* _percent_sign_   37   */
     TEXT_KEY,      /* _ampersand_      38   */
     TEXT_KEY,      /* _left_quote_          39   */
     TEXT_KEY,      /* _left_parens_    40   */
     TEXT_KEY,      /* _right_parens_   41   */
     TEXT_KEY,      /* _asterisk_       42   */
     TEXT_KEY,      /* _plus_      43   */
     TEXT_KEY,      /* _comma_          44   */
     TEXT_KEY,      /* _minus_          45   */
     TEXT_KEY,      /* _period_         46   */
     TEXT_KEY,      /* _slash_          47   */

     N0_KEY,             /* _0_              48   */
     N1_KEY,             /* _1_              49   */
     N2_KEY,             /* _2_              50   */
     N3_KEY,             /* _3_              51   */
     N4_KEY,             /* _4_              52   */
     N5_KEY,             /* _5_              53   */
     N6_KEY,             /* _6_              54   */
     N7_KEY,             /* _7_              55   */
     N8_KEY,             /* _8_              56   */
     N9_KEY,             /* _9_              57   */

     TEXT_KEY,      /* _colon_          58   */
     TEXT_KEY,      /* _semi_colon_          59   */
     TEXT_KEY,      /* _less_than_      60   */
     TEXT_KEY,      /* _equals_         61   */
     TEXT_KEY,      /* _greater_than_   62   */
     HELP_KEY,      /* _question_mark_  63   */
     TEXT_KEY,      /* _at_sign_        64   */

     A_KEY,              /* _A_              65   */
     B_KEY,              /* _B_              66   */
     C_KEY,              /* _C_              67   */
     D_KEY,              /* _D_              68   */
     E_KEY,              /* _E_              69   */
     F_KEY,              /* _F_              70   */
     G_KEY,              /* _G_              71   */
     H_KEY,              /* _H_              72   */
     I_KEY,              /* _I_              73   */
     J_KEY,              /* _J_              74   */
     K_KEY,              /* _K_              75   */
     L_KEY,              /* _L_              76   */
     M_KEY,              /* _M_              77   */
     N_KEY,              /* _N_              78   */
     O_KEY,              /* _O_              79   */
     P_KEY,              /* _P_              80   */
     Q_KEY,              /* _Q_              81   */
     R_KEY,              /* _R_              82   */
     S_KEY,              /* _S_              83   */
     T_KEY,              /* _T_              84   */
     U_KEY,              /* _U_              85   */
     V_KEY,              /* _V_              86   */
     W_KEY,              /* _W_              87   */
     X_KEY,              /* _X_              88   */
     Y_KEY,              /* _Y_              89   */
     Z_KEY,              /* _Z_              90   */

     TEXT_KEY,      /* _left_brakcet_    91  */
     TEXT_KEY,      /* _backslash_       92  */
     TEXT_KEY,      /* _right_bracket_   93  */
     TEXT_KEY,      /* _carot_           94  */
     TEXT_KEY,      /* _underscore_          95   */
     TEXT_KEY,      /* _apostrophe_          96   */

                    /* Lowercase alphas      */

     LA_KEY,             /* _a_              97   */
     LB_KEY,             /* _b_              98   */
     LC_KEY,             /* _c_              99   */
     LD_KEY,             /* _d_              100  */
     LE_KEY,             /* _e_              101  */
     LF_KEY,             /* _f_              102  */
     LG_KEY,             /* _g_              103  */
     LH_KEY,             /* _h_              104  */
     LI_KEY,             /* _i_              105  */
     LJ_KEY,             /* _j_              106  */
     LK_KEY,             /* _k_              107  */
     LL_KEY,             /* _l_              108  */
     LM_KEY,             /* _m_              109  */
     LN_KEY,             /* _n_              110  */
     LO_KEY,             /* _o_              111  */
     LP_KEY,             /* _p_              112  */
     LQ_KEY,             /* _q_              113  */
     LR_KEY,             /* _r_              114  */
     LS_KEY,             /* _s_              115  */
     LT_KEY,             /* _t_              116  */
     LU_KEY,             /* _u_              117  */
     LV_KEY,             /* _v_              118  */
     LW_KEY,             /* _w_              119  */
     LX_KEY,             /* _x_              120  */
     LY_KEY,             /* _y_              121  */
     LZ_KEY,             /* _z_              122  */

     TEXT_KEY,      /* _left_brace_          123  */
     TEXT_KEY,      /* _vertical_bar_   124  */
     TEXT_KEY,      /* _right_brace_        125   */
     TEXT_KEY,      /* _not_       126  */

     NULL_KEY,      /*             127  */
    NULL_KEY,       /*             128+0     */
    NULL_KEY,       /*             128+1     */
    NULL_KEY,       /*             128+2     */
    NULL_KEY,       /* _nul_  128+3     */
    NULL_KEY,       /*             128+4     */
    NULL_KEY,       /*             128+5     */
    NULL_KEY,       /*             128+6     */
    NULL_KEY,       /*             128+7     */
    NULL_KEY,       /*             128+8     */
    NULL_KEY,       /*             128+9     */
    NULL_KEY,       /*             128+10    */
    NULL_KEY,       /*             128+11    */
    NULL_KEY,       /*             128+12    */
    NULL_KEY,       /*             128+13    */
    NULL_KEY,       /*             128+14    */

     TAB_BACK_KEY,       /* _tab_back_       128+15    */

     NULL_KEY,      /* _a_q_       128+16    */
     NULL_KEY,      /* _a_w_       128+17    */
     NULL_KEY,      /* _a_e_       128+18    */
     NULL_KEY,      /* _a_r_       128+19    */
     NULL_KEY,      /* _a_t_       128+20    */
     NULL_KEY,      /* _a_y_       128+21    */
     NULL_KEY,      /* _a_u_       128+22    */
     NULL_KEY,      /* _a_i_       128+23    */
     NULL_KEY,      /* _a_o_       128+24    */
     NULL_KEY,      /* _a_p_       128+25    */

    NULL_KEY,       /*             128+26    */
    NULL_KEY,       /*             128+27    */
    NULL_KEY,       /*             128+28    */
    NULL_KEY,       /*             128+29    */

     NULL_KEY,      /* _a_a_       128+30    */
     NULL_KEY,      /* _a_s_       128+31    */
     NULL_KEY,      /* _a_d_       128+32    */
     NULL_KEY,      /* _a_f_       128+33    */
     NULL_KEY,      /* _a_g_       128+34    */
     NULL_KEY,      /* _a_h_       128+35    */
     NULL_KEY,      /* _a_j_       128+36    */
     NULL_KEY,      /* _a_k_       128+37    */
     NULL_KEY,      /* _a_l_       128+38    */

    NULL_KEY,       /*             128+39    */
    NULL_KEY,       /*             128+40    */
    NULL_KEY,       /*             128+41    */
    NULL_KEY,       /*             128+42    */
    NULL_KEY,       /*             128+43    */

     NULL_KEY,      /* _a_z_       128+44    */
     NULL_KEY,      /* _a_x_       128+45    */
     NULL_KEY,      /* _a_c_       128+46    */
     NULL_KEY,      /* _a_v_       128+47    */
     NULL_KEY,      /* _a_b_       128+48    */
     NULL_KEY,      /* _a_n_       128+49    */
     NULL_KEY,      /* _a_m_       128+50    */

    NULL_KEY,       /*             128+51    */
    NULL_KEY,       /*             128+52    */
    NULL_KEY,       /*             128+53    */
    NULL_KEY,       /*             128+54    */
    NULL_KEY,       /*             128+55    */
    NULL_KEY,       /*             128+56    */
    NULL_KEY,       /*             128+57    */
    NULL_KEY,       /*             128+58    */

     PAGE_HELP_KEY, /* _f1_             128+59    */
     ACTION_KEY,         /* _f2_             128+60    */ 
     VIEWPATH_KEY,  /* _f3_             128+61    */
     PATH_KEY,      /* _f4_             128+62    */
     GUIDE_KEY,          /* _f5_             128+63    */
     SCAN_KEY,      /* _f6_             128+64    */
     INDEX_KEY,          /* _f7_             128+65    */
     PREVIOUS_MENU_KEY,  /* _f8_             128+66    */
     PRT_KEY,       /* _f9_             128+67    */
     UNDO_KEY,      /* _f10_       128+68    */

    NULL_KEY,       /*             128+69    */
    NULL_KEY,       /*             128+70    */

     HOME_KEY,      /* _home_      128+71    */
     CURSOR_UP_KEY, /* _cursor_up       128+72    */
     BACK_KEY,      /* _page_up_        128+73    */

     NULL_KEY,      /*             128+74    */

     CURSOR_LEFT_KEY,    /* _cursor_left_    128+75    */

     NULL_KEY,      /*                  128+76    */

     CURSOR_RIGHT_KEY,   /* _cursor_right_   128+77    */

     NULL_KEY,      /*                  128+78    */

     END_KEY,       /* _end_            128+79    */
     CURSOR_DOWN_KEY,    /* _cursor_down_    128+80    */
     NEXT_KEY,      /* _page_down_      128+81    */
     INSERT_KEY,         /* _insert_         128+82    */
     DELETE_KEY,         /* _delete_         128+83    */

     NULL_KEY,      /* _s_f1_      128+84    */
     NULL_KEY,      /* _s_f2_      128+85    */
     NULL_KEY,      /* _s_f3_      128+86    */
     NULL_KEY,      /* _s_f4_      128+87    */
     NULL_KEY,      /* _s_f5_      128+88    */
     NULL_KEY,      /* _s_f6_      128+89    */
     NULL_KEY,      /* _s_f7_      128+80    */
     NULL_KEY,      /* _s_f8_      128+91    */
     NULL_KEY,      /* _s_f9_      128+92    */
     NULL_KEY,      /* _s_f10_          128+93    */

     NULL_KEY,      /* _c_f1_      128+94    */
     NULL_KEY,      /* _c_f2_      128+95    */
     NULL_KEY,      /* _c_f3_      128+96    */
     NULL_KEY,      /* _c_f4_      128+97    */
     NULL_KEY,      /* _c_f5_      128+98    */
     NULL_KEY,      /* _c_f6_      128+99    */
     NULL_KEY,      /* _c_f7_      128+100   */
     NULL_KEY,      /* _c_f8_      128+101   */
     NULL_KEY,      /* _c_f9_      128+102   */
     NULL_KEY,      /* _c_f10_          128+103   */

     NULL_KEY,      /* _a_f1_      128+104   */
     NULL_KEY,      /* _a_f2_      128+105   */
     NULL_KEY,      /* _a_f3_      128+106   */
     NULL_KEY,      /* _a_f4_      128+107   */
     NULL_KEY,      /* _a_f5_      128+108   */
     NULL_KEY,      /* _a_f6_      128+109   */
     NULL_KEY,      /* _a_f7_      128+110   */
     NULL_KEY,      /* _a_f8_      128+111   */
     NULL_KEY,      /* _a_f9_      128+112   */
     NULL_KEY,      /* _a_f10_          128+113   */

     NULL_KEY,      /* _c_prtscr_       128+114   */
     NULL_KEY,      /* _c_cursor_left_  128+115   */
     NULL_KEY,      /* _c_cursor_right_ 128+116   */
     NULL_KEY,      /* _c_end_          128+117   */
     NULL_KEY,      /* _c_page_down_    128+118   */
     NULL_KEY,      /* _c_home_         128+119   */

     NULL_KEY,      /* _a_1_       128+120   */
     NULL_KEY,      /* _a_2_       128+121   */
     NULL_KEY,      /* _a_3_       128+122   */
     NULL_KEY,      /* _a_4_       128+123   */
     NULL_KEY,      /* _a_5_       128+124   */
     NULL_KEY,      /* _a_6_       128+125   */
     NULL_KEY,      /* _a_7_       128+126   */
     NULL_KEY,      /* _a_8_       128+127   */
     NULL_KEY,      /* _a_9_       128+128   */
     NULL_KEY,      /* _a_0_       128+129   */
     NULL_KEY,      /* _a_minus_        128+130   */
     NULL_KEY,      /* _a_equals_       128+131   */

     NULL_KEY,      /* _c_page_up_      128+132   */

};
