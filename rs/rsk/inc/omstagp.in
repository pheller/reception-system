/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/* omstagp.in 11/10/87 14:45 */
/*
     file:          omstagp.in
     author:        Michael L. Gordon
     date:          14-May-1987
     facility: Object Stage Facility


     Description:

          Private interface of the object stage facility.

     History:

*/
#ifndef   omstagpDEF
#define omstagpDEF 1

#ifndef SYSunitDEF
#include  <sysunit.in>
#endif

#ifndef OBJUNITDEF
#include  <objunit.in>
#endif
#ifndef objversDEF
#include  <objvers.in>
#endif

/* function OStageNoVersionClassCheck - check the version currency of the
   NoVersion Class and set the one shot version check attribute for all
   objects which are usually not version checked.
*/
extern    void OStageNoVersionClassCheck( OVersionBytest );

/* function OStageControlChangeNotify - notification that the control
   structures of the stage have been changed.
*/
extern    void OStageControlChangeNotify();

/* function OStageCandidacyTest - determine if the object specified by its
   header is a candidate for staging.
   Returns 0 if a stage candidate, else -1.
*/
extern    int  OStageCandidacyTest( ObjectHdr * );

/* restricted function OStageControlSync - set the control integrity tests an
   write the structures
   Returns the status of OStoreSafeSync
*/
int       OStageControlSync( void );

/* restricted function OStageGetHandle - get stage handle
   Only trusted callers should access this function.
*/
extern    StoreHandlet   OStageGetHandle( void );

/* restricted function OStageSetHandle - set stage handle to specified handle
   returns the previous stage handle
   Sets the storectlptr, too.
   Only trusted callers should access this function.
*/
extern    StoreHandlet   OStageSetHandle( StoreHandlet );

/* private function ostagefindentry - find the object in the directory.
   Returns pointer to an entry.  If retcode = 0 then entry is the match,
   if +1 then entry is 1st free one in list, if -1 then list full.
*/
extern    OStageDirEntryt     *ostagefindentry( Objectid *, int * );

/* function OStageForfeitSpace - forfeit stage entries until
   the specified number of allocation units have been freed.
   Returns the number of AUs freed.
*/
extern    Wordt      OStageForfeitSpace( Wordt );

/* function OStageForfeitDirEntry - forfeit stage entries until
   the specified number of entries have been freed.
   Returns the number of entries freed.
*/
extern    int  OStageForfeitDirEntry( int );
#endif
/* end of omstagp.in */
