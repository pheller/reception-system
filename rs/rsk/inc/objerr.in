/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*  objerr.in  3/11/87 */
/*
     file:          objerr.in
     author:        Michael L. Gordon
     date:          2-July-1986
     facility: Object Processor

     Description:

          Public interface of the error unit of the object processor.

     History:

          11 March 1987  Michael L. Gordon
               Add badobjptr error.  Conditional print statements as
               a function of DEBUG.
*/
#ifndef objerrDEF
#define objerrDEF 1

/* define the caller codes */
#define   objunitERR     11        /* same as OM_PROC */
#define   objscanERR     7         /* same as OP3 */

/* define the error codes - same for both objunit and objscan */
#define   objunitENUM_objid   1
#if DEBUG
#define   objunitEMSG_objid   "bad object id"
#endif
#define   objunitENUM_objlen  2
#if DEBUG
#define   objunitEMSG_objlen  "bad object length"
#endif
#define   objunitENUM_objtype 3
#if DEBUG
#define   objunitEMSG_objtype "bad object type"
#endif
#define   objunitENUM_segtype 4
#if DEBUG
#define   objunitEMSG_segtype "bad segment type"
#endif
#define   objunitENUM_getbyte 5
#if DEBUG
#define   objunitEMSG_getbyte "get obj byte"
#endif
#define   objunitENUM_objidxt 6
#if DEBUG
#define   objunitEMSG_objidxt "bad objidxt"
#endif
#define   objunitENUM_delfree 7
#if DEBUG
#define   objunitEMSG_delfree "delete of free object"
#endif
#define   objunitENUM_badseek 8
#if DEBUG
#define   objunitEMSG_badseek "seek off object"
#endif
#define   objunitENUM_malloc  9
#if DEBUG
#define   objunitEMSG_malloc  "malloc error"
#endif
#define   objunitENUM_imbreferr    10
#if DEBUG
#define   objunitEMSG_imbreferr    "imb obj ref err"
#endif
#define   objunitENUM_scanstack    11
#if DEBUG
#define   objunitEMSG_scanstack    "obj scan stack"
#endif
#define objunitENUM_badobjptr 12
#if DEBUG
#define   objunitEMSG_badobjptr    "bad objcxtptr"
#endif
/* definition of feature flags for OpError processing */
#define   OpErrFlag_return         000001         /* return to caller rather
                                                        invoking exception ha */
#define   OpErrFlag_prtobjid       000002         /* display the objid */
#define   OpErrFlag_prtstream      000004         /* display the stream */
#define   OpErrFlag_prtobjcxt      000010         /* dispaly the object direc */
#define   OpErrFlag_prtallstreams  000020         /* display all streams */


/* public function OpErrSetMask - set the feature mask for subsequent
   invocation of OpError.  The mask is inclusive ored with the feature
   flag passed to OpError at invocation time.
   Flags which cannot be set in the feature mask:
     OpErrFlag_prtobjid
     OpErrFlag_prtstream

   The function returns the old mask.
int  OpErrSetMask( mask )
     int            mask;
*/
#if DEBUG
#ifdef LINT_ARGS
extern    int  OpErrSetMask( int );
#else
extern    int  OpErrSetMask();
#endif
#endif

/* public function OpError -  error handler of the objunit.
   Feature flag indicates what gets displayed.
void OpError( feature, callerCode, errorCode, objidptr, stream )
     int            feature;
     Bytet          callerCode, errorCode;
     Objectid  *objidptr;
     Objstream stream;
*/
#ifdef LINT_ARGS
extern    void OpError( int, Bytet, Bytet, );
#else
extern    void OpError();
#endif

#endif

/* end of objerr.in */
