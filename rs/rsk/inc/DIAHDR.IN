/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/****************************************************************************

      FILE:    DIAHDR.IN -- Definition of all DIA headers

  DESCRIPTION:  This file defines DIA(Data Interchange Architecture) structur
          This definition bases on 'TRINTEX DATA INTERCHANGE
          ARCHITECTURE'.(Robert Filepp  ext. 8164 )

-----------------------------------------------------------------------------
 
  DATE        PROGRAMMER            REASON
 8/28/86    C.   Hsieh              Original

*****************************************************************************
/** DIA hdr type **/
#define        FM0  0x00      /* type 0 */
#define        FM0_ 0x80      /* type 0 & concatenated hdr present */ 
#define        FM2  0x02      /* type 2 */ 
#define        FM2_ 0x82      /* type 2 & concatenated hdr present */ 
#define        FM4  0x04      /* type 4 */ 
#define        FM4_ 0x84      /* type 4 & concatenated hdr present */ 
#define        FM9  0x09      /* type 9 */ 
#define        FM9_ 0x89      /* type 9 & concatenated hdr present*/ 
#define        FM64 0x40      /* type 64 */
#define        FM64_     0xC0      /* type 64 & concatenated hdr present */ 

/** DIA hdr length **/
#define        FM0LEN         0x10 /* 16 bytes */
#define        FM2LEN         0x04 /* 4 bytes */
#define        FM9LEN         0x06 /* 6 bytes */
#define        FM64LEN        0x06    /* 6 bytes */

unsigned short  FM4LEN ;      /*FM4 has variable length */

typedef struct diahdr_0            /** DIA hdr type 0 - PRIMARY **/
{                                       
  unsigned char          hdrlen;        /* Header length - byte 0 */ 
  unsigned char          hdrtype;  /* Header type       - byte 1 */ 
  unsigned char          fcode;         /* Function code - byte 2 */ 
  unsigned char     dmode;         /* Data Mode     - byte 3 */
  unsigned char          sid[4];        /* Source ID    - byte 4-7 */
  unsigned char     logseq;        /* Logon seq #   - byte 8 */
  unsigned char          msgid;         /* Mesg Seq #    - byte 9 */
  unsigned char          did[4];        /* Dest. ID      - byte 10-13 */
  unsigned short    txtlen;        /* Text Length   - byte 14,15 */
}DIAFM0;

typedef struct diahdr_2            /** DIA hdr type 2 - BLOCK COUNT **/
{
  unsigned char          hdrlen;        /* Header length - byte 0 */
  unsigned char          hdrtype;  /* Header type   - byte 1 */
  unsigned char          totalblk; /* Total blk #   - byte 2 */
  unsigned char          curblknum;     /* Current blk # - byte 3 */
}DIAMF2;

typedef struct diahdr_4            /** DIA hdr type 4 - GATEWAY **/
{
  unsigned char     hdrlen;        /* Header length       - byte 0 */
  unsigned char     hdrtype;  /* Header type         - byte 1 */
  unsigned char          ttxuid[7];     /* TTX User ID         - byte 2-8 */
  unsigned char          edmode;        /* External Data Mode  - byte 9 */
  union xcid                  /* Correlation ID      - byte 10-18 */
  {
   unsigned char    id2[2];           /* 2 bytes            */
   unsigned char    id4[4];           /* 4 bytes            */
   unsigned char    id6[6];           /* 6 bytes            */
   unsigned char    id8[8];           /* 8 bytes            */
  }corrid;
}DIAFM4;

typedef struct diahdr_9            /** DIA hdr type 9 - VTAM **/
{
  unsigned char          hdrlen;        /* Header length  - byte 0 */
  unsigned char          hdrtype;  /* Header type    - byte 1 */
  unsigned char          fcode;         /* Function code  - byte 2 */
  unsigned char          rcode;         /* Reason code    - byte 3 */
  unsigned char          flag;          /* Flag          - byte 4 */
  unsigned char          txtlen;        /* Text length    - byte 5 */
}DIAFM9;

typedef struct diahdr_64      /** DIA hdr type 64 - STATUS **/
{
  unsigned char     hdrlen;        /* Header length  - byte 0 */
  unsigned char          hdrtype;  /* Header type    - byte 1 */
  unsigned char          status;         /* Status type    - byte 2 */
  unsigned char          dmode;         /* Data Mode      - byte 3 */
  unsigned short    txtlen;        /* text length    - byte 4,5 */
}DIAFM64;

typedef struct xdiastr
{
  char              *diasptr;
  unsigned short    diaslen;
}DIASTR;
