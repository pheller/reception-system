/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/**************************************************
*
*       nferr.c
*
*  Nonfatal error Fatal error messages.
*
*   Author: Algis J.
*   History: 15 July '87 Original Algis
*
**************************************************/

#include <nferr.in>
#include <nonfatal.in>

extern null_rtn();
extern query_state();
/* Define Non-fatal error table. */

ECB nf_err_table[] =
{
"Over flow condition on non fatal error reporting.",null_rtn,
"Missing .CMD object",null_rtn,
"Missing safe page to navigate to.",null_rtn,
"No alternate object id.",query_state,
"Missing alternate object.",query_state
};

query_state()
{

NonFatalState  query_state;

   query_state = QueryNonFatalStatus();
   printf("Non fatal status - ");
   switch(query_state) {

      case(NonFatalStateNoError):
         printf("NonFatalStateNoError\n");
         break;
      case(NonFatalStateMissingFormat):
         printf("NonFatalStateMissingFormat\n");
         break;
      case(NonFatalStateMissingPTO):
         printf("NonFatalStatePTO\n");
         break;
      case(NonFatalStateMissingElement):
         printf("NonFatalStateMissingElement\n");
         break;
      case(NonFatalStateMissingWindow):
         printf("NonFatalStateMissingWindow\n");
         break;
      case(NonFatalStateMissingPGM):
         printf("NonFatalStatePGM\n");
         break;
      case(NonFatalStateMissingPDO):
         printf("NonFatalStatePDO\n");
         break;
      case(NonFatalStateUndefinedObjType):
         printf("NonFatalStateUndefinedType\n");
         break;
      default:
         printf("Unknown type\n");

   }
}
