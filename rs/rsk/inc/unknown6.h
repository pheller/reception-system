/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
extern  Objstream   Obj_ref_get();
extern  int     Obj_ref_kill();
extern  int Get_object();
extern  int Get_obj_byte();
extern  int Obj_seek();
extern  int Obj_get_offset();
extern  int Obj_deaccess();
extern  int Obj_rundown();
extern  int Obj_refto_id();
extern  int Obj_fetch();
extern  int Obj_kill();
