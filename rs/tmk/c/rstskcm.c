/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/*********************************************************************/
/*                                          */
/*                 T R I N T E X  C O N F I D E N T I A L            */
/*                                          */
/*********************************************************************/
/* CHANGE HISTORY -                              */
/*   DATE    LEV       TITLE            AUTHOR              */
/* 09/10/86  000     RSTASKCM.C              L. WHEELER          */
/*********************************************************************/
/*                                          */
/*    DESCRIPTION                               */
/*        This module is a collection of subroutines       */
/*        providing o/s independent access to intertask       */
/*        communication functions.              */
/*                                          */
/*       GLOBAL ENTRY POINTS                          */
/*          enq_task - send ICB (intertask communication block)  */
/*                    to another task                 */
/*                                          */
/*          deq_task - get next ICB off my queue           */
/*                                          */
/*    EXTERNAL SUBROUTINES CALLED                     */
/*        whotask - TMK fetch task id                 */
/*        qsvcatn - TMK (non-blocking) attention request        */
/*        dispret - TMK return to dispatcher          */
/*        getreq  - TMK examine request on request queue      */
/*        compreq - TMK complete a request            */
/*        disptask - TMK dispatch a task                   */
/*                                          */
/*********************************************************************/
#include <iwssvcsc.h>
#include <iwssvccc.h>
#include <rsystem.h>
#include <rserror.h>
#include <stdio.h>
#include <dos.h>

/*********************************************************************/
/* FUNCTION                                                     */
/*   enq_task - put an ICB on another task's queue              */
/*                                          */
/* INPUT                                    */
/*   task - id of recipient task                      */
/*   priority - placement of msg on task's work queue:     */
/*        Q_TOP - place at top of queue (for hi priority)       */
/*        Q_BOTTOM - place at bottom of queue              */
/*   icb - pointer to icb to be placed on the queue             */
/*                                          */
/* OUTPUT                                   */
/*                                          */
/* RETURNS                                       */
/*   error - return code                         */
/*                                          */
/* ERROR CODES                                        */
/*   E_NONE - operation successful                    */
/*   E_TSKID - invalid id for recipient task               */
/*   E_PRIORITY - invalid queueing priority                */
/*   E_BADICB - null or invalid icb pointer or invalid icb      */
/*   E_MSG_TYPE - invalid request type                */
/*                                          */
/* SYNOPSIS -                                    */
/*   #include <rsystem.h>                             */
/*   #include <rserror.h>                             */
/*   int enq_task (task, priority, icb)               */
/*   int task;                                   */
/*   int priority;                               */
/*   ICB far *icb;                               */
/*********************************************************************/

int enq_task (task, priority, icb)
int task; /* task id of icb recipient */
int priority; /* put icb at top or bottom of queue */
ICB far *icb; /* ptr to icb to be enqueued */
{
     struct svcplst params; /* TMK parameter list structure */
     struct grqptrs grq; /* TMK get request structure */
     ICB far *icbp; /* ptr to icb for sync send getreq */
     int new_task; /* task id of icb sender for sync send */
     int error; /* return code */

     if ((task < MIN_TASK) || (task > MAX_TASK))
          error = E_TSKID; /* invalid task id */
     else if ((priority != Q_TOP) && (priority != Q_BOTTOM))
          error = E_PRIORITY; /* invalid queuing priority */
     else if (icb == (ICB far *)NULL)
          error = E_BADICB; /* invalid icb ptr or icb */
     else {

          /* set sender id in icb */
          icb -> itask = whotask();
          /* turn far pointer into segment and offset */
          parms.sp_args[0] = FP_OFF(icb);
          parms.sp_args[1] = FP_SEG(icb);
          error = E_NONE; /* assume success for now */
          switch (icb -> itype) {
               case S_RES:    /* sync response */
                    priority = Q_TOP;
               case A_REQ:    /* async request */
               case A_RES:    /* async response */
                    qsvcatn (task, &parms, priority);
                    break;
               case S_REQ:    /* sync request */

                    qsvcatn (task, &parms, priority);
                    for (;;) {
                    dispret (_QUSET); /* suspend */
                    /* when redispatched check queue */
                    /* if something on it, check from who */
                    new_task = getreq (0, &grq, &parms);
                    if (new_task == task) {
                         /*
                            if from task we sent to, check
                            whether it's a request ro response;
                            piece together segment and offset
                         */
                         FP_OFF(icbp) = parms.sp_args[0];
                         FP_SEG(icbp) = parms.sp_args[1];
                         if (icbp -> itype == S_RES)
                              break;
                         /* loop until response is received */
                    }
                    }
                    break;
               default:
                    error = E_MSG_TYPE;
          }
     }
     return (error);
}
/*********************************************************************/
/* FUNCTION                                                      */
/*   deq_task - get next ICB off my queue                   */
/*                                          */
/* INPUT                                    */
/*                                          */
/* OUTPUT                                   */
/*                                          */
/* RETURNS                                       */
/*   pointer to ICB or NULL if nothing is on the queue      */
/*                                          */
/* ERROR CODES                                   */
/*                                          */
/* SYNOPSIS                                      */
/*   #include <rsystem.h>                             */
/*   ICB far *deq_task();                             */
/*********************************************************************/

ICB far *deq_task()
{
     ICB far *icbp;      /* temporary icb ptr */
     struct svcplst parms;    /* TMK parameter list structure */
     struct grqptrs grq; /* TMK get request structure */

     grq.tag = 0; /* init tag prior to getreq */
     /* get first item off work queue */
     task = getreq (0, &grq, &parms);
     if ((task == 0) && (grq.tag == 0))
          icbp = (ICB far *)NULL;
     else {
          /* complete request so TMK can free rqe */
          compreq (grq.tag);
          /* piece together ICB ptr's segment and offset */
          FP_OFF(icbp) = parms.sp_args[0];
          FP_SEG(icbp) = parms.sp_args[1];
     }
     return (icbp);
}
