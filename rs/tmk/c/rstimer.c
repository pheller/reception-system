/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/*********************************************************************/
/*                            */
/*                 T R I N T E X  C O N F I D E N T I A L            */
/*                            */
/*********************************************************************/
/* CHANGE HISTORY -                      */
/*   DATE    LEV       TITLE         AUTHOR          */
/* 06/27/86  000      RSTIMER.C                 L. WHEELER        */
/*********************************************************************/
/*                            */
/*     DESCRIPTION                      */
/*      This module contains the system timer task and      */
/*      a collection of subroutines providing o/s        */
/*      independent access to intertask timer functions.    */
/*                            */
/*       GLOBAL ENTRY POINTS                   */
/*      timer_task                               */
/*      create_timer - request timer from logical o/s        */
/*      set_timer - start a timer running          */
/*      clear_timer - stop a timer             */
/*      delete_timer - return timer to logical o/s          */
/*      check_timer - see what current value of timer is    */
/*                            */
/*    GLOBALLY ACCESSIBLE DATA STRUCTURES             */
/*      sleep_list - sleep intervals (one per task)       */
/*                            */
/*    EXTERNAL SUBROUTINES CALLED                */
/*      claimsem - TMK claim semaphore             */
/*      relsem   - TMK release semaphore             */
/*      deftimr - TMK define timer               */
/*      settimr - TMK set timer                     */
/*      dispret - TMK return to dispatcher          */
/*      disptask - TMK dispatch task                    */
/*                            */                      
/*    EXTERNAL DATA STRUCTURES REFERENCED             */
/*                            */
/*    COMMENTS                      */
/*      Note that timer_task runs in a different context    */
/*      than the other routines in this module.  They run   */
/*      as subroutines in the context of tasks such as the  */
/*      Communications or Service Mgrs, for example.  Hence */
/*      semaphore protection of shared data structures.       */
 
/*
   Rewritten 4/28/87 by LH for greater clarity.
*/
/*********************************************************************/
#include <stdio.h>
#include <iwssvcsc.h>
#include <iwssvccc.h>
#include <rsystem.h>
#include <rstimer.h>
#include <rserror.h>
#define LINT_ARGS 1                        /* enable stricter type checking */

/*global*/  int timer_task(void);
/*global*/  int create_timer(void);
/*global*/  int set_timer(int ,int ,int far *,int );
/*global*/  int delete_timer(int );
/*global*/  int clear_timer(int );
/*global*/  int check_timer(int );

/* make sleep list global so it's accessible from sleep() */

int sleep_list[NUM_TASKS];                    /* sleep times - one per task */

/* declare timer control blocks but don't make global */

static TCB timers[NUM_TIMERS];                      /* timer control blocks */
/********************************************************************
FUNCTION
timer_task - task which services logical o/s timers

Rewritten 4/28/87 by LH for greater clarity.

********************************************************************/

int
timer_task ()
{
   register int i;

   /*
   Initialize timers to zero.
   */
   for (i = 0; i < NUM_TIMERS; i++)
      timers[i].tinuse = timers[i].tvalue = timers[i].ttask = 0;
   /*
   Clear sleep list.
   */
   for (i = 0; i < NUM_TASKS; i++)
      sleep_list[i] = 0;
   deftimr(TIMR_ID, SLEEP_TIME, 0, 0);                      /* create timer */
   settimr(TIMR_ID, SLEEP_TIME);                             /* kick it off */
   dispret(_QUSET);                                          /* go to sleep */
   for (;;)                           /* upon wakeup go into main task loop */
   {
      /*
      Claim sleep semaphore while looking at globals.
      */
      claimsem(SLEEP_SEM);
      for (i = 0; i < NUM_TASKS; i++)                    /* scan sleep list */
         if (sleep_list[i] && !--sleep_list[i])
            disptask (i + USER_TSK_BASE);              /* the sleeper wakes */
      relsem(SLEEP_SEM);                               /* release semaphore */
      /*
      Claim timer semaphore while looking at timers.
      */
      claimsem(TIMER_SEM);
      /*
      Look for an expired timer -- nonzero means not expired.
      */
      for (i = 0; i < NUM_TIMERS; i++)
         if (timers[i].tinuse && timers[i].tvalue && !--timers[i].tvalue)
         {
            if (timers[i].tflag)           /* if there's a flag to clear... */
               *timers[i].tflag = 0;                               /* do so */
            if (timers[i].ttask)         /* if there's a task to wake up... */
               disptask(timers[i].ttask);
         }
      relsem (TIMER_SEM);
      dispret (_QUSET);                                 /* go back to sleep */

   }
}
/********************************************************************
FUNCTION
create_timer - request timer resource from logical o/s

RETURNS
error code or timer id for future reference

ERROR CODES
E_NO_TIMER - no timers are available at this time

SYNOPSIS
#include <rserror.h>
int create_timer ();
********************************************************************/

int
create_timer()
{
   register int i;                                        /* scratch index */

   for (i = 0; i < NUM_TIMERS; i++)
      if (!timers[i].tinuse)                              /* timer in use? */
      {
         timers[i++].tinuse = 1;                      /* set "in use" flag */
         return i;             /* return timer ID, which is indexed from 1 */
      }
   return E_NO_TIMER;                                 /* ran out of timers */
}
/********************************************************************
FUNCTION
set_timer - start timer running

INPUT
interval - number of 100 msec units to count down
flag - pointer to flag to clear when timer expires
task - id of task to notify when timer expires

OUTPUT

RETURNS
error - result of operation

ERROR CODES
E_NONE - operation successful
e_TIMRID - timer identifier is invalid
E_TIM_NOT_CREATED - timer access has not been requested yet
E_TIME - interval is invalid
E_TSKID - task identifier is invalid

SYNOPSIS
#include <rserror.h>
int set_timer (timer_id, interval, flag, task)
int timer_id;
int interval;
int flag;
int task;

4/28/87 -- rewritten for clarity by LH.

********************************************************************/

int
set_timer (timer_id, interval, flag, task)
int timer_id;                                /* id returned by timer_create */
int interval;                       /* number of 100 msec units til expires */
int far *flag;                     /* user flag to clear when timer expires */
int task;                              /* id of task to notify when expires */
{
   /*
   Timer id's are indexed from 1, array of timers from 0.
   */
   if (--timer_id < TIMER_MIN || timer_id > TIMER_MAX)
      return E_TIMRID;                                  /* invalid timer id */
   if (!timers[timer_id].tinuse)
      return E_TIMR_NOT_CREATED;                     /* the timer is in use */
   if (interval < 0)
      return E_TIME;                              /* invalid timer interval */
   if (task && task < MIN_TASK || task > MAX_TASK)
      return E_TSKID;                                    /* invalid task id */
   claimsem (TIMER_SEM);    /* claim timer semaphore while updating globals */
   timers[timer_id].tvalue = interval;
   timers[timer_id].ttask = task;
   timers[timer_id].tflag = flag;
   relsem (TIMER_SEM);                                 /* release when done */
   return E_NONE;                                            /* good return */
}
/********************************************************************
FUNCTION
delete_timer -

OUTPUT
id of timer for future reference or error code

RETURNS
error code

ERROR CODES
E_TIMRID - invalid timer id

SYNOPSIS
#include <rserror.h>
int delete_timer (timer_id);
int timer_id;


4/28/87 -- rewritten for clarity by LH.

********************************************************************/
int
delete_timer(timer_id)
int timer_id;                                /* id returned by timer_create */
{
   /*
   Timer id's are indexed from 1, array of timers from 0.
   */
   if (--timer_id < TIMER_MIN || timer_id > TIMER_MAX)
      return E_TIMRID;                                  /* invalid timer id */
   claimsem (TIMER_SEM);    /* claim timer semaphore while updating globals */
   timers[timer_id].tinuse = 0;                        /* clear timer block */
   timers[timer_id].tvalue = 0;
   timers[timer_id].ttask = 0;
   timers[timer_id].tflag = 0;
   relsem (TIMER_SEM);                                 /* release when done */
   return E_NONE;                                            /* good return */
}
/********************************************************************
FUNCTION
clear_timer - set timer interval to 0

INPUT
timer_id - id of timer to clear

OUTPUT

RETURNS
error - result of operation

ERROR CODES
E_NONE - operation successful
E_TIMRID - invalid timer id

SYNOPSIS
#include <rserror.h>
int clear_timer (timer_id);
int timer_id;


4/28/87 -- rewritten for clarity by LH.

********************************************************************/

int
clear_timer (timer_id)
int timer_id;                                                   /* timer id */
{
   /*
   Timer id's are indexed from 1, array of timers from 0.
   */
   if (--timer_id < TIMER_MIN || timer_id > TIMER_MAX)
      return E_TIMRID;                                  /* invalid timer id */
   claimsem (TIMER_SEM);     /* claim timer semaphore while updating global */
   timers[timer_id].tvalue = 0;                                 /* clear it */
   relsem (TIMER_SEM);                                 /* release when done */
   return E_NONE;                                            /* good return */
}
/********************************************************************
FUNCTION
check_timer - get current value of timer

INPUT
timer_id - id of timer to check

OUTPUT

RETURNS
interval or error code

ERROR CODES
E_TIMRID - invalid timer id

SYNOPSIS
#include <rserror.h>
int check_timer (timer_id);
int timer_id;

4/28/87 -- rewritten for clarity by LH.

********************************************************************/
int
check_timer (timer_id)
int timer_id;                                                   /* timer id */
{
   /*
   Timer id's are indexed from 1, array of timers from 0.
   */
   if (--timer_id < TIMER_MIN || timer_id > TIMER_MAX ||
     !timers[timer_id].tinuse)
      return E_TIMRID;                   /* timer invalid or not yet in use */
   return timers[timer_id].tvalue;                /* return ticks remaining */
}
