/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWAQUERS - interface to task communication queueing services     */
/*                                          */
/*    DESCRIPTION - provides C interface to the following       */
/*      queuing services:                             */
/*         qsvcatn: send message, no reply returned        */
/*                                          */
/*    HOW INVOKED -                              */
/*        qsvcatn:  qsvcatn(targ,plst_addr,priority)       */
/*                                          */
/*    INPUTS -                              */
/*        PARAMETERS:                            */
/*          targ:     target task id (= SFV of the task)        */
/*          plst_addr: address of parm list in caller's space   */
/*               that contains up to 14 bytes of data to   */
/*               be passed to the target.  The target      */
/*               must know the form.  Parm list should be  */
/*               a structure of type SVCPLST.              */
/*          priority:  priority of request.  Zero = end of     */
/*               que.    Other numbers (1-7fff hex) are        */
/*               higher priority and affect que ordering   */
/*    SUBROUTINES -                              */
/*        EXTERNAL:                              */
/*        issuesvc:executes the svc                   */
/*                                          */
/********************************************************************/
#include <iwssvcsc.h>                    /* svc structure maps      */
#include <iwssvccc.h>                    /* svc constants           */

extern issuesvc();

/**********************************************************************/
/*  Qsvcatn - send message, no reply returned                     */
/**********************************************************************/
qsvcatn(targ,parms,prior)
int targ;
struct svcplst *parms;
int prior;
{
struct svcregs svcregs;       /* parms to svc        */

  svcregs.sr_indx = targ;          /* ax = SFV index       */
  svcregs.sr_type = _QSVCATN;      /* bx = SVC type        */
  svcregs.sr_spec = prior;         /* cx = priority        */
  issuesvc(parms,&svcregs);
 }
