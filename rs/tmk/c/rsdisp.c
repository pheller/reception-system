/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/*                 T R I N T E X  C O N F I D E N T I A L            */
/*                                          */
/*********************************************************************/
/* CHANGE HISTORY -                              */
/*   DATE    LEV       TITLE            AUTHOR              */
/* 06/27/86  000      RSDISP.C               L. WHEELER          */
/*********************************************************************/
/*                                          */
/*    DESCRIPTION                                */
/*        This module is a collection of subroutines        */
/*        providing o/s independent access to task dispatch    */
/*        functions.                             */
/*                                          */
/*       GLOBAL ENTRY POINTS                          */
/*             resume  - make a particular task dispatchable      */
/*             preempt - return control to dispatcher            */
/*        sleep   - give up control for a set time interval    */
/*        suspend - give up control until redispatched           */
/*                                          */
/*    GLOBALLY ACCESSIBLE DATA STRUCTURES                   */
/*                                          */
/*    EXTERNAL SUBROUTINES CALLED                     */
/*        dispret - TMK return to dispatcher          */
/*        disptask - TMK make task dispatchable             */
/*        claimsem - TMK claim a semaphore            */
/*        relsem - TMK release a semaphore            */
/*        whotask - TMK fetch task id                 */
/*                                          */
/*    EXTERNAL DATA STRUCTURES REFERENCED                   */
/*        sleep_list - declared in rstimer.c module         */
/*                                          */
/*    COMMENTS                              */
/*********************************************************************/
#include <iwssvccc.h>
#include <rsystem.h>
#include <rstimer.h>
#include <rserror.h>
/*********************************************************************/
/* FUNCTION                                                      */
/*   resume - make a task dispatchable                */
/*                                          */
/* INPUT                                    */
/*   task - id of target task                    */
/*                                          */
/* RETURNS                                       */
/*   error - return code                         */
/*                                          */
/* ERROR CODES                                        */
/*   E_NONE - operation successful                    */
/*   E_TSKID - invalid id for target task                   */
/*                                          */
/* SYNOPSIS                                      */
/*   #include <rserror.h>                             */
/*   int resume (task);                          */
/*   int task;                                   */
/*********************************************************************/

int resume (task)
int task; /* target task identifier */
{
     int error; /* return code */

     if ((task < MIN_TASK) || (task > MAX_TASK))
          error = E_TSKID;    /* invalid task id */
     else {
          error = E_NONE;
          disptask (task); /* issue TMK call */
     }
     return (error);
}
/*********************************************************************/
/* FUNCTION                                                      */
/*   preempt - give up control for one dispatch cycle       */
/*                                          */
/* SYNOPSIS                                      */
/*   int preempt ();                                  */
/*********************************************************************/

int preempt ()
{
     /* return to dispatcher but remain dispatchable */

     dispret (_QDISP); /* issue TMK call */
     return;
}
/*********************************************************************/
/* FUNCTION                                                      */
/*   sleep - give up control for a particular time interval      */
/*                                          */
/* INPUT                                    */
/*   interval - number of 100 msec units to sleep           */
/*                                          */
/* RETURNS                                       */
/*   error - return code                         */
/*                                          */
/* ERROR CODES                                   */
/*   E_NONE - operation successful                    */
/*   E_TIME - invalid sleep interval                        */
/*                                          */
/* SYNOPSIS                                      */
/*   #include <rserror.h>                             */
/*   int sleep (interval);                            */
/*   int interval;                               */
/*********************************************************************/

int sleep (interval)
     int interval;  /* number of 100 msec units to sleep */
{
     extern sleep_list[]; /* sleep list in rstimer.c module */
     int error;     /* return code */
     int i; /* scratch sleep list index */

     if (interval < 0)
          error = E_TIME;
     else {
          i = whotask() - USER_TSK_BASE; /* sleep list index */
          claimsem (SLEEP_SEM); /* claim while updating global */
          sleep_list[i] = interval;
          relsem (SLEEP_SEM); /* release after update */
          dispret (_QUSET); /* sleep until time is up */
          while (sleep_list[i]) dispret (_QUSET);
          error = E_NONE;
     }
     return (error);
}
/*********************************************************************/
/* FUNCTION                                                      */
/*   suspend - give up control until redispatched           */
/*                                          */
/* SYNOPSIS                                      */
/*   int suspend ();                                  */
/*********************************************************************/

int suspend ()
{
     /* return to dispatcher letting him decide when to redispatch */

     dispret (_QUSET); /* issue TMK call */
     return;
}
