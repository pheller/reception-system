/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWATIMR  - timer support functions
/*                                          */
/*    DESCRIPTION - this module is a collection of subroutines to*/
/*        provide access to various timer services. The         */
/*        routines are:                          */
/*          deftimr - define (init) a timer                */
/*          settimr - set the timer and start it running        */
/*          resettimr- reset timer to zero count           */
/*                                          */
/*    SUBROUTINES -                              */
/*        INTERNAL: some of the routines call each other        */
/*        EXTERNAL:                              */
/*        issuesvc: executes the svc for the semaphore service*/
/*        dispret: returns task to dispatcher to wait      */
/*                                          */
/*    COMMENTS -                                 */
/*        1. Invalid timer requests abend the multitasker.      */
/*        2. When a task waits for a timer, it is marked as     */
/*        "nondispatchable". Any signal from another task that*/
/*        sets the task dispatchable again may override the   */
/*        timer. The task should check the flag word defined  */
/*        on the creatimr to see if the timer ran out or        */
/*        something else happened. If the timer did not expire*/
/*        it should be reset.                    */
/*        3. Note that a timer may automatically restart if     */
/*        requested on creatimr. That defines a periodic timer*/
/*        that does not need resets.                  */
/*        4. Time delays are in the unit tick supported by the   */
/*        multitasker. Specify 291 ticks per second.       */
/*        5. The sr_spec parameter on some timer functions      */
/*        represents a priority. We always use priority = 0.  */
/********************************************************************/
#include <iwssvccc.h>         /* svc request constants */
#include <iwssvcsc.h>         /* svc request structures */
static struct sfvplst *nullptr = 0;   /* null pointer  */
extern issuesvc();

/********************************************************************/
/* deftimr - claim a timer with the specified id. The input    */
/*     arguments are:                                 */
/*     timerid  - which system time to use (predefined)         */
/*     resetime - time delay to reset timer to each time it     */
/*             expires. Nonzero makes a periodic timer. Zero  */
/*             means timer must be specifically restarted.    */
/*     popofst/popseg - address (offset,segment) of a variable to*/
/*             be set to the timer number each time this timer*/
/*             expires. Caller must clear the variable. If    */
/*             the address is zero, no variable is set. The   */
/*             task waking up again must suffice in that case.*/
/*                                          */
/*     When a timer expires, it automatically makes the owning task */
/*     dispatchable again. This routine does not support the option */
/*     of suppressing the state change.                    */
/*                                          */
/********************************************************************/
void deftimr(timerid,resetime,popofst,popseg)
int timerid,resetime,popofst,popseg;
{
  struct svcregs svcregs;          /* register passing to svc  */
  struct svcplst plst;

  plst.sp_args[0] = resetime;      /* a restart value    */
  plst.sp_args[1] = popofst;
  plst.sp_args[2] = popseg;
  svcregs.sr_indx = timerid;       /* this timer          */
  svcregs.sr_type = _QSVCTIMMK;    /* make it             */
  svcregs.sr_spec = 0;
  issuesvc(&plst, &svcregs);
 }

/********************************************************************/
/* settimr  - set the specified timer to the given value in ticks   */
/*     and start the timer running. The caller is not suspended.    */
/*     The routine returns nothing.                        */
/*                                          */
/********************************************************************/
void settimr(timerid,value)
int timerid;
int value;           /* timer duration in ticks */
{
  struct svcregs svcregs;          /* register passing to svc    */
  svcregs.sr_indx = timerid;       /* the timer to set    */
  svcregs.sr_type = _QSVCSETTIM;   /* request type = settime  */
  svcregs.sr_spec = value;
  issuesvc(nullptr, &svcregs);
}

/********************************************************************/
/* resettimr - set the specified timer's count to zero. This either */
/*     shuts the timer off if it has no automatic restart, or resets*/
/*     the timer to its initial count.                     */
/*                                          */
/********************************************************************/
void resettimr(timerid)
int timerid;
{
  settimr(timerid,0);               /* set count to zero       */
}
