/* VERSION = 6.1.2    LAST_UPDATE = 6.0.5    */
/********************************************************************/
/*                                          */
/* RSYSTEM  - "C" structure definitions and equates for Logical O/S */
/*                                          */
/*    DESCRIPTION - define structure layouts (no actual creation */
/*        of data) and constants for use in the C version of     */
/*        the LOS.                                    */
/*                                          */
/*    HOW INVOKED - via "#include <rsystem.h>"                */
/*                                          */
/********************************************************************/
/* define semaphore id numbers */

#define TIMER_SEM   17
#define SLEEP_SEM   18
#define MEM_SEM          19

/* define bounds of task numbers; lower ones are reserved */

#define   MIN_TASK  6
#define USER_TSK_BASE    MIN_TASK
#define   MAX_TASK  16 /* Changed form 12 to 13, 4/8/87, LH */
                           /* changed from 13 to 16 for ESP Algis
                     6 Jan '88 */
#define NUM_TASKS   MAX_TASK - MIN_TASK + 1 /* Algis 6 Jan '88 */

/* define format of header of Intertask Communication Block (ICB) */

typedef struct icb_block {
     unsigned char itask;     /* task id of sender */
     unsigned char itype;     /* type -- async req/resp or sync req/resp */
     unsigned char ifunc;     /* function to be performed by recipient */
     unsigned char istatus;   /* status of function performed */
} ICB;

/* define the two icb queuing priorities */

#define   Q_BOTTOM  0
#define   Q_TOP          1

/* define ways of sending icbs: sync/async and req/resp */

#define A_REQ       1
#define A_RES       2
#define S_REQ       3
#define S_RES       4
/********************************************************************/
/*   END RSYSTEM                                 */
/********************************************************************/
/* VERSION = ##.##.## LAST_UPDATE = ##.##.## */
#define VERSION_NO  '1','.','0','0','.','0','0'
