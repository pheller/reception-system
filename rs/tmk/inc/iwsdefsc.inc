;/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
;/*******************************************************************/
;*                                          */
;* IWSDEFSC - assembler version of iwsdefsc.h (partial)         */
;*                                          */
;*    DESCRIPTION - masm include file to match C iwsdefsc.h.     */
;*        See that file for details.                  */
;*                                          */
;*    HOW INVOKED - via "include iwsdefsc.inc"                   */
;*                                          */
;********************************************************************/
;
;DQE constants
;
QNODISP  equ 0
QDISP      equ 1
QWAIT      equ 2
QUSET      equ 3
QSEMWAIT equ 4
;
QSUSPND  equ 0
QNOSUSP  equ 1
;
;DQE layout
;
zdqedisp equ 0
zdqeprio equ 2
zdqeflag equ 3
zdqestak equ 4
zrqhfrt  equ 8
zrqhlst  equ 10
zcqhfrt  equ 12
zcqhlst  equ 14
zdqesfv  equ 16
zdqeact  equ 18
zdqenext equ 20
;
; RQE layout
;
zrqerqst equ 0
zrqedqep equ 2
zrqenxtp equ 4
zrqeprty equ 6
zrqeplst equ 8
;
;SFV layout
;
zsfvtag  equ 0
zsfvepa  equ 2
zsfvdqea equ 2
zsfvmant equ 4
;
; old constants defining where in system an event occurred
;
qsvcindx  equ 103h
;
;* abend codes */
ILLEGSFV  equ 1
SVCNORQE  equ 2
WRMANOP   equ 3
WRSEMOP   equ 4
NOTSEMWAIT equ 5
ILLEGSVC  equ 6
DUPLINAME equ 7
NOSFV       equ 8
NODQE       equ 9
ILLEGTIM  equ 10
NOTOWNTIM equ 11
SWAPCRAPS equ 12
MANHASQ   equ 13
SEMREMERR equ 14
FCBNOTFCB equ 15
ILLEGREM  equ 16
MANREMERR equ 17
BRICK1      equ 18
BADCMPRQ  equ 19
;
;* SVC codes */
;
_QSVCATN  equ   2      ; /*  attention       */
_QSVCIATN equ   3      ; /*  IH attention         */
_QSVCCMPRQ equ  4      ; /*  complete request     */
_QSVCGETRQ equ  7      ; /*  get request          */
_QSVCCLAIM equ  8      ; /*  claim semaphore */
_QSVCREL   equ  9      ; /*  release semaphore    */
_QSVCDSPT  equ  10     ; /*  set dispatchable     */
_QSVCSETTIM equ  11    ; /*  set timer count */
_QSVCTIMMK  equ  12    ; /*  create a timer  */
_QSVCSHUT  equ  14     ; /*  shutdown        */
_QSVCCURSFV equ  15    ; /*  get current SFV index     */
_QSVCSEMMK  equ  17    ; /*  create semaphore     */
_QSVCMGRMK  equ  18    ; /*  create task     */

  
  
  
  
  
  
  
  
  
  
  
  
  
