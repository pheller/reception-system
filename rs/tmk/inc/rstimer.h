/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* RSTIMER  - "C" structure definitions and equates for LOS timer   */
/*                                          */
/*    DESCRIPTION - define structure layouts (no actual creation */
/*        of data) and constants for use in the C version of     */
/*        the LOS.                               */
/*                                          */
/*    HOW INVOKED - via "#include <rstimer.h>"                */
/********************************************************************/
#define   TIMER_MIN 0
#define NUM_TIMERS  10
#define TIMER_MAX   (NUM_TIMERS - TIMER_MIN - 1)

#define TIMR_ID          5    /* Timer task's timer */
#define SLEEP_TIME  29   /* number of units TMK timer waits */
                    /* (equal to about 100 msecs) */

typedef struct tcb_block {
     int tinuse;    /* 'in use' flag */
     int far *tflag;     /* user flag to clear upon expiration */
     int ttask;     /* id of task to notify upon expiration */
     int tvalue;    /* number of 100 msec units to count down */
} TCB;
/********************************************************************/
/*   END RSTIMER                                 */
/********************************************************************/
