/* CM      = 1.1.2    LAST_UPDATE = 1.0.1    */
/* Link level queues and constants                     */

#define MAXPKT 256            /* rollover packet number     */
#define RPQWDW 8              /* size of rcve protocol window */
#define RPQ_SIZE 16           /* size of rcve packet queue    */
#define TPQWDW 2              /* size of xmit protocol window */
#define   SKIP -2             /* skip code             */
#define TWOACK -3             /* last acked - first unacked */
#define   PKPRBK    4              /* Max packets/block      */
#define   BLK1ST    0              /* intertask code for 1st pkt */
#define   BLK2ND    1              /* intertsk code for not 1st pkt*/
#define XCARE  0              /* don't care status          */
#define   SHIFTY    1              /* shift queue indication */
#define   SHIFTN    0              /* do not shift queue     */
#define FOUND  1              /* found in queue        */
#define NFOUND 0              /* not found in queue         */
#define   FORWD     0              /* forward               */
#define   LINK 1              /* link packets               */


                         /* packet protocol statuses   */
#define   CLEAR     0              /* queue element empty    */
#define   POK  1              /* packet received correctly  */
#define   ACK  2              /* ack this packet       */
#define NACK   3              /* nak this packet       */
#define   FWDD 4              /* packet has been forwarded  */
#define TOTX   5              /* packet to be transmitted   */
#define WACK   6              /* wait-acknowledged status   */
#define JNACK  7
#define NACK_CRC 8

                         /* class of received packet   */
#define   DATA 0              /* Data packet           */
#define   RTT  1              /* protocol pkt for xmit side */
#define   RRT  2              /* protocol pkt for rcve side */

#define NO_RESET 0            /* LA 03/29/88           */
#define RESET    1

                         /* receive protocol queue     */
struct rpqe{
          unsigned char pkt_no;    /* data packet number     */
          unsigned char pkt_status;     /* data packet protocol status */
          struct pkt *pkt_addr;    /* address of data packet */
          struct rpqe *nxt;   /* next queue element         */
          struct rpqe *prev;  /* previous queue element     */
};

                         /* transmit protocol queue    */
struct pqe{
          int ix;             /* queue element indicator    */
          unsigned char pkt_no;    /* data packet number     */
          unsigned char pkt_status;     /* data packet protocol status */
          struct pkt *pkt_addr;    /* address of data packet */
          struct pqe *nxt;    /* link to next queue element */
};
