/* CM      = 1.1.2    LAST_UPDATE = 1.0.0    */
/* sem.h - Semaphores and flags defines                     */

#define NSEM   3              /* number of sems        */
#define NTSK   4              /* max no of tstks/sem        */

                         /* Semaphores Statuses        */              
#define FREE   0              /* semaphore available        */
#define BUSY   1              /* semaphore not available    */

                         /* Semaphores            */
#define   txsem     0              /* transmitter semaphore */
#define ltsem   1             /* local transport sem   */
#define cmsem  2              /* comm subsys sem       */

                         /* semaphores queue element   */
struct smq{
     unsigned short id;       /* semaphore id               */
     unsigned short stat;          /* semaphore status      */
};
