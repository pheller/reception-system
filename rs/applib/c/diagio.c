/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/* diagio.c 9/19/87 12:25 */
/*
     file:          diagio.c
     author:        Michael L. Gordon
     date:          18-Sep-1987
     facility: Dignostic IO Facility

     Description:

          Implementation of the diagnostic IO unit.

     History:

*/
#define LINT_ARGS 1

#include  <fcntl.h>
#include  <sys\types.h>
#include  <sys\stat.h>
#include  <io.h>
#include  <string.h>
#include  <diagio.in>

#ifndef begin
#define   begin {
#define   end }
#define then
#endif

#define   diagpermchannelmax  4

/* DiagClass context descriptor type */
     typedef struct begin
          int       channel;
     end diagclasscxtt;

/* Definition of the diag class descriptor instance */
     static    diagclasscxtt  diagcxt[ numDiagClasst ] = begin
          /* printer class */      4
     end;

/* function DiagIOreOpen - The specified diagClass is associated with the
     specified file or device for subsequent operations.
   Returns 0 or OS code.
*/
int  DiagIOreOpen( diagclass, filespec )
     DiagClasst     diagclass;
     char      *filespec;
begin
     extern    int       errno;

     /* First close the channel unless a linked reference */

     if ( diagcxt[ (int) diagclass ].channel > diagpermchannelmax ||
      diagclass == DiagClassPrinter ) then begin
          close( diagcxt[ (int) diagclass ].channel );
     end;
     /* Now open the channel */
     if ( ( diagcxt[ (int) diagclass ].channel = open( filespec, O_CREAT |
      O_RDWR | O_TRUNC | O_TEXT, S_IREAD | S_IWRITE ) ) == -1 ) then begin
          /* error opening the file */
          return( errno );
     end else return( 0 );
end /* DiagIOreOpen */

/* function DiagIOputBuf - put a buffer of characters to the diagClass file,
   with the length in bytes specified by the caller.
   Returns 0 or OS error code.
*/
int  DiagIOputBuf( diagclass, strptr, length )
     DiagClasst     diagclass;
     char      *strptr;
     int            length;
begin
     extern    int       errno;

     /* write the buffer */
     if ( write( diagcxt[ (int) diagclass ].channel, strptr, length ) != length) begin
          /* error writing */
          return( errno );
     end else return( 0 );
end /* DiagIOputBuf */

/* function DiagIOputs - put a null terminated string to the diagClass file.
   Returns 0 or OS error code.
*/
int  DiagIOputs( diagclass, strptr )
     DiagClasst     diagclass;
     char      *strptr;
begin

     return( DiagIOputBuf( diagclass, strptr, strlen( strptr ) ) );
end /* DiagIOputs */


/* function DiagIOputc - put a character to the diagClass file
   Returns 0 or OS error code.
*/
int  DiagIOputc( diagclass, c )
     DiagClasst     diagclass;
     char      c;
begin

     return( DiagIOputBuf( diagclass, & c, 1 ) );
end /* DiagIOputc */

/* end of diagio.c */
