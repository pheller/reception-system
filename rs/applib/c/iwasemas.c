/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWASEMAS - semaphore functions                     */
/*                                          */
/*    DESCRIPTION - this module is a collection of subroutines to*/
/*        provide support for supervisor semaphore services. The */
/*        routines are:                          */
/*          defsem  - define a semaphore to the system     */
/*          claimsem - request/claim the semaphore for your task */
/*          relsem  - release semaphore to make available       */
/*                                          */
/*    COMMENTS -                                 */
/*        1. Invalid semaphore requests abend the multitasker.   */
/*                                          */
/********************************************************************/
#include <iwssvccc.h>         /* svc request constants */
#include <iwssvcsc.h>         /* svc request structures */
static struct sfvplst *nullplst = 0; /* null pointer   */
extern issuesvc();

/********************************************************************/
/* defsem - create a semaphore. The argument is the semaphore num-  */
/*     ber, which must have been predefined. The semaphore has no   */
/*     name. No value is returned.                    */
/*                                          */
/********************************************************************/
void defsem(semid)
int semid;          /* pointer to 8-character semaphore name */
{
  struct svcregs svcregs;          /* parms to svc       */
  
  svcregs.sr_indx = semid;
  svcregs.sr_type = _QSVCSEMMK;
  svcregs.sr_spec = 0;
  issuesvc(nullplst,&svcregs);             /* execute the svc   */
}

/********************************************************************/
/* claimsem - claim the semaphore identified by the input argument. */
/*     The task waits if the semaphore is unavailable, even if the  */
/*     requesting task is the owner. Upon return, the task owns     */
/*     the semaphore unless a higher-priority task released it.     */
/*     (That should not happen.)                      */
/*                                          */
/********************************************************************/
void claimsem(sfv)
int sfv;       /* semaphore id  */
{
  struct svcregs svcregs;
  svcregs.sr_indx = sfv;      /* semaphore id          */
  svcregs.sr_type = _QSVCCLAIM;      /* request type = claim */
  svcregs.sr_spec = 0;             /* unused           */
  issuesvc(nullplst, &svcregs);      /* execute the svc     */
 }

/********************************************************************/
/* relsem - release the semaphore identified by the input argument. */
/*     Pending claim requests are thus activated, so the semaphore  */
/*     may be reclaimed. Relsem returns nothing.           */
/*                                          */
/********************************************************************/
void relsem(sfv)
int sfv;       /* sempahore id  */
{
  struct svcregs svcregs;
  svcregs.sr_indx = sfv;      /* semaphore id          */
  svcregs.sr_type = _QSVCREL;      /* request type = release  */
  svcregs.sr_spec = 0;             /* unused           */
  issuesvc(nullplst, &svcregs);      /* execute the svc     */
 }
