syntax on
filetype indent plugin on
set modeline
set modelines=5
:command! -bang -range -nargs=1 -complete=file MW <line1>,<line2>write<bang> <args> | <line1>,<line2>delete _
:command! -bang -range -nargs=1 -complete=file MA <line1>,<line2>write<bang> >> <args> | <line1>,<line2>delete _

if has("gui_macvim")
    " set macvim specific stuff
    set list
    set listchars=eol:⏎,tab:␉·,trail:␠,nbsp:⎵
    set syntax=off
    set noai

    set cursorline
    set cursorcolumn
    set ruler
    set nu
    set et
    set virtualedit=all
    set bg=dark
    autocmd ColorScheme * highlight Normal guibg=Black guifg=White
    autocmd ColorScheme * highlight ColorColumn guibg=#222222
    autocmd ColorScheme * highlight CursorColumn guibg=#555555
    autocmd ColorScheme * highlight CursorRow guibg=#555555
    autocmd ColorScheme * highlight LineNr guifg=yellow
    autocmd ColorScheme * highlight NonText guifg=red
    autocmd ColorScheme * highlight SpecialKey guifg=red

    set guifont=Courier:h14
    exec "set cc=" . join(range(1,80,2), ",")
    set autochdir


    function! SuperTab()
      let currow = getcurpos()[1]
      let curcol = getcurpos()[4]

      let searchstr = strpart(getline(prevnonblank(currow-1)), curcol-1)

      let coloffset = matchend(searchstr, '\S*\s*')

      call cursor(currow, curcol+coloffset)

    endfunction

    inoremap <tab> <C-O>:call SuperTab()<CR>

    set comments=sl:/*,mb:*,elx:*/
    set formatoptions+=r
endif
